#include <Qt>
#include <QDir>
#include <QtCore>
#include <QWhatsThis>
#include <QMessageBox>
#include <QString>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QtPrintSupport/QPrinter>
#include <QListWidget>
#include <QListWidgetItem>
#include <QTextDocument>
#include <QIODevice>
#include <QtDebug>
#include <QTextCodec>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "landing.h"
#include "sha1.h"
#include "contexthelp.h"
#include "linkfilter.h"

MainWindow::MainWindow(QWidget *parent) :

    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    config = new Autosave();
    config->load();

    QTimer *autoSaveTimer = new QTimer();
    autoSaveTimer->start((config->map["spinSavingRate"].toInt() * 60 * 1000));
    connect(autoSaveTimer,SIGNAL(timeout()),this, SLOT(timerevent()));
    
    UndoRedoStack = new MyUndoRedoStack(config->map["spinSaveDepth"].toInt());

//---------------------------------------------
//    //Загрузка списка аэропортов из БД в combobox
//    QSqlQueryModel * modal = new QSqlQueryModel();
//    showBD();
//    QSqlQuery* query = new QSqlQuery(db);
//    query->prepare("select AirpName from AirportsTable");
//    query->exec();
//    modal->setQuery(*query);
//    ui->cmbDepAirport->setModel(modal);
//    ui->cmbDestAirport->setModel(modal);
//    //---------------------------------------------

    ui->btnShowReport_Confirm->setVisible(false);
    ui->btnShowReport_Cancel->setVisible(false);

    //отображение список+фильтр файлов в конкретном месте
    QDir reportDirectory(reportFolder);
    reportDirectory.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QStringList myList = reportDirectory.entryList();
    ui->lstWidget->addItems(myList);

    helpwindow = new helpsurface(this, ui->btnContextHelpMode);

    contexthelp = new ContextHelp(0);
    Linkfilter *filter = new Linkfilter(this);
    this->installEventFilter(filter);
    connect(filter, SIGNAL(linkClicked(QString)),this, SLOT(ShowLink(QString)));
    
    //Задаем всплывающие подсказки для кнопок
    ui->btnContextHelpMode->setWhatsThis("Вы перешли в режим глобальной справки, для получения <b>дополнительной информации</b>  <a href='www.google.com'>Нажми тут</a>");

    // Сделать в combobox неактивными нужные элементы
    ui->cmbAircraftType->setItemData(2, 0, Qt::UserRole - 1);
    ui->cmbAircraftType->setItemData(3, 0, Qt::UserRole - 1);
    ui->cmbAircraftType->setItemData( 2, QColor( 100, 100, 100, 255 ), Qt::ForegroundRole );
    ui->cmbAircraftType->setItemData( 3, QColor( 100, 100, 100, 255 ), Qt::ForegroundRole );

    //ui->cmbDepAirport->setItemData(2, 0, Qt::UserRole - 1);
    //ui->cmbDepAirport->setItemData( 2, QColor( 100, 100, 100, 255 ), Qt::ForegroundRole );

    //ui->cmbDestAirport->setItemData(2, 0, Qt::UserRole - 1);
    //ui->cmbDestAirport->setItemData( 2, QColor( 100, 100, 100, 255 ), Qt::ForegroundRole );

    //ui->swcmbAltAirport->setStyleSheet("QListView{color:rgb(100,100,100);}");

    ui->lblVf_value->hide();
    ui->lblVs_value->hide();
    ui->lblV1_value->hide();
    ui->line_30->hide();
    ui->line_27->hide();
    ui->line_23->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

// ФУНКЦИИ НА КНОПКАХ
void MainWindow::ShowLink(const QString &Link){
    contexthelp->show();    //Слот открытия основной справки по гиперссылке whatsthis
}

// Открытие -> Вкладка ПОСАДКА
void MainWindow::on_btnShowLandingPage_pressed()
{
    ui->labelPageHeader->setText("ПОСАДКА");
    ui->swPages->setCurrentWidget(ui->pageLandingInput);
}

// Открытие -> Вкладка НАСТРОЙКИ
void MainWindow::on_btnShowSettingsPage_pressed()
{
    ui->labelPageHeader->setText("НАСТРОЙКИ");
    ui->swPages->setCurrentWidget(ui->pageSettings);
    ui->stackedWidget->setCurrentIndex(0);
}

// Открытие -> Вкладка ВЗЛЁТ
void MainWindow::on_btnShowTakeoffPage_pressed()
{
    ui->labelPageHeader->setText("ВЗЛЁТ");
    ui->swPages->setCurrentWidget(ui->pageTakeOffInput);
}

// Открытие -> Вкладка НАЧАЛЬНЫЕ ДАННЫЕ
void MainWindow::on_btnShowGeneralPage_pressed()
{
    ui->swPages->setCurrentWidget(ui->pageInitiaParameters);
    ui->labelPageHeader->setText("НАЧАЛЬНЫЕ ДАННЫЕ");
}

// Открытие -> Вкладка КРЕЙСЕРСКИЙ РЕЖИМ
void MainWindow::on_btnShowCruisePage_pressed()
{
    ui->labelPageHeader->setText("КРЕЙС. РЕЖ.");
    ui->swPages->setCurrentWidget(ui->pageCruise);
}

// Открытие -> Вкладка СОХРАНЁННЫЕ РАСЧЁТЫ
void MainWindow::on_btnShowCalculations_pressed()
{
    ui->labelPageHeader->setText("СОХРАНЁННЫЕ РАСЧ.");
    ui->swPages->setCurrentWidget(ui->pageSavedCalculations);
}

// Открытие -> Вкладка БИБЛИОТЕКА АП
void MainWindow::on_btnShowAirportLibraryPage_pressed()
{
    ui->labelPageHeader->setText("БИБЛИОТЕКА АП");
    ui->swPages->setCurrentWidget(ui->pageShowAirportLibrary);
}

// Вкладка ВЫХОД ( из приложения )
void MainWindow::on_btnExit_clicked()
{
    config->save();
    QApplication::quit();
}

// Условия разрешения работы со вкладками
void MainWindow::IfInitialDataAssigned()
{
    bool isInitialDataAssigned = (ui->cmbAircraftType->currentText() != "---")&&\
            (ui->btn_Ch_Dep_Airport->text() != "Выбрать аэропорт отправления")&&\
            (ui->btn_Ch_Dest_Airport->text() != "Выбрать аэропорт назначения")&&\
            (ui->btn_Ch_Dep_Airport->text() != "")&&\
            (ui->btn_Ch_Dest_Airport->text() != ""); // если отмечены 3 комбо-бокса
    if(isInitialDataAssigned)
    {
        ui->btnShowCruisePage->setEnabled(true);
        ui->btnShowLandingPage->setEnabled(true);
        ui->btnShowTakeoffPage->setEnabled(true);
        ui->swlblInitialExclamation->setText("Необходимые начальные параметры заданы");
        ui->swlblInitialExclamation->setStyleSheet("color: rgb(0, 255, 0);");
    }
    else
    {
        ui->btnShowCruisePage->setEnabled(false);
        ui->btnShowLandingPage->setEnabled(false);
        ui->btnShowTakeoffPage->setEnabled(false);
        ui->swlblInitialExclamation->setText("<html><head/><body><p>Для продолжения расчёта ЛТХ на режимах укажите</p><p>\
                                             тип ВС и аэропорты отправления, прибытия и запасной</p></body></html>");
                                             ui->swlblInitialExclamation->setStyleSheet("color:yellow;");
        return;
    }
}

/*  void MainWindow::IfInitialDataAssigned1()
    {
        bool isInitialDataAssigned1 = (ui->ledReportPassword->text() != ""); // если password введён
        if(isInitialDataAssigned1)
    {
        ui->btnCreateReport->setEnabled(true);
    }
    else
    {
        ui->btnCreateReport->setEnabled(false);
        return;
    }
    }*/

// Запись в поле Тип самолёта в Header
void MainWindow::on_cmbAircraftType_currentIndexChanged()
{
    ui->lblnameAircraftType->setText(ui->cmbAircraftType->currentText());
    IfInitialDataAssigned();
}

// Запись в поле Аэропорт отправления в Header
void MainWindow::on_cmbDepAirport_currentIndexChanged()
{
    IfInitialDataAssigned();
}

// Запись в поле Аэропорт назначения в Header
void MainWindow::on_cmbDestAirport_currentIndexChanged()
{
    IfInitialDataAssigned();
}

// Функция расчётов для Посадки
void MainWindow::CalculateLanding()
{
    /* ИД */
    double weight = ui->spnWeightLanding->value();
    double bar_altitude = ui->spinAltitudeBarLanding->value();
    double dblWind = ui->spnWindLanding->value();
    double dblTemperature = ui->spinTemperatureLanding->value();
    double dblRWSlope = ui->spinSlopeLandingRW->value();

    FlapsConf enmFlapsConfLanding = FLAPS_3;
    switch(ui->cmbFlapsModeLanding->currentIndex())
    {
    case 0:
    {
        enmFlapsConfLanding = FLAPS_0;
        break;
    }
    case 1:
    {
        enmFlapsConfLanding = FLAPS_1;
        break;
    }
    case 2:
    {
        enmFlapsConfLanding = FLAPS_1_PLUS_F;
        break;
    }
    case 3:
    {
        enmFlapsConfLanding = FLAPS_2;
        break;
    }
    case 4:
    {
        enmFlapsConfLanding = FLAPS_3;
        break;
    }
    case 5:
    {
        enmFlapsConfLanding = FLAPS_FULL;
        break;
    }
    default:
        break;
    }

    FBWCS_mode enmFBWCSmode = FBWCS_DIRECT_MODE;

    /* ... Vsw ...*/
    double Vsw = getVsr(weight,
                        bar_altitude,
                        enmFlapsConfLanding);
    ui->lblVsw_value->setText(QString::number(Vsw, 'f', 0));

    /* ... Vmin ...*/
    double Vmin = getVmin(weight, enmFlapsConfLanding);
    ui->lblVmin_value->setText(QString::number(Vmin, 'f', 0));

    /* ... Vref ... */
    double Vref = getVref( weight,
                           enmFBWCSmode,
                           enmFlapsConfLanding);
    ui->lblVr_value->setText(QString::number(Vref, 'f', 0));

    /* ... ALD ... */
    double dblALD = getALD(weight, // посадочный вес, [кг]
                           enmFlapsConfLanding, // конфигурация механизации крыла
                           BM_MED, // режим торможения
                           bar_altitude, // высота, [фт]
                           dblWind, // попутный ветер, [уз.]
                           dblTemperature); // температура, [град.C]
    ui->lblALD_value->setText(QString::number(dblALD, 'f', 0));

    /* ... Vmax ... */
    double Vmax = getVmax(enmFlapsConfLanding);
    ui->lblVmax_value->setText(QString::number(Vmax, 'f', 0));

    /* ... Mmax ... */
    double Mmax = getM_max(dblTemperature,
                           bar_altitude,
                           dblALD,
                           dblRWSlope,
                           dblWind,
                           1.6);
    ui->lblMmax_landing_value->setText(QString::number(Mmax, 'f', 0));
}

// Вкладка ВЗЛЁТ
// Переход -> Просмотр Результатов
void MainWindow::on_btnResults_pressed()
{
    ui->swPages->setCurrentWidget(ui->pageTakeOffOutput);
}

// Переход -> Ввод Данных
void MainWindow::on_btnDataEnter_pressed()
{
    ui->swPages->setCurrentWidget(ui->pageTakeOffInput);
}

//Кнопки на странице ВЗЛЁТ
// Листания списка cmbRWSurfStatusTakeoff  вверх
void MainWindow::on_btnRunwayStatusUP_pressed()
{
    int intCurrentIndex = ui->cmbRWSurfStatusTakeoff->currentIndex();
    if(intCurrentIndex > 0)
    {
        ui->cmbRWSurfStatusTakeoff->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbRWSurfStatusTakeoff  вниз
void MainWindow::on_btnRunwayStatusDOWN_pressed()
{
    int intCurrentIndex = ui->cmbRWSurfStatusTakeoff->currentIndex();
    if(intCurrentIndex < 7 )
    {
        ui->cmbRWSurfStatusTakeoff->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbEngineIntactTakeoff  вверх
void MainWindow::on_btnEngineConfUP_pressed()
{
    int intCurrentIndex = ui->cmbEngineIntactTakeoff->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbEngineIntactTakeoff->setCurrentIndex(intCurrentIndex-1);
    }

}

// Листания списка cmbEngineIntactTakeoff  вниз
void MainWindow::on_btnEngineConfDOWN_pressed()
{
    int intCurrentIndex = ui->cmbEngineIntactTakeoff->currentIndex();
    if(intCurrentIndex < 1 )
    {
        ui->cmbEngineIntactTakeoff->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbEngineBleedTakeoff  вверх
void MainWindow::on_btnPickUP_pressed()
{
    int intCurrentIndex = ui->cmbEngineBleedTakeoff->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbEngineBleedTakeoff->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbEngineBleedTakeoff  вниз
void MainWindow::on_btnPickDOWN_pressed()
{
    int intCurrentIndex = ui->cmbEngineBleedTakeoff->currentIndex();
    if(intCurrentIndex < 3 )
    {
        ui->cmbEngineBleedTakeoff->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbFBWCSmodeTakeoff  вверх
void MainWindow::on_btnModeEdsuUP_pressed()
{
    int intCurrentIndex = ui->cmbFBWCSmodeTakeoff->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbFBWCSmodeTakeoff->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbFBWCSmodeTakeoff  вниз
void MainWindow::on_btnModeEdsuDOWN_pressed()
{
    int intCurrentIndex = ui->cmbFBWCSmodeTakeoff->currentIndex();
    if(intCurrentIndex < 1 )
    {
        ui->cmbFBWCSmodeTakeoff->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbFlapsModeTakeoff  вверх
void MainWindow::on_btnConfMechUP_pressed()
{
    int intCurrentIndex = ui->cmbFlapsModeTakeoff->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbFlapsModeTakeoff->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbFlapsModeTakeoff  вниз
void MainWindow::on_btnConfMechDOWN_pressed()
{
    int intCurrentIndex = ui->cmbFlapsModeTakeoff->currentIndex();
    if(intCurrentIndex < 5 )
    {
        ui->cmbFlapsModeTakeoff->setCurrentIndex(intCurrentIndex+1);
    }
}

// Вкладка ПОСАДКИ
// Переход -> Просмотр Результатов
void MainWindow::on_btnResults_Land_pressed()
{
    CalculateLanding();
    ui->swPages->setCurrentWidget(ui->pageLandingOutput);
}

// Переход -> Ввод Данных
void MainWindow::on_btnDataEnterLand_pressed()
{
    ui->swPages->setCurrentWidget(ui->pageLandingInput);
}

//Кнопки на странице ПОСАДКА
// Листания списка cmbRWSurfStatusLanding  вверх
void MainWindow::on_btnRunwayStatusUP_Land_pressed()
{
    int intCurrentIndex = ui->cmbRWSurfStatusLanding->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbRWSurfStatusLanding->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbRWSurfStatusLanding  вниз
void MainWindow::on_btnRunwayStatusDOWN_Land_pressed()
{
    int intCurrentIndex = ui->cmbRWSurfStatusLanding->currentIndex();
    if(intCurrentIndex < 7 )
    {
        ui->cmbRWSurfStatusLanding->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbFBWCSmodeLanding  вверх
void MainWindow::on_btnModeEdsuUP_Land_pressed()
{
    int intCurrentIndex = ui->cmbFBWCSmodeLanding->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbFBWCSmodeLanding->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbFBWCSmodeLanding  вниз
void MainWindow::on_btnModeEdsuDOWN_Land_pressed()
{
    int intCurrentIndex = ui->cmbFBWCSmodeLanding->currentIndex();
    if(intCurrentIndex < 1 )
    {
        ui->cmbFBWCSmodeLanding->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbFlapsModeLanding  вверх
void MainWindow::on_btnEngineConfUP_Land_pressed()
{
    int intCurrentIndex = ui->cmbFlapsModeLanding->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbFlapsModeLanding->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbFlapsModeLanding  вниз
void MainWindow::on_btnEngineConfDOWN_Land_pressed()
{
    int intCurrentIndex = ui->cmbFlapsModeLanding->currentIndex();
    if(intCurrentIndex < 5 )
    {
        ui->cmbFlapsModeLanding->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbEngineIntactLanding  вверх
void MainWindow::on_btnEngineModeUP_Land_pressed()
{
    int intCurrentIndex = ui->cmbEngineIntactLanding->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbEngineIntactLanding->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbEngineIntactLanding  вниз
void MainWindow::on_btnEngineModeDOWN_Land_pressed()
{
    int intCurrentIndex = ui->cmbEngineIntactLanding->currentIndex();
    if(intCurrentIndex < 1 )
    {
        ui->cmbEngineIntactLanding->setCurrentIndex(intCurrentIndex+1);
    }
}

// Вкладка АП
// Переход -> Страница Полной информации о аэропорте
void MainWindow::on_btnAirportLibrary_pressed()
{
    ui->stackedWidget_2->setCurrentIndex(1);
}

// Переход -> Страница список аэропортов
void MainWindow::on_btnAirportLibraryBack_pressed()
{
    ui->stackedWidget_2->setCurrentIndex(0);
}

// Вкладка НАСТРОЙКИ
// Переход -> Страница Отчётов
void MainWindow::on_btnReport_pressed()
{
    ui->stackedWidget->setCurrentIndex(1);
}

// Переход -> Страница Стили
void MainWindow::on_btnSettingStyle_pressed()
{
    ui->stackedWidget->setCurrentIndex(2);
}

// Переход -> Страница Погода
void MainWindow::on_btnSettingWeather_pressed()
{
    ui->stackedWidget->setCurrentIndex(3);
}

// Переход -> Страница Сохранения
void MainWindow::on_btnSettingSave_pressed()
{
    ui->stackedWidget->setCurrentIndex(4);
}

// Переход -> Страница Профиля
void MainWindow::on_btnSettingProfile_pressed()
{
    ui->stackedWidget->setCurrentIndex(5);
    ui->stackedWidget_3->setCurrentIndex(0);
}

// Переход -> Вкладка НАСТРОЙКИ
void MainWindow::on_btnStyleBack_pressed()
{
    ui->stackedWidget->setCurrentIndex(0);
}

// Переход -> Вкладка НАСТРОЙКИ
void MainWindow::on_btnWeatherBack_pressed()
{
    ui->stackedWidget->setCurrentIndex(0);
}

// Переход -> Вкладка НАСТРОЙКИ
void MainWindow::on_btnSaveBack_pressed()
{
    ui->stackedWidget->setCurrentIndex(0);
}

// Переход -> Вкладка НАСТРОЙКИ
void MainWindow::on_btnReportBack_pressed()
{
    ui->stackedWidget->setCurrentIndex(0);
}

// Листания списка cmbStyleFont вверх
void MainWindow::on_btnStyleFontUP_pressed()
{
    int intCurrentIndex = ui->cmbStyleFont->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbStyleFont->setCurrentIndex(intCurrentIndex-1);
    }

}

// Листания списка cmbStyleFont  вниз
void MainWindow::on_btnStyleFontDOWN_pressed()
{
    int intCurrentIndex = ui->cmbStyleFont->currentIndex();
    if(intCurrentIndex < 1 )
    {
        ui->cmbStyleFont->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbStyleColour  вверх
void MainWindow::on_lblStyleColourUP_pressed()
{
    int intCurrentIndex = ui->cmbStyleColour->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbStyleColour->setCurrentIndex(intCurrentIndex-1);
    }

}

// Листания списка cmbStyleColour вниз
void MainWindow::on_lblStyleColourDOWN_pressed()
{
    int intCurrentIndex = ui->cmbStyleColour->currentIndex();
    if(intCurrentIndex < 1 )
    {
        ui->cmbStyleColour->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbWeatherserver вверх
void MainWindow::on_btnWeatherserverUP_pressed()
{
    int intCurrentIndex = ui->cmbWeatherserver->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbWeatherserver->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbWeatherserver вниз
void MainWindow::on_btnWeatherserverDOWN_pressed()
{
    int intCurrentIndex = ui->cmbWeatherserver->currentIndex();
    if(intCurrentIndex < 2 )
    {
        ui->cmbWeatherserver->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbWeathermeteo вверх
void MainWindow::on_btnWeathermeteoUP_pressed()
{
    int intCurrentIndex = ui->cmbWeathermeteo->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbWeathermeteo->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbWeathermeteo вниз
void MainWindow::on_btnWeathermeteoDOWN_pressed()
{
    int intCurrentIndex = ui->cmbWeathermeteo->currentIndex();
    if(intCurrentIndex < 3 )
    {
        ui->cmbWeathermeteo->setCurrentIndex(intCurrentIndex+1);
    }
}

// Листания списка cmbAvtoSave  вверх
void MainWindow::on_btnAvtoSaveUP_pressed()
{
    int intCurrentIndex = ui->cmbAvtoSave->currentIndex();
    if(intCurrentIndex > 0 )
    {
        ui->cmbAvtoSave->setCurrentIndex(intCurrentIndex-1);
    }
}

// Листания списка cmbAvtoSave вниз
void MainWindow::on_btnAvtoSaveDOWN_pressed()
{
    int intCurrentIndex = ui->cmbAvtoSave->currentIndex();
    if(intCurrentIndex < 1 )
    {
        ui->cmbAvtoSave->setCurrentIndex(intCurrentIndex+1);
    }
}

// Страницы Профиля
// Переход -> Настройки ( Главная )
void MainWindow::on_btnMainProfileBack_pressed()
{
    ui->stackedWidget->setCurrentIndex(0);
}

// Переход -> Вход
void MainWindow::on_btnMainLog_pressed()
{
    ui->stackedWidget_3->setCurrentIndex(1);
}

// Переход -> Регистрация
void MainWindow::on_btnMainReg_pressed()
{
    ui->stackedWidget_3->setCurrentIndex(2);
}

// Страницы Регистрации
// Переход -> Главная страницы Профиля
void MainWindow::on_btnRegBack_pressed()
{
    // Очистка полей ввода

    ui->lblRegERORR->clear();
    ui->ledRegLogin->clear();
    ui->ledRegPassword->clear();
    ui->ledRegPassword2->clear();

    ui->stackedWidget_3->setCurrentIndex(0);
}

// Функция Регистрации
void MainWindow::on_btnReg_pressed()
{
    QString qlogin, qpass, qpass1, qpass2;

    // Чтение вводимой информации в переменные

    qlogin = ui->ledRegLogin->text();

    qpass1 = ui->ledRegPassword->text();

    qpass2 = ui->ledRegPassword2->text();

    // Если всё верно -> Регистрация успешна, иначе вывод ошибки

    if(qpass1 == qpass2 && qlogin != "" && qpass1 != "" && qpass2 != ""){

        qpass = qpass1;

        std::string login, pass;

        // Перевод QString в string для работы хеширования

        login = qlogin.toStdString();
        pass = qpass.toStdString();

        // Хеширование

        login = sha1(login);
        pass = sha1(pass);

        //Запись хешированных данных пользователя в файл

        QFile out("/res/reg.txt");
        if(out.open(QIODevice::Append | QIODevice::Text))
        { // Если файл успешно открыт для записи в текстовом режиме
            QTextStream writeStream(&out); // Создаем объект класса QTextStream
            // и передаем ему адрес объекта fileOut


            writeStream << login.c_str() << endl;
            writeStream << pass.c_str() << endl;
            out.close(); // Закрываем файл
        }

        // Оповощение

        ui->lblRegERORR->setStyleSheet("color: rgb(0, 255, 0)");
        ui->lblRegERORR->setText("Регистрация успешна");

        //Очистка полей ввода

        ui->ledRegLogin->clear();
        ui->ledRegPassword->clear();
        ui->ledRegPassword2->clear();
    }
    else{
        if(qpass1 != qpass2){
            ui->lblRegERORR->setStyleSheet("color: rgb(255, 0, 0)");
            ui->lblRegERORR->setText("Пароли не совпадают");
        }
        else {
            ui->lblRegERORR->setStyleSheet("color: rgb(255, 0, 0)");
            ui->lblRegERORR->setText("Введите логин/пароль");
        }
    }
}

// Страницы Входа
// Переход -> Главная страницы Профиля
void MainWindow::on_btnLogBack_pressed()
{
    // Очистка полей ввода

    ui->lblLogERORR->clear();
    ui->ledLogLogin->clear();
    ui->ledLogPassword->clear();

    ui->stackedWidget_3->setCurrentIndex(0);
}

// Функция Входа в профиль
void MainWindow::on_btnLog_pressed()
{
    QString qlogin, qpass, qbufferlog, qbufferpass;

    // Чтение вводимой информации в переменные

    qlogin = ui->ledLogLogin->text();

    qpass = ui->ledLogPassword->text();

    std::string login, pass, bufferlog, bufferpass;

    // Если всё верно -> Вход успешен, иначе вывод ошибки

    if(qlogin != "" && qpass != ""){

        // Перевод QString в string для работы хеширования

        login = qlogin.toStdString();
        pass = qpass.toStdString();

        // Хеширование

        login = sha1(login);
        pass = sha1(pass);

        // Считывание из файла логина и пароля

        QFile reg("reg.txt");
        if(reg.open(QIODevice::ReadOnly | QIODevice::Text))
        { // Если файл успешно открыт для записи в текстовом режиме
            while(!reg.atEnd()){
                qbufferlog = reg.readLine();
                qbufferpass = reg.readLine();

                // Обратный перевод из string в QString

                bufferlog = qbufferlog.simplified().toStdString();
                bufferpass = qbufferpass.simplified().toStdString();

                // Сравнение логина и пароля

                if(bufferlog == login && bufferpass == pass){
                    ui->lblLogERORR->setText("");
                    ui->stackedWidget_3->setCurrentIndex(3);
                }
                else{
                    ui->lblLogERORR->setStyleSheet("color: rgb(255, 0, 0)");
                    ui->lblLogERORR->setText("Неверный логин/пароль");
                }
            }
            reg.close();
        }
    }
    else{
        ui->lblLogERORR->setStyleSheet("color: rgb(255, 0, 0)");
        ui->lblLogERORR->setText("Введите логин/пароль");

    }
}
/* QString name = ui->comboBox->currentText();

    QSqlQuery qry;
    qry.prepare("select * from AirportsTable where AirportName = '"+name+"' ");

    qry.prepare("select * from AirportsTable where AirportName = '"+val+"' or Country = '"+val+"' or City = '"+val+"' or Coordinatese = '"+val+"' or CodeIKAO = '"+val+"' ");
/* qry.exec();

    if(qry.exec())
    {
        while(qry.next())
        {
            ui->ledAirportName->setText(qry.value(0).toString());
            ui->ledCountry->setText(qry.value(1).toString());
            ui->ledCity->setText(qry.value(2).toString());
            ui->ledCoordinates->setText(qry.value(3).toString());
            ui->ledCodeIKAO->setText(qry.value(4).toString());
        }

    }*/
void MainWindow::on_btnContextHelpMode_clicked(bool checked)
{
    if(checked)
    {
        // показать поверхность контекстной справки
        helpwindow->show();
    }
    else
    {
        // убрать поверхность контекстной справки
        helpwindow->hide();
    }
}
void MainWindow::on_btnContextHelpOpen_clicked()
{
    //Открыть основную справку
    contexthelp->show();
}

//Сохранение отчёта, будут добавлены сразу все параметры ввода

void MainWindow::on_btnCreateReport_clicked()
{
    if(ui->ledReportPassword->text().isEmpty()){
        QMessageBox *qm = new QMessageBox(this);
        qm->setStyleSheet("color: white;");
        qm->critical(0, "Enter password", "Please enter password");
        return;
    }
    reportDirname = (reportFolder + "/" + QDate::currentDate().toString("'IRKUT_'yyyy_MM_dd") +
                             QTime::currentTime().toString("_hh_mm_ss"));// строка с текущей датой
    QDir().mkdir(reportDirname);
    /* Сздание папки */
    //qDebug(dirname.toLatin1());
    QDir reportDirectory(reportDirname);
    if (!reportDirectory.exists()){
        //qDebug("Папка не существует");
        QDir().mkdir(reportDirname);
    }
    QString reportFilename = reportDirname + "/" + "Report.htm";
    QString back = reportDirname + "/" + "Back";
    QDir().mkdir(back);

    /* словарь параметров */

    QHash<QString, FlightData> flightData;
    //flightData["V1"] = FlightData(100.0);
    flightData["V1"] = FlightData(100.0);
    flightData["AirpCode"] = QString("Sheremetevo");
    flightData["IntType"] = 5000;

    QFile templateFile(QDir().currentPath() + "/res/Template.htm");
    if (!templateFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QMessageBox *qm = new QMessageBox(this);
        qm->setStyleSheet("color: white;");
        qm->critical(0, "Cannot open file Template.htm", "Cannot open file Template.htm");
        return;
    }
    QByteArray buffer = templateFile.readAll();
    templateFile.close();
    QString allContents = QString::fromLatin1(buffer);
    /* подстановки*/
    QHash<QString, FlightData>::iterator i;

    //TODO

    //    for (i = flightData.begin();
    //         i != flightData.end();
    //         i++)
    //    {
    //        QString stringForReplace;
    //        switch (i.value().data_type) {
    //        case FlightData::typeDouble :
    //        {
    //            stringForReplace = QString::number(i.value().data.toDouble());
    //            qDebug() << "typeDouble";
    //            qDebug() << "stringForReplace = "<< stringForReplace;
    //            break;
    //        }
    //        case FlightData::typeInteger :
    //        {
    //            stringForReplace = QString::number(i.value().data.toInt());
    //            qDebug() << "typeInteger";
    //            qDebug() << "stringForReplace = "<< stringForReplace;
    //            break;
    //        }
    //        case FlightData::typeString :
    //        {
    //            stringForReplace =i.value().data.toString();
    //            qDebug() << "typeString";
    //            qDebug() << "stringForReplace = "<< stringForReplace;
    //            break;
    //        }
    //        default:
    //            break;
    //        }
    //        allContents = allContents.replace(i.key(),
    //                                          stringForReplace);
    //    }

    /* Вписать в файл */

    buffer = allContents.toLatin1();
    QFile reportFile(reportFilename);
    if(!reportFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QMessageBox *qm = new QMessageBox(this);
        qm->setStyleSheet("color: white;");
        qm->critical(0, "Cannot open file " + reportFilename, "Cannot open file " + reportFilename);
        return;
        }
    reportFile.write(buffer);
    reportFile.resize(reportFile.pos());
    reportFile.seek(0);
    QTextStream in2(&reportFile);
    ui->txtBrowse->setHtml(in2.readAll());
    ui->ledReportPassword->setVisible(false);
    ui->lblReportPodpis->setVisible(false);
    ui->btnShowReport_Confirm->setVisible(true);
    ui->btnShowReport_Cancel->setVisible(true);
    reportFile.close();
}
void MainWindow::on_lstWidget_doubleClicked()
{
    int row = ui->lstWidget->currentRow();
    if(ui->lstWidget->currentItem()->text() == "Report.htm"){
        return ;
    }
    QString item = ui->lstWidget->takeItem(row)->text();
    ui->lstWidget->clear();

    if(item == "Back"){
        QDir reportDirectory(reportFolder);
        reportDirectory.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
        QStringList myList = reportDirectory.entryList();
        ui->lstWidget->clear();
        ui->lstWidget->addItems(myList);
        ui->txtBrowse->clear();
    }
    else{
        //отображение список+фильтр файлов в конкретном месте
        QStringList filters;
        filters << "*.htm" << "Back";
        QDir reportDirectory(reportFolder + "/" + item);
        reportDirectory.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
        reportDirectory.setNameFilters(filters);
        QStringList myList = reportDirectory.entryList();
        ui->lstWidget->clear();
        ui->lstWidget->addItems(myList);
    }
    currentReportDirectory = item;
}

void MainWindow::on_btnShowReport_Report_pressed()
{
    if(ui->lstWidget->selectedItems().isEmpty()){
        QMessageBox *qm = new QMessageBox(this);
        qm->setStyleSheet("color: white;");
        qm->information(0, "Select an item.", "Please select an item");
        return;
    }
    int row = ui->lstWidget->currentRow();
    QString item = ui->lstWidget->takeItem(row)->text();
    
    if(item != "Report.htm"){
        QMessageBox *qm = new QMessageBox(this);
        qm->setStyleSheet("color: white;");
        qm->information(0, "Select an item.", "Please select an item");
        QDir reportDirectory(reportFolder);
        reportDirectory.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
        QStringList myList = reportDirectory.entryList();
        ui->lstWidget->clear();
        ui->lstWidget->addItems(myList);
        return;
    }else{
    QString reportFilename = reportFolder + "/" + currentReportDirectory + "/" + item;
    QFile reportFile(reportFilename);
    
    if (!reportFile.open(QIODevice::ReadOnly)){
        QMessageBox *qm = new QMessageBox(this);
        qm->setStyleSheet("color: white;");
        qm->information(0, "Cannot open file " + item, "Canot open file" + item);
        return;
    }
    QTextStream in1(&reportFile);
    ui->txtBrowse->setText(in1.readAll());
}
    QDir reportDirectory(reportFolder);
    reportDirectory.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QStringList myList = reportDirectory.entryList();
    ui->lstWidget->clear();
    ui->lstWidget->addItems(myList);
}

void MainWindow::on_btnShowReport_Confirm_pressed()
{
    QDir templateFiles(QDir::currentPath() + "/res/TemplateFiles");
    if (! templateFiles.exists())
        return;
    QDir().mkdir(reportDirname + "/TemplateFiles");
    foreach (QString f, templateFiles.entryList(QDir::Files)) {
        QFile::copy(templateFiles.path() + "/" + f, reportDirname + "/TemplateFiles/" + f);
    }
    
    /* печать в PDF */
    QPrinter printer;
    printer.setOrientation(QPrinter::Portrait);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);

    // путь сохранения документа
    QString reportFilenamePdf = reportDirname + "/" +"Report.pdf";
    printer.setOutputFileName(reportFilenamePdf);
    QTextDocument doc;
    QFile reportFile(reportDirname + "/Report.htm");
    if (!reportFile.open(QIODevice::ReadOnly)) {
        QMessageBox *qm = new QMessageBox(this);
        qm->setStyleSheet("color: white;");
        qm->critical(0, "Cannot open file " + reportDirname + "/Report.htm", "Cannot open file " + reportDirname + "/Report.htm");
        return;
    }
    QByteArray buffer = reportFile.readAll();
    reportFile.close();
    doc.setHtml(QObject::trUtf8(buffer));
    doc.print(&printer);
    
    QString signCommandString = "java -jar " + QDir::currentPath() +"/PortableSigner.jar -b en -i " + QDir::currentPath() + "/res/img.png -n -o " + reportDirname + "/Report-signed.pdf" + " -p " + ui->ledReportPassword->text() + " -s " + QDir::currentPath() + "/res/pfx.pfx -t " + reportDirname  + "/Report.pdf";
    system((const char *)signCommandString.toStdString().c_str());

    QMessageBox *qm = new QMessageBox(this);
    qm->setStyleSheet("color: white;");
    qm->information(0, "Report saved and signed", "Report saved and signed");

    ui->txtBrowse->clear();
    ui->btnShowReport_Confirm->setVisible(false);
    ui->btnShowReport_Cancel->setVisible(false);
    ui->ledReportPassword->setVisible(true);
    ui->lblReportPodpis->setVisible(true);
    
    QDir reportDirectory1(reportFolder);
    reportDirectory1.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QStringList myList = reportDirectory1.entryList();
    ui->lstWidget->clear();
    ui->lstWidget->addItems(myList);
}

void MainWindow::on_btnShowReport_Cancel_pressed()
{
    QDir().remove(reportDirname);
    ui->txtBrowse->clear();
    ui->btnShowReport_Confirm->setVisible(false);
    ui->btnShowReport_Cancel->setVisible(false);
    ui->ledReportPassword->setVisible(true);
    ui->lblReportPodpis->setVisible(true);
}

void MainWindow::on_Undo_clicked()
{
    UndoRedoStack->RestoreValue();
}

void MainWindow::on_cmbAircraftType_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}

void MainWindow::on_spnWeightTakeoff_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}

void MainWindow::init(){
    ui->cmbAircraftType->setCurrentIndex(config->map["cmbAircraftType"].toInt());
    ui->cmbFlapsModeTakeoff->setCurrentIndex(config->map["cmbFlapsModeTakeoff"].toInt());
    ui->cmbFBWCSmodeTakeoff->setCurrentIndex(config->map["cmbFBWCSmodeTakeoff"].toInt());
    ui->cmbRWSurfStatusTakeoff->setCurrentIndex(config->map["cmbRWSurfStatusTakeoff"].toInt());
    ui->spnSlopeTakeoffRW->setValue(config->map["spnSlopeTakeoffRW"].toDouble());
    ui->cmbEngineBleedTakeoff->setCurrentIndex(config->map["cmbEngineIntactTakeoff"].toInt());
    ui->spinTemperatureTakeoffRW->setValue(config->map["spinTemperatureTakeoffRW"].toInt());
    ui->cmbEngineBleedTakeoff->setCurrentIndex(config->map["cmbEngineBleedTakeoff"].toInt());
    ui->spnWeightTakeoff->setValue(config->map["spnWeightTakeoff"].toDouble());
    ui->spnWindTakeoff->setValue(config->map["spnWindTakeoff"].toDouble());
    ui->spnAltitudeBarTakeoff->setValue(config->map["spnAltitudeBarTakeoff"].toDouble());
    ui->spinSlopeLandingRW->setValue(config->map["spinSlopeLandingRW"].toDouble());
    ui->cmbEngineIntactLanding->setCurrentIndex(config->map["cmbEngineIntactLanding"].toInt());
    ui->cmbFlapsModeLanding->setCurrentIndex(config->map["cmbFlapsModeLanding"].toInt());
    ui->cmbFBWCSmodeLanding->setCurrentIndex(config->map["cmbFBWCSmodeLanding"].toInt());
    ui->cmbRWSurfStatusLanding->setCurrentIndex(config->map["cmbRWSurfStatusLanding"].toInt());
    ui->spinTemperatureLanding->setValue(config->map["spinTemperatureLanding"].toInt());
    ui->spnWindLanding->setValue(config->map["spnWindLanding"].toDouble());
    ui->spnWeightLanding->setValue(config->map["spnWeightLanding"].toDouble());
    ui->spinAltitudeBarLanding->setValue(config->map["spinAltitudeBarLanding"].toDouble());
    ui->spnCruiseMass->setValue(config->map["spnCruiseMass"].toDouble());
    ui->spnCruiseFuel->setValue(config->map["spnCruiseFuel"].toDouble());
    ui->SpnCruiseDistance->setValue(config->map["spnCruiseDistance"].toDouble());
    ui->ledAirportName->setText(config->map["ledAirportName"].toString());    //ПОЛЯ ВЫВОДА ДОП ИНФЫ
    ui->ledBarAlt->setText(config->map["ledBarAlt"].toString());              //ПОЛЯ ВЫВОДА ДОП ИНФЫ
    ui->ledRW_lngth->setText(config->map["ledRW_lngth"].toString());          //ПОЛЯ ВЫВОДА ДОП ИНФЫ
    ui->ledCodeIKAO->setText(config->map["ledCodeIKAO"].toString());          //ПОЛЯ ВЫВОДА ДОП ИНФЫ
    ui->ledCoordinates->setText(config->map["ledCoordinates"].toString());    //ПОЛЯ ВЫВОДА ДОП ИНФЫ
    ui->ledCountry->setText(config->map["ledCountry"].toString());            //ПОЛЯ ВЫВОДА ДОП ИНФЫ
    ui->cmbStyleFont->setCurrentIndex(config->map["cmbStyleFont"].toInt());
    ui->cmbStyleColour->setCurrentIndex(config->map["cmbStyleColour"].toInt());
    ui->spinFontSize->setValue(config->map["spinFontSize"].toInt());
    ui->cmbWeathermeteo->setCurrentIndex(config->map["cmbWeathermeteo"].toInt());
    ui->cmbWeatherserver->setCurrentIndex(config->map["cmbWeatherserver"].toInt());
    ui->spinUpdateRate->setValue(config->map["spinUpdateRate"].toInt());
    ui->cmbAvtoSave->setCurrentIndex(config->map["cmbAutoSave"].toInt());
    ui->spinSaveDepth->setValue(config->map["spinSaveDepth"].toInt());
    ui->spinSavingRate->setValue(config->map["spinSavingRate"].toInt());
    ui->spinSaveDepth->setValue(config->map["spinSaveDepth"].toInt());
}
void MainWindow::on_cmbDepAirport_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbDestAirport_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_swcmbAltAirport_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbRWSurfStatusTakeoff_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnWindTakeoff_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnAltitudeBarTakeoff_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbEngineIntactTakeoff_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbEngineBleedTakeoff_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnSlopeTakeoffRW_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinTemperatureTakeoffRW_valueChanged(int arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbFBWCSmodeTakeoff_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbFlapsModeTakeoff_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbRWSurfStatusLanding_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnWindLanding_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnWeightLanding_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinAltitudeBarLanding_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinSlopeLandingRW_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinTemperatureLanding_valueChanged(int arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbFBWCSmodeLanding_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbFlapsModeLanding_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbEngineIntactLanding_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnCruiseMass_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnCruiseDistance_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spnCruiseFuel_valueChanged(double arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbStyleshrift_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinTemperatureTakeoffRW_3_valueChanged(int arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbStylesxema_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbWeatherserver_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinTemperatureTakeoffRW_4_valueChanged(int arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbWeathermeteo_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_cmbSaveavtosave_currentIndexChanged(int index)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinTemperatureTakeoffRW_5_valueChanged(int arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_spinTemperatureTakeoffRW_6_valueChanged(int arg1)
{
    UndoRedoStack->MemoValue(QObject::sender());
}
void MainWindow::on_btnSettingDefault_clicked()
{
    config->map["cmbDepAirport"] = "---";
    config->map["cmbAircraftType"] = "---";
    config->map["cmbAltAirport"] = "---";
    config->map["cmbDestAirport"] = "---";
    config->map["cmbFlapsModeTakeoff"] = "FLAPS 3";
    config->map["cmbFBWCSmodeTakeoff"] = "DIRECT MODE";
    config->map["cmbRWSurfStatusTakeoff"] = "Сухая";
    config->map["spnSlopeTakeoffRW"] = "0.0";
    config->map["cmbEngineIntactTakeoff"] = "Оба работают";
    config->map["spinTemperatureTakeoffRW"] = "0";
    config->map["cmbEngineBleedTakeoff"] = "СКВ+ПОСмг+ПОСкр";
    config->map["spnWeightTakeoff"] = "36000";
    config->map["spnWindTakeoff"] = "0";
    config->map["spnAltitudeBarTakeoff"] = "0";
    config->map["spinSlopeLandingRW"] = "0.0";
    config->map["cmbEngineIntactLanding"] = "Оба работают";
    config->map["cmbFlapsModeLanding"] = "FLAPS 3";
    config->map["cmbFBWCSmodeLanding"] = "DIRECT MODE";
    config->map["cmbRWSurfStatusLanding"] = "Сухая";
    config->map["spinTemperatureLanding"] = "15";
    config->map["spnWindLanding"] = "2";
    config->map["spnWeightLanding"] = "36000";
    config->map["spinAltitudeBarLanding"] = "1000";
    config->map["spnCruiseMass"] = "36000";
    config->map["spnCruiseFuel"] = "0";
    config->map["SpnCruiseDistance"] = "125";
    config->map["spinTemperatureTakeoffRW_3"] = "20";
    config->map["spinTemperatureTakeoffRW_4"] = "5";
    config->map["cmbRWSurfStatusTakeoff_5"] = "5";
    config->map["spinTemperatureTakeoffRW_6"] = "5";
    config->map["cmbSaveavtosave"] = "Да";
    config->map["cmbWeathermeteo"] = "Только сервер";
    config->map["cmbWeatherserver"] = "1 сервер";
    config->map["spinSaveDepth"] = "5";
    config->map["spinSavingRate"] = "5";
    init();
}
void MainWindow::on_spinTemperatureLanding_valueChanged(const QString &arg1)
{

}
void MainWindow::timerevent(){
    config->save();
}
void MainWindow::on_btnContextHelpMode_clicked()
{

}
void MainWindow::on_spnWindTakeoff_clicked()
{
}

//РАБОТА С БАЗОЙ ДАННЫХ АЭРОПОРТОВ(начало разработки 30.03.17)
bool MainWindow::showBD(){
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./res/my_db.sqlite");
    if (!db.open()){
        qDebug()<<"No connection!";
        return false;
    }
    else {
        qDebug()<<"Connected!";
        dbOpenChecker = true;
    }
    return true;
}
void MainWindow::on_btn_loadAirBD_clicked()
{
    QSqlQueryModel * modal = new QSqlQueryModel();
    showBD();
    QSqlQuery* query = new QSqlQuery(db);
    query->prepare("select * from AirportsTable");
    query->exec();
    modal->setQuery(*query);
    ui->twAirLib->setModel(modal);
    ui->twAirLib->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->twAirLib->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->twAirLib->setEditTriggers(QAbstractItemView::NoEditTriggers);
    qDebug()<<(modal->rowCount());
}
void MainWindow::on_twAirLib_activated(const QModelIndex &index)
{
    QString val=ui->twAirLib->model()->data(index).toString();
    qDebug()<<val;
    QSqlQuery query;
    query.prepare("select * from AirportsTable where CodeICAO='"+val+"' or AirpName='"+val+"' or Country='"+val+"' or RW_lngth='"+val+"' or BarAlt='"+val+"' or Coord='"+val+"'");
    if(query.exec())
    {
        while(query.next())
        {
            ui->ledCodeIKAO->setText(query.value(0).toString());
            ui->ledAirportName->setText(query.value(1).toString());
            ui->ledCountry->setText(query.value(2).toString());
            ui->ledRW_lngth->setText(query.value(3).toString());
            ui->ledBarAlt->setText(query.value(4).toString());
            ui->ledCoordinates->setText(query.value(5).toString());
        }
    }else
        qDebug()<<"ERROR LOAD FULL INFO";
}
void MainWindow::on_lineEditBD_textChanged(const QString &arg1)
{
    QString buff = ui->lineEditBD->text();
    QSqlQuery query;
    QSqlQueryModel * modal = new QSqlQueryModel();
        query.prepare("select * from AirportsTable where CodeICAO like '%"+buff+"%' or AirpName like '%"+buff+"%' or Country like '%"+buff+"%' or Coord like '%"+buff+"%'");
        query.exec();
        modal->setQuery(query);
        ui->twAirLib->setModel(modal);
        ui->twAirLib->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        ui->twAirLib->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        ui->twAirLib->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void MainWindow::on_twAirLibFilter_cellChanged(int row, int column)
{
    if (row == 0 && column == 0){
        filterBuffCodeICAO = ui->twAirLibFilter->item(row, column)->text();
        qDebug()<< filterBuffCodeICAO;
        if (filterBuffCodeICAO != ""){
            filterBuffCodeICAO = "CodeICAO like '%"+filterBuffCodeICAO+"%'";
            qDebug()<< filterBuffCodeICAO;
        }
    }
    if (row == 0 && column == 1){
        filterBuffAirpName = ui->twAirLibFilter->item(row, column)->text();
        qDebug()<< filterBuffAirpName;
        if (filterBuffAirpName != ""){
            filterBuffAirpName = "AirpName like '%"+filterBuffAirpName+"%'";
            qDebug()<< filterBuffAirpName;
        }
    }
    if (row == 0 && column == 2){
        filterBuffCountry = ui->twAirLibFilter->item(row, column)->text();
        qDebug()<< filterBuffCountry;
        if (filterBuffCountry != ""){
            filterBuffCountry = "Country like '%"+filterBuffCountry+"%'";
            qDebug()<< filterBuffCountry;
        }
    }
    if (row == 0 && column == 3){
        filterBuffRW_lngth = ui->twAirLibFilter->item(row, column)->text();
        qDebug()<< filterBuffRW_lngth;
        if (filterBuffRW_lngth != ""){
            filterBuffRW_lngth = "RW_lngth like '%"+filterBuffRW_lngth+"%'";
            qDebug()<< filterBuffCoord;
        }
    }
    if (row == 0 && column == 4){
        filterBuffBarAlt = ui->twAirLibFilter->item(row, column)->text();
        qDebug()<< filterBuffBarAlt;
        if (filterBuffBarAlt != ""){
            filterBuffBarAlt = "BarAlt like '%"+filterBuffBarAlt+"%'";
            qDebug()<< filterBuffBarAlt;
        }
    }
    if (row == 0 && column == 5){
        filterBuffCoord = ui->twAirLibFilter->item(row, column)->text();
        qDebug()<< filterBuffCoord;
        if (filterBuffCoord != ""){
            filterBuffCoord = "Coord like '%"+filterBuffCoord+"%'";
            qDebug()<< filterBuffCoord;
        }
}
    QString sqlReqFilter = "select * from AirportsTable where ";
    bool AndOfSqlReqChecker = false;
    if (filterBuffCodeICAO != "" && AndOfSqlReqChecker == false){
        sqlReqFilter = sqlReqFilter + filterBuffCodeICAO;
        AndOfSqlReqChecker = true;
        qDebug()<< sqlReqFilter;
    } else if (filterBuffCodeICAO != "" && AndOfSqlReqChecker != false){
        sqlReqFilter = sqlReqFilter + " and " + filterBuffCodeICAO;
        qDebug()<< sqlReqFilter;
    }
    if (filterBuffAirpName != "" && AndOfSqlReqChecker == false){
        sqlReqFilter = sqlReqFilter + filterBuffAirpName;
        AndOfSqlReqChecker = true;
        qDebug()<< sqlReqFilter;
    } else if (filterBuffAirpName != "" && AndOfSqlReqChecker != false){
        sqlReqFilter = sqlReqFilter + " and " + filterBuffAirpName;
        qDebug()<< sqlReqFilter;
    }
    if (filterBuffCountry != "" && AndOfSqlReqChecker == false){
        sqlReqFilter = sqlReqFilter + filterBuffCountry;
        AndOfSqlReqChecker = true;
        qDebug()<< sqlReqFilter;
    } else if (filterBuffCountry != "" && AndOfSqlReqChecker != false){
        sqlReqFilter = sqlReqFilter + " and " + filterBuffCountry;
        qDebug()<< sqlReqFilter;
    }
    if (filterBuffBarAlt != "" && AndOfSqlReqChecker == false){
        sqlReqFilter = sqlReqFilter + filterBuffBarAlt;
        AndOfSqlReqChecker = true;
        qDebug()<< sqlReqFilter;
    } else if (filterBuffBarAlt != "" && AndOfSqlReqChecker != false){
        sqlReqFilter = sqlReqFilter + " and " + filterBuffBarAlt;
        qDebug()<< sqlReqFilter;
    }
    if (filterBuffRW_lngth != "" && AndOfSqlReqChecker == false){
        sqlReqFilter = sqlReqFilter + filterBuffRW_lngth;
        AndOfSqlReqChecker = true;
        qDebug()<< sqlReqFilter;
    } else if (filterBuffRW_lngth != "" && AndOfSqlReqChecker != false){
        sqlReqFilter = sqlReqFilter + " and " + filterBuffRW_lngth;
        qDebug()<< sqlReqFilter;
    }
    if (filterBuffCoord != "" && AndOfSqlReqChecker == false){
        sqlReqFilter = sqlReqFilter + filterBuffCoord;
        AndOfSqlReqChecker = true;
        qDebug()<< sqlReqFilter;
    } else if (filterBuffCoord != "" && AndOfSqlReqChecker != false){
        sqlReqFilter = sqlReqFilter + " and " + filterBuffCoord;
        qDebug()<< sqlReqFilter;
    }

    QSqlQuery query;
    QSqlQueryModel * modal = new QSqlQueryModel();

    query.prepare(sqlReqFilter);
    if(query.exec())
{
        modal->setQuery(query);
        ui->twAirLib->setModel(modal);
        ui->twAirLib->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        ui->twAirLib->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        ui->twAirLib->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
    else{
        query.prepare("select * from AirportsTable");
        query.exec();
        modal->setQuery(query);
        ui->twAirLib->setModel(modal);
        ui->twAirLib->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        ui->twAirLib->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        ui->twAirLib->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
}

void MainWindow::on_btn_ClearFilterBD_clicked()
{
    ui->lineEditBD->setText("");

    ui->twAirLibFilter->takeItem(0,0);
    ui->twAirLibFilter->takeItem(0,1);
    ui->twAirLibFilter->takeItem(0,2);
    ui->twAirLibFilter->takeItem(0,3);
    ui->twAirLibFilter->takeItem(0,4);
    ui->twAirLibFilter->takeItem(0,5);

    filterBuffCodeICAO = "";
    filterBuffAirpName = "";
    filterBuffCountry = "";
    filterBuffRW_lngth = "";
    filterBuffBarAlt = "";
    filterBuffCoord = "";

    if (dbOpenChecker == true){
        QSqlQuery query;
        QSqlQueryModel * modal = new QSqlQueryModel();
        query.prepare("select * from AirportsTable");
        query.exec();
        modal->setQuery(query);
        ui->twAirLib->setModel(modal);
        ui->twAirLib->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        ui->twAirLib->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        ui->twAirLib->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
}

void MainWindow::on_btn_Ch_Dep_Airport_clicked()
{
    QSqlQueryModel * modal = new QSqlQueryModel();
    showBD();
    QSqlQuery* query = new QSqlQuery(db);
    query->prepare("select * from AirportsTable");
    query->exec();
    modal->setQuery(*query);
    ui->twAirLib->setModel(modal);
    ui->twAirLib->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->twAirLib->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->twAirLib->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->btn_Change_Dep_Airp->setEnabled(true);
    ui->btn_Change_Dest_Airp->setEnabled(false);

    currentairportbtn = ui->btn_Ch_Dep_Airport;
    ui->swPages->setCurrentIndex(7);
}

void MainWindow::on_btn_Ch_Dest_Airport_clicked()
{
    QSqlQueryModel * modal = new QSqlQueryModel();
    showBD();
    QSqlQuery* query = new QSqlQuery(db);
    query->prepare("select * from AirportsTable");
    query->exec();
    modal->setQuery(*query);
    ui->twAirLib->setModel(modal);
    ui->twAirLib->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->twAirLib->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->twAirLib->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->btn_Change_Dep_Airp->setEnabled(false);
    ui->btn_Change_Dest_Airp->setEnabled(true);

    currentairportbtn = ui->btn_Ch_Dest_Airport;
    ui->swPages->setCurrentIndex(7);
}

void MainWindow::on_btn_Change_Dep_Airp_clicked()
{
    if(db.open()){
    ui->twAirLib->currentIndex();
    currentairportbtn->setText(ui->twAirLib->currentIndex().data().toString());
    ui->swPages->setCurrentIndex(0);
    if(currentairportbtn->text() != "")
    ui->lblnameDepAirport->setText(currentairportbtn->text());
    ui->btn_Change_Dep_Airp->setEnabled(true);
    ui->btn_Change_Dest_Airp->setEnabled(true);
    MainWindow::IfInitialDataAssigned();
    }
}

void MainWindow::on_btn_Change_Dest_Airp_clicked()
{
    if(db.open()){
    ui->twAirLib->currentIndex();
    currentairportbtn->setText(ui->twAirLib->currentIndex().data().toString());
    ui->swPages->setCurrentIndex(0);
    if(currentairportbtn->text() != "")
    ui->lblnameDestAirport->setText(currentairportbtn->text());
    ui->btn_Change_Dep_Airp->setEnabled(true);
    ui->btn_Change_Dest_Airp->setEnabled(true);
    MainWindow::IfInitialDataAssigned();
    }
}
