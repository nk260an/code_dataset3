#ifndef CUSTOMTYPE_H
#define CUSTOMTYPE_H

#include <QObject>
#include <QUrl>

class CustomType : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(int indentation READ indentation WRITE setIndentation NOTIFY indentationChanged)

public:
    explicit CustomType(QObject *parent = 0);
    CustomType(const CustomType &other);
    ~CustomType();

    QString text();
    void setText(QString text);

    int indentation();
    void setIndentation(int indentation);

    void setUrl(QUrl url);
    QUrl url();
signals:
    void textChanged();
    void indentationChanged();
    void urlChanged();

private:
    QString myText;
    int myIndentation;
    QUrl myUrl;
};

Q_DECLARE_METATYPE(CustomType)

#endif // CUSTOMTYPE_H
