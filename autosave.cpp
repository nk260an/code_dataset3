#include "autosave.h"
#include <QXmlStreamReader>
#include <QFile>
#include <QDir>

void Autosave::load(){
    QDir dir;
    QStringList filter;
    QStringList files;
    QString filename;
    QFile file;
    QXmlStreamReader xmlReader(&file);
    QString ElemName;
    
    filter << "*.xml"; //Расширение файла
    if(!QDir(QDir::currentPath() + "/config").exists()){
        QDir().mkdir(QDir::currentPath() + "/config");
    }
    dir.setPath(QDir::currentPath() + "/config"); //путь к файлам
    if(files.isEmpty()){
        filename = QDir::currentPath() + "/res/default_settings.xml"; //Загрузка параметров по умоляанию
    }else{
        filename = dir.path() + files.last(); //Последний файл конфигураци
    }
    file.setFileName(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text)){
    }
    
    while( !xmlReader.atEnd() )
    {
        xmlReader.readNext();
        if( xmlReader.name() == "SettingsMap" )
        {
            xmlReader.readNext();
            continue;
        }
        if( xmlReader.isEndElement() )
        {
            xmlReader.readNext();
            continue;
        }
        if(xmlReader.isStartDocument()){
            continue;
        }
        ElemName = xmlReader.name().toString();
        xmlReader.readNext();
        map[ElemName] = xmlReader.text().toString();
        xmlReader.readNext();
        xmlReader.readNext();
    }
    file.close();
}
void Autosave::save(){
    QDir dir;
    QStringList filter;
    QStringList files;
    QString filename;
    QFile file;
    QXmlStreamWriter xmlWriter(&file);
    QSettings::SettingsMap::const_iterator mi;
    int maxNumberOfFiles = map["spinSaveDepth"].toInt(); //Максимальное число автосохраненных конфигураций
    
    dir.setPath(QDir::currentPath() + "/config"); //Путь к файлам
    files = dir.entryList(filter, QDir::Filter::Files); //Все файлы в директории
    if(files.size() > maxNumberOfFiles){
        for(int i = 0; i < files.size() - maxNumberOfFiles;i++){
            filename = "autosave_conf" + QString::number(i);
            filename += ".xml";
            dir.remove(filename);
        }
        files = dir.entryList(filter, QDir::Filter::Files);
        for(int i = 0; i < files.size();i++){
            filename = "autosave_conf" + QString::number(i);
            filename += ".xml";
            dir.rename(files[i], filename);
        }
        files = dir.entryList(filter, QDir::Filter::Files);
    } else if (files.size() == maxNumberOfFiles) {
        dir.remove("autosave_conf0.xml");
        for(int i = 1; i < files.size();i++){
            dir.rename(files[i], files[i-1]);
        }
        files = dir.entryList(filter, QDir::Filter::Files);
    }
    filename = dir.path() + "/autosave_conf";
    filename.append(QString::number(files.size()));
    filename.append(".xml");
    file.setFileName(filename);
    if(!file.open(QFile::WriteOnly | QFile::Text)){
    }
    xmlWriter.setAutoFormatting( true );
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement( "SettingsMap" );
    for( mi = map.begin(); mi != map.end(); mi++ )
    {
        xmlWriter.writeStartElement( mi.key() );
        xmlWriter.writeCharacters( mi.value().toString() );
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    file.close();
}
