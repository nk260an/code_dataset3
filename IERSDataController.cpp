#include "IERSDataController.h"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>

using namespace std;

IERSDataController::IERSDataController()
{
	// �������� ������������� ������ ������
	m_filesExists = false;
	for (auto it = file_names.begin(); it != file_names.end(); ++it)
	{
		std::ifstream infile(*it);
		m_filesExists = m_filesExists || infile.good();
	}

	
	if (m_filesExists) // ���� ����� �������
	{
		bool files_Uptodate = false;
		files_Uptodate = true;
		if (files_Uptodate) // ���� ����� ��������� - �������
		{
			if (parse() == 0)
			{
				data_ready = true;
				return;
			}
			else
			{
				data_ready = false;
			}
		}

		download_files();
		// ���� ������ ��� ��� ��� �������� - ��������

	}
	else {

	}
}


IERSDataController::~IERSDataController()
{
}

// ��������� � ���������� ftp://hpiers.obspm.fr/iers/bul/bulb_new/bulletinb.pdf
// Bulletin A is issued weekly by the IERS Rapid Service / Prediction Center at the U.S.Naval Observatory, Washington, DC.
// It provides the most recently observed estimates as well as predictions of Earth Orientation Parameters(EOPs).
// Bulletin B / C04 is issued monthly by the IERS Earth Orientation Centre(EOC) at the Paris Observatory, and
// provides daily data covering a period from several decades to 30 days previous to the date of publication.
//
// Bulletin A:
//
// finals2000A.all -- all EOP values since 02 January 1973
// with dX & dY using IAU2000A Nutation/Precession Theory
// (with 1 year of predic tions)
// https://datacenter.iers.org/eop/-/somos/5Rgv/latest/9
//
// �������:
// http://toshi.nofs.navy.mil/ser7/finals2000A.all updated weekly (Thursday), prior to 1 July 2002 (Tuesday and Thursday)
// http://maia.usno.navy.mil 
// ftp://maia.usno.navy.mil 
// ftp://toshi.nofs.navy.mil 
//
// finals2000A.data -- polar motion, UT1-UTC,
// and length of day since 01 January 1992
// with dX & dY using IAU2000A Nutation/Precession Theory
// (with 1 year of predictions)
// https://datacenter.iers.org/eop/-/somos/5Rgv/latest/10
//
// �������:
// http://toshi.nofs.navy.mil/ser7/finals2000A.data updated weekly (Thursday), prior to 1 July 2002 (Tuesday and Thursday)
// http://maia.usno.navy.mil 
// ftp://maia.usno.navy.mil 
// ftp://toshi.nofs.navy.mil 
//
// finals2000A.daily -- polar motion, UT1-UTC, and length of day values
// for last 90 days with dX & dY using IAU2000A Nutation/Precession Theory
// (with 90 days of predictions)
// https://datacenter.iers.org/eop/-/somos/5Rgv/latest/13
//
// �������:
// http://toshi.nofs.navy.mil/ser7/finals2000A.daily
// http://maia.usno.navy.mil 
// ftp://maia.usno.navy.mil 
// ftp://toshi.nofs.navy.mil 

// TODO ������� ������
// TODO �������� md5 �����

int IERSDataController::update_files()
{
	bool is_old_file = true;
	bool is_file_exist = false;
	// ���� ������ ��� - �������
	// ���� ���� ������ - �������
	// �������� ����� �� MD5

	//finals2000A.all
	//finals2000A.data
	//finals2000A.daily

	if (is_old_file || !is_file_exist)
	{
		// TODO ������
	}
	return 0;
}

// ������ ���� ������� ������������ URLDownloadToFile �� <Urlmon.h>
int IERSDataController::download_files(
	const std::string& host,
	const std::string& uri,
	const std::string& savefilepath)
{
	try
	{
		ofstream fs;
		fs.open(savefilepath,
			std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);

		boost::asio::io_context io_context;

		HttpAsioClient clnt(io_context);
		clnt.download_file("toshi.nofs.navy.mil", "/ser7/finals.daily", &fs);
		io_context.run();
		return 0;
	}
	catch (std::exception& e)
	{
		std::cout << "Exception: " << e.what() << "\n";
		return -1;
	}

}
