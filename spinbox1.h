#ifndef SPINBOX1_H
#define SPINBOX1_H

#include <QSpinBox>
#include <QMouseEvent>
#include <keyboardsurface.h>

class SpinBox1 : public QSpinBox
{
public:
    virtual void focusInEvent(QFocusEvent *e){
        keyb = new KeyboardSurface(this);
        keyb->show(this);
    }
    SpinBox1(QWidget *parent = 0);
    KeyboardSurface * keyb;
};

#endif // SPINBOX1_H
