#ifndef NFC_CONTROLLER_H
#define NFC_CONTROLLER_H

#include <QObject>
#include <QDebug>

#include <QNearFieldManager>
#include <QNdefRecord>
#include <QNearFieldTarget>
#include <QNdefNfcTextRecord>
#include <QNdefMessage>
#include <QCryptographicHash>

class NFC_Controller : public QObject
{
    Q_OBJECT
public:
    QByteArray cypher(QString Pass);
    explicit NFC_Controller(QObject *parent = nullptr);
    QString UID_num;
    QString NDEF_num;

signals:
    void authYes();
    void authNot();
    void authUID();
    void authUIDYes();
    void authUIDNot();
public slots:
    void auth(QString Log, QString Pass);
    void auth(QString passuid);
    void startListening();
    void stopListening();
    void ndefMessageRead(const QNdefMessage &message);
    void targetError(QNearFieldTarget::Error error, const QNearFieldTarget::RequestId &id);
    void targetDetected(QNearFieldTarget *target);
    void targetLost(QNearFieldTarget *target);
private:
    enum TouchAction {
        NoAction,
        ReadNdef,
        WriteNdef
    };

    QNdefMessage ndefMessage() const;
    QNearFieldManager * m_manager;
    TouchAction m_touchAction;
    QNearFieldTarget::RequestId m_request;

};

#endif // NFC_CONTROLLER_H
