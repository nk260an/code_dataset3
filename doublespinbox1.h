#ifndef SDOUBLEPINBOX1_H
#define SDOUBLEPINBOX1_H
#include <QDoubleSpinBox>
#include <QMouseEvent>
#include <keyboardsurface.h>
class DoubleSpinBox1 : public QDoubleSpinBox
{
public:
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE
    {
        keyb = new KeyboardSurface(parentWidget());
        keyb->show(this);
    }
    virtual void focusInEvent(QFocusEvent *e){
        keyb = new KeyboardSurface(this);
        keyb->show(this);
    }

    DoubleSpinBox1(QWidget *parent = 0);
    KeyboardSurface * keyb;
};

#endif // DOUBLESPINBOX1_H
