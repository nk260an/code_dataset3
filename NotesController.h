#ifndef NOTESLISTCONTROLLER_H
#define NOTESLISTCONTROLLER_H
//#include "model.h"
#include <QDate>
#include <QTime>

#include <QAbstractListModel>
#include <QStringList>
#include <QVariant>
#pragma once

#define ATCH_TEXT 0x01
#define ATCH_SCREEN 0x02
#define ATCH_PHOTO 0x04
#define ATCH_AUDIO 0x08
#define ATCH_VIDEO 0x10

enum class enmNoteIconClass
{
    ATCH_DEFAULT = 0x01,
    ATCH_CORRECTION = 0x02,
    ATCH_MISTAKE = 0x04,
    ATCH_CRITICAL_MISTAKE = 0x08
};


class NoteObject
{
public:
    NoteObject(const QString &datetime,
               const QString &caption,
               const QString &reference,
               const QString &path,
               const int &atch);

    QString dateTime() const;
    QString caption() const;
    QString reference() const;
    QString path() const;
    bool isText() const;
    bool isScreenshot() const;
    bool isAudio() const;
    bool isVideo() const;
    bool isPhoto() const;

private:
    QString m_datetime;
    QString m_caption;
    QString m_reference;
    QString m_path;
    bool m_text;
    bool m_screenshot;
    bool m_audio;
    bool m_photo;
    bool m_video;
//![1]
};

// несмотря на то, что в группе делали модель на QList, оказалось, что она не обновляет ListView в GUI
// поэтому создали аналогичный класс, наследованный от QAbstractListModel

class NotesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataRoles {
        DatetimeRole = Qt::UserRole + 1,
        CaptionRole,
        ReferenceRole,
        PathRole,
        TextIncludedRole,
        ScreenshotIncludedRole,
        AudioIncludedRole,
        PhotoIncludedRole,
        VideoIncludedRole
    };

    NotesModel(QObject *parent = 0);

    void addNote(const NoteObject & newNote);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;
    void clear();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<NoteObject> m_note_items;

};

class NotesListController : public QObject
{
    Q_OBJECT
public:
    NotesListController();

    NotesModel notesModel;
    void loadNotes();
    void saveXML(const NoteObject &newNote);
public slots:
    void exampleClearList(); // код очистки ListView для Богомолова
    void addNote(QString datetime,
                QString caption,
                QString reference,
                QString path,
                int isText,
                int isScreenshot,
                int isAudio,
                int isPhoto,
                int isVideo);
};

#endif // NOTESLISTCONTROLLER_H
