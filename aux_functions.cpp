#include <math.h>
#include "aux_functions.h"

double getLinearInterpolationScalar1D( double const dblX,
                                       double const dblX1,
                                       double const dblX2,
                                       double const dblY1,
                                       double const dblY2)
/* +++функция для линейной одномерной интерполяции */
{
    return dblY1 + (dblY2-dblY1)/(dblX2-dblX1)*(dblX - dblX1);
}

double getLinearInterpolationVector1D(double const dblXArray[], // массив значений аргумента, для которых известно значение функции; массив должен быть монотонно возрастающий
                                      double const dblYArray[], // массив значений функции для аргументов dblXArray[]
                                      double const dblX, // аргумент, для которого нужно проинтерполировать функцию
                                      int const intImax) // максимальный индекс массивов, [длина массива - 1]
/* +++фукнция для линейной одномерной интерполяции в массиве */
{
    if( (dblX < dblXArray[0]) || ( dblXArray[intImax] < dblX) ) // если аргумент за пределами шкалы dblXArray  - ошибка
    {
        // возвращение кода ошибки

        // возвращение нулевого указателя
        return 0.0;
    }
    else
    {
        // прогноз по индексу (линейная интерполяция i, вместо неё можно метод половинного деления)
        int i = 0;

        // округление в меньшую сторону
        i = (int)floor( intImax * (dblX - dblXArray[0])/(dblXArray[intImax] - dblXArray[0]) );

        // уточнение i
        while(!( (dblX >= dblXArray[i]) && (dblX <= dblXArray[i+1]) ))
        {
            if(dblX < dblXArray[i])
            {
                i--;
            }
            else
            {
                i++;
            }
        }

        //интерполяция между значениями с рассчитанными индексами i и i+1
        return getLinearInterpolationScalar1D(dblX,
                                              dblXArray[i],
                                              dblXArray[i+1],
                dblYArray[i],
                dblYArray[i+1]);
    }

}

/*+++ функция для вычисления квадрата гипотенузы */
double getHypotenuse2(double const dblX,
                      double const dblY)
{
    return dblX*dblX - dblY*dblY;
}

double getRmin(double const dblXArray[], // массив значений аргумента, для которых известно значение функции; массив должен быть монотонно возрастающий
               double const dblYArray[], // массив значений функции для аргументов dblXArray[]
               double const dblX, // координата X точки, до которой нужно определить расстояние
               double const dblY, // координата Y точки, до которой нужно определить расстояние
               int const intImax) // максимальный индекс массивов, [длина массива - 1]
/* +++Фукнция для определения минимального расстояния от точки до кривой */
{
    double r1 = 0.0;
    double r2 = 0.0;
    for(int i = 0; i<=intImax; i++)
    {
        r2 = getHypotenuse2(dblXArray[i] - dblX, dblYArray[i] - dblY); // квадрат расстояния

        if((r2 <= r1)||(i == 0)) // если расстояние уменьшается (или если точка кривой первая)
        {
            r1 = r2;
        }
        else // если расстояние начинает увеличиваться - значит нужный i найден
        {
            // более точный расчёт высоты (по перпендикуляру)
            double r = 0.0;
            double A[3] = {0.0, 0.0, 0.0};
            A[0] = dblX;
            A[1] = dblY;

            double P1[3] = {0.0, 0.0, 0.0};
            double P2[3] = {0.0, 0.0, 0.0};

            if(i<intImax) // если точка не последняя - перпендикуляр от {dblX, dblY} к i, i+1 точкам кривой
            {
                P1[0] = dblXArray[i];
                P1[1] = dblYArray[i];

                P2[0] = dblXArray[i+1];
                P2[1] = dblYArray[i+1];
            }
            else // если точка последняя - от {dblX, dblY} к i-1, i точкам кривой
            {
                P1[0] = dblXArray[i-1];
                P1[1] = dblYArray[i-1];

                P2[0] = dblXArray[i];
                P2[1] = dblYArray[i];
            }
            r = getDistanceLinePoint(A, P1, P2); // высота равна модулю векторного произведения

            // выход
            return r;
        }
    }
}

double getDistanceLinePoint(double const A[3], // точка, до которой определяется расстояние, [x, y, z]
double const P1[3], // первая точка кривой, [x1, y1, z1]
double const P2[3]) // вторая точка кривой, [x2, y2, z2]
/* +++расстояние от точки до прямой, заданной двумя точками */
{
    double V1[3] = {0.0, 0.0, 0.0}; // вектор P1->A
    V1[0] = A[0] - P1[0];
    V1[1] = A[1] - P1[1];
    V1[2] = A[2] - P1[2];

    double V2[3] = {0.0, 0.0, 0.0};// вектор P1->P2
    V2[0] = P2[0] - P1[0];
    V2[1] = P2[1] - P1[1];
    V2[2] = P2[2] - P1[2];

    return 0/*abs(V1[0] * V2[1] - V1[1] * V2[0]) / sqrt(V2[0]*V2[0] + V2[1]*V2[1])*/;
}
