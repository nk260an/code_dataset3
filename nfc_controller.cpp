#include "nfc_controller.h"
#include <QEventLoop>

QByteArray NFC_Controller::cypher(QString Pass)
{
    Pass = Pass + "141026bfc89eda81cb723b4d33b7d8f49a83bb2fd6582c627e95c11afb5bb7bd";
    QByteArray Password = Pass.toUtf8();

    return (QCryptographicHash::hash(Password, QCryptographicHash::Sha256));
}

NFC_Controller::NFC_Controller(QObject *parent) : QObject(parent)
{
// TODO определить включена ли антенна и предложить её включить

// связать сигналы nfcmanager со слотами
    m_manager = new QNearFieldManager(this);
    m_touchAction = NoAction;
    connect(m_manager, SIGNAL(targetDetected(QNearFieldTarget*)),
            this, SLOT(targetDetected(QNearFieldTarget*)));
    connect(m_manager, SIGNAL(targetLost(QNearFieldTarget*)),
            this, SLOT(targetLost(QNearFieldTarget*)));

    startListening();

}

void NFC_Controller::auth(QString Log, QString Pass)
{
    qDebug() << "*** Сработал слот Auth" << "Log " << Log << "Pass " << Pass;
    qDebug() << "*** " << cypher(Pass).toHex();


    if((cypher(Pass).toHex() == ("72e2640cac4d67dab3ad99c8232814755194e4dc9bbc9143eaf347d44f26b243"))&&(Log == "login"))
    {
       emit authYes();
    }
    else
    {
        emit authNot();
    }

}

void NFC_Controller::auth(QString passuid)
{
    passuid+=(UID_num+"72e2640cac4d67dab3ad99c8232814755194e4dc9bbc9143eaf347d44f26b243");
    qDebug() << "*** Сработал слот Auth с UID " << "UID+Pass+Соль " << passuid ;
    qDebug() << "*** cyphered UID+Pass+Соль" << cypher(passuid).toHex();
    qDebug()<< "*** NDEF = "<< NDEF_num;

    if(cypher(passuid).toHex() == ("e3a1a78e9e1222cb91c0b26a20c9c4719cfe02524ebfdecc9ec9fae44606afad"))
    {
        emit authUIDYes();
    }
    else
    {
        emit authUIDNot();
    }

}

// слот, который срабатывает при считывании
void NFC_Controller::targetDetected(QNearFieldTarget *target) // класс для доступа к данным карты
{
    qDebug() << "*** Сработал слот targetDetected";
    //qDebug() << "*** NearFieldTarget:";



        connect(target, SIGNAL(ndefMessageRead),
                this, SLOT(ndefMessageRead));
        connect(target, SIGNAL(error()),
                this, SLOT(targetError));

        m_request = target->readNdefMessages();


        if (!m_request.isValid()) // cannot read messages
            targetError(QNearFieldTarget::NdefReadError, m_request);



    qDebug()<< "*** UID "<< target->uid().toHex();

    /*QEventLoop loop;
    connect()
    loop.exec();*/

    UID_num = target->uid().toHex();

    emit authUID();
}

//TODO получить данные с карты и обработать
    //  ... и сразу считает и сравнивает хэши


void NFC_Controller::targetLost(QNearFieldTarget *target)
{
    qDebug() << "*** Сработал слот targetLost";
    target->deleteLater();
}

void NFC_Controller::startListening()// функция переводящая при аутентфикации класс в считывание
{
    qDebug() << "*** startListening()";
    m_touchAction = ReadNdef;
    m_manager->setTargetAccessModes(QNearFieldManager::NdefReadTargetAccess);
    m_manager->startTargetDetection();  //переводит в режим считывания
}

void NFC_Controller::stopListening()
{
    qDebug() << "*** stopListening()";
    m_touchAction = NoAction;
    m_manager->setTargetAccessModes(QNearFieldManager::NoTargetAccess);
    m_manager->stopTargetDetection();  // выключает режим считывания
}

void NFC_Controller::ndefMessageRead(const QNdefMessage &message)
{
    qDebug() << "*** QNdefMessage : " << message.toByteArray();
    foreach (const QNdefRecord &record, message) // поэлементно разобрать состав record
    {
        if (record.isRecordType<QNdefNfcTextRecord>())
        {
            //addRecord<TextRecordEditor>(ui, record);
            qDebug() << "*** record.isRecordType<QNdefNfcTextRecord>";
            qDebug() << "*** record" << record.payload();
            NDEF_num = record.payload();

            // TODO посчитать хэш SHA2(серийный номер + пароль)
            // и сравнить с данными из карты
            // if (SHA2(серийный номер + пароль [+ соль ]) == тексту с карты)
            //         доступ разрешён
        }
        /*else if (record.isRecordType<QNdefNfcUriRecord>()) {
            addRecord<UriRecordEditor>(ui, record);
        }
        else if (record.typeNameFormat() == QNdefRecord::Mime &&
                   record.type().startsWith("image/")) {
            addRecord<MimeImageRecordEditor>(ui, record);
        }
        else if (record.isEmpty())
        {
            addRecord<EmptyRecordLabel>(ui);
        }*/
        else {
            qDebug() << "*** invalid record";
            //addRecord<UnknownRecordLabel>(ui, record);
        }
    }

    m_manager->setTargetAccessModes(QNearFieldManager::NoTargetAccess);

    m_manager->stopTargetDetection();

    m_request = QNearFieldTarget::RequestId();

    emit authUID();

}

void NFC_Controller::targetError(QNearFieldTarget::Error error,
                                 const QNearFieldTarget::RequestId &id)
{
    /*Q_UNUSED(error);
    Q_UNUSED(id);

    if (m_request == id) {
        switch (error) {
        case QNearFieldTarget::NoError:
            ui->statusBar->clearMessage();
            break;
        case QNearFieldTarget::UnsupportedError:
            ui->statusBar->showMessage(tr("Unsupported tag"));
            break;
        case QNearFieldTarget::TargetOutOfRangeError:
            ui->statusBar->showMessage(tr("Tag removed from field"));
            break;
        case QNearFieldTarget::NoResponseError:
            ui->statusBar->showMessage(tr("No response from tag"));
            break;
        case QNearFieldTarget::ChecksumMismatchError:
            ui->statusBar->showMessage(tr("Checksum mismatch"));
            break;
        case QNearFieldTarget::InvalidParametersError:
            ui->statusBar->showMessage(tr("Invalid parameters"));
            break;
        case QNearFieldTarget::NdefReadError:
            ui->statusBar->showMessage(tr("NDEF read error"));
            break;
        case QNearFieldTarget::NdefWriteError:
            ui->statusBar->showMessage(tr("NDEF write error"));
            break;
        default:
            ui->statusBar->showMessage(tr("Unknown error"));
        }

        ui->status->setStyleSheet(QString());
        m_manager->setTargetAccessModes(QNearFieldManager::NoTargetAccess);
        m_manager->stopTargetDetection();
        m_request = QNearFieldTarget::RequestId();
    }*/
}
