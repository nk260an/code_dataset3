#ifndef LANDING_H
#define LANDING_H

#endif // LANDING_H
#include "common.h"
double getLDA_grip_correction(double const dblGripCoefficient); //параметр - нормативный коэффицент сцепления на ВПП, [безразмерный]
/*RRJ-95B РЛЭ, Рисунок 1.03.25 Коэффициент коррекции длины посадочной дистанции (pdf с. 183)*/

double getALD(double const dbl_weight, // посадочный вес, [кг]
               FlapsConf const enmFlapsConf, // конфигурация механизации крыла
               BrakingMode const enmBrakingMode, // режим торможения
               double const dblAltitude, // высота, [фт]
               double const dblWind, // попутный ветер, [уз.]
               double const dblTemperature); // температура, [град.C]
/*  Расчёт фактической посадочной дистанции,
    документ: RRJ-95B РЛЭ, 1.03.40, стр.14, pdf. С. 184  */

double getVref(double const dbl_weight, // посадочный вес, [кг]
               FBWCS_mode const enmFBWCSmode, // режим ЭДСУ
               FlapsConf const enmFlapsConf); // конфигурация механизации крыла
/*  посадочная скорость,
    документ: RRJ-95B РЛЭ, раздел 1.03.40, с.15-16, Таблицы 1.03.1-2,  pdf. с. 185-186 */

double getVmin(double const dbl_weight, // посадочный вес, [кг]
               FlapsConf const enmFlapsConf); // режим ЭДСУ);
/* минимально допустимая скорость полета при полном отклонении бру на кабрирование,
    документ: RRJ-95B РЛЭ, раздел 1.03.40 стр.19, Таблица 1.03.3, pdf с. 189*/

double getVsr(double const dbl_weight, // вес самолёта, 30 000...45 000[кг]
              double const dbl_h, // высота полёта, <40 000 [фт]
              FlapsConf const enmFlapsConf); // конфигурация механизации
/*  скорость сваливания
    документ: RRJ-95B РЛЭ, раздел 1.03.40 стр.20-22, Рисунки 1.03.29-31, pdf с. 190-192 */

double getLandingWeight(double const dblBarAlt, // барометрическая высота аэродрома, [фт]
                         double const dblTemperature, // Температура воздуха на аэродроме, [град С]
                         EngineBleedAirMode const enmBleedAir); // режим отбора воздуха из двигателя
/* Определение максимальной посадочной массы, ограниченной градиентом набора высоты 2,1% при уходе на второй круг
    документ: RRJ-95B РЛЭ, раздел 1.03.40 с.2-3, рисунок 1.03.18-19, pdf с. 172-73 */

double getLandingWeightNminus1(double const dblBarAlt, // барометрическая высота аэродрома, [фт]
                 double const dblTemperature, // Температура воздуха на аэродроме, [град С]
                 EngineBleedAirMode const enmBleedAir); // режим отбора воздуха из двигателя
/* Масса самолёта, ограниченная полным градиентом набора высоты при уходе на второй круг с одним неработающим двигателем
    документ: RRJ-95B РЛЭ, раздел 1.03.40 с.5-7, рисунок 1.03.22-22, pdf с. 175-177 */

double getRLD(double const dblBarAlt, // барометрическая высота аэродрома, [фт]
                 double const dblTemperature, // Температура воздуха на аэродроме, [град С]
                 double const dblLandingWEight,
                 EngineBleedAirMode const enmBleedAir); // режим отбора воздуха из двигателя
/* Ограничение посадочной массы по располагаемой посадочной дистанции
    документ: RRJ-95B РЛЭ, раздел 1.03.40 с.9-11, рисунок 1.03.23-24, pdf с. 179-181 */

double getVmax(FlapsConf const enmFlapsConf);
/* pdf с. 2440 */

double getM_max(double const dblTemperature,
             double const dblBarAltRW,
             double const dblALD,
             double const dbl_RWslope,
             double const dblWind,
             double const dblSafetyCoeff);
