#include <QGuiApplication>
#include <QQmlApplicationEngine>
//#include <QtWebView>
#include <QQmlContext>
#include <QQmlEngine>
#include "mytypes.h"
#include "OpenedController.h"
#include <QDir>
#include "documenthandler.h"
#include "NotesController.h"
#include "FavouriteController.h"
#include "TimetableController.h"
#include "ContensController.h"
#include "RecentController.h"
#include <QZXing.h>
#include "NotificationController.h"
#include "ftssearch.h"
#include "treemodel.h"
#include "customtype.h"
#include "nfc_controller.h"

#ifdef __MINGW64__
    #include "speechrec.h"
#elif _MSC_VER
    #include "speechrec.h"
#endif


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    NotesListOrganizer objNotesListOrganizer;

    TimetableController timetableController;

    // TODO заполнить модель пробными данными

    ContensController contentsController;
    ftssearch model1;

    #ifdef __MINGW64__
        speechrec speechr;
    #elif _MSC_VER
        speechrec speechr;
    #endif

	NotificationController model2;
    NFC_Controller nfc_controller;


    QZXing::registerQMLTypes();
    qmlRegisterType<NoteAttachmentClass>("com.rkkenergy", 1, 0, "NoteAttachmentClass");
    qRegisterMetaType<NoteAttachmentClass::enmNoteAttachment>("NoteAttachmentClass::enmNoteAttachment");

    qmlRegisterType<NoteIconClass>("com.rkkenergy", 1, 0, "NoteIconClass");
    qRegisterMetaType<NoteIconClass::enmNoteIconClass>("NoteIconClass::enmNoteIconClass");
    qmlRegisterType<DocumentHandler>("DocumentHandler", 1, 0, "DocumentHandler");




    qmlRegisterType<NotesListOrganizer>("com.rkkenergy", 1, 0, "NotesListOrganizer");

    OpenedController openedController;

    NotesListController notesListController;

    RecentController recentController;

    FavouriteController favouriteController;

    /*********************************************************************************/
    /********** ДО СОЗДАНИЯ ОБЪЕКТА ДВИЖКА - НИКАКОГО ВЗАИМОДЕЙСТВИЯ С QML ***********/
    /*********************************************************************************/

    QQmlApplicationEngine engine;

    /*********************************************************************************/
    /*** ПОСЛЕ СОЗДАНИЯ ОБЪЕКТА ДВИЖКА - РЕГИСТРИРОВАТЬ ОБЪЕКТЫ И КЛАССЫ К КОНТЕКСТЕ */
    /*********************************************************************************/

    // зарегистрировать в QML модель списка заметок из C++

    QQmlContext *ctxt = engine.rootContext();

    #ifdef __MINGW64__
        ctxt->setContextProperty("speechr", &speechr);
    #elif _MSC_VER
        ctxt->setContextProperty("speechr", &speechr);
    #endif

    ctxt->setContextProperty("notesModel",
                             &(notesListController.notesModel));

    ctxt->setContextProperty("modelTimetable",
                             &(timetableController.timetableModel));

    ctxt->setContextProperty("verticalContentsList",
                             &(contentsController.verticalContentsList));
    ctxt->setContextProperty("horizontalContentsList",
                             &(contentsController.horizontalContentsList));
    ctxt->setContextProperty("recentController", // название, под которым объект будет виден из qml
                             &(recentController.recentModel)); // отсылка к c++ объекту

    ctxt->setContextProperty("openedController",
                             &(openedController.openedModel));

    ctxt->setContextProperty("favouriteController",
                             &(favouriteController.favouriteModel));

    ctxt->setContextProperty("ftsList",
                             &(model1.ftsList));
	ctxt->setContextProperty("notificationList",
        &(model2.notificationList));
    //model1.addContact(Contact("Mr. User 1","123-45-67",24));
    //ctxt->setContextProperty("myModel", &model1);


    //Q_INIT_RESOURCE(treeview);




    TreeModel model;


    // QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("theModel",&model);
 //   engine.rootContext()->setContextProperty("theModel",&model1);
    qmlRegisterType<CustomType>("hvoigt.net", 1, 0, "CustomType");


    engine.rootContext()->
            setContextProperty("nfc_controller",
                               &nfc_controller);
    engine.rootContext()->
            setContextProperty("objNotesListOrganizer",
                               &objNotesListOrganizer);
    engine.rootContext()->
            setContextProperty("applicationDirPath",
                               QGuiApplication::applicationDirPath());

    /***************************************************************************************/
    /*** ДО ЗАПУСКА QML ИНТЕРФЕЙСА НА ДВИЖКЕ - РЕГИСТРИРОВАТЬ ОБЪЕКТЫ И КЛАССЫ К КОНТЕКСТЕ */
    /***************************************************************************************/

    engine.load(QUrl(QLatin1String("qrc:///main.qml"))); // один слеш - проблемы с URL, обрабатываемыми в коде QML

    /***************************************************************************************/
    /*** ПОСЛЕ ЗАПУСКА QML ИНТЕРФЕЙСА НА ДВИЖКЕ - СВЯЗЫВАТЬ СИГНАЛЫ QML СО СЛОТАМИ C++ *****/
    /***************************************************************************************/

    QObject * rootObject = engine.rootObjects().first();
    QObject::connect(rootObject, SIGNAL(signalAddNote(QString, QString, QString, QString, int, int, int, int, int)),
                     &notesListController, SLOT(addNote(QString, QString, QString, QString, int, int, int, int, int)));

    QObject::connect(rootObject, SIGNAL(signalSetCurrentContentsItem(int)),
                     &contentsController, SLOT(slotSetCurrentContentsItem(int)));


    QObject::connect(rootObject, SIGNAL(searchSignal(const QString)),
                     &model, SLOT(cppSlot(const QString)));


    QObject::connect(rootObject, SIGNAL(ftssearchSignal(const QString)),
                     &model1, SLOT(ftsSlot(const QString)));
	QObject::connect(rootObject, SIGNAL(notifSignal(const int)),
        &model2, SLOT(notifSlot(const int)));
    QObject::connect(rootObject, SIGNAL(authData(QString, QString)),
                         &nfc_controller, SLOT(auth(QString, QString)));

    QObject::connect(rootObject, SIGNAL(cardpass(QString)),
                         &nfc_controller, SLOT(auth(QString)));

    QObject::connect(rootObject, SIGNAL(startNFC()),
                         &nfc_controller, SLOT(startListening()));
    QObject::connect(rootObject, SIGNAL(speechrec(bool)),
                         &speechr, SLOT(speechSlot(bool)));
    return app.exec();
}
