#include "listsurface.h"
#include <QComboBox>

ListSurface::ListSurface(QWidget *parent)
{
    _parent = parent;
    frame = new QFrame(parent->parentWidget());
    
    lstview = new QListView();
    lstview->setStyleSheet("padding: 0px;color: white;margin: 0px;font-size: 30px;background: #08C;");
    
    grid = new QGridLayout(frame);
    int size = ((QComboBox*)_parent)->height() * 3.5;
    
    if((((QComboBox*)_parent)->y() - size / 3) < parent->parentWidget()->y()){
        frame->setGeometry(((QComboBox*)_parent)->x(), parent->parentWidget()->y(), ((QComboBox*)parent)->geometry().width(), size);
    }else{
        if((((QComboBox*)parent)->y() - size / 3 + size) > parent->parentWidget()->y() + parent->parentWidget()->height()){
            frame->setGeometry(((QComboBox*)_parent)->x(), parent->parentWidget()->y()+ parent->parentWidget()->height() - size, ((QComboBox*)parent)->geometry().width(), size);
        } else{
        frame->setGeometry(((QComboBox*)_parent)->x(), ((QComboBox*)_parent)->y() - size / 3, ((QComboBox*)parent)->geometry().width(), size);
    }
    }
    connect(lstview, SIGNAL(clicked(QModelIndex)), this, SLOT(Ex()));
    grid->addWidget(lstview, 0,0,1,1);

    QFrame::hide();
}
void ListSurface::show(QObject *sender){
    QAbstractItemModel *mdl = ((QComboBox*)_parent)->model();
    lstview->setModel(mdl);
    frame->show();
}
void ListSurface::Ex(){
    ((QComboBox*)_parent)->setCurrentIndex(lstview->currentIndex().row());
    frame->hide();
}
