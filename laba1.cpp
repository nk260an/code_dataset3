// 18.09.2017


#include "stdafx.h" // ��� ���������� ������� ������ ����������� � ���������� �����
// ������ uses (�������) ���� include stdafx.h ��������� ���� ���� ����������� ����������� � �������������
// .h - ���������
#include <iostream>
#include <bitset>
#include <cmath>
// � ������� ������� - �����, ������� ������� ������ � ��������� ������ (������������ ������)
//� "��������" - �����


int main() // ����� ����� (����� ������) ��������� � ���� �������
		   // ������� - �������� ����, ������� ����������� ��� (��������������)
{
	std::cout << "hello world\n";
	// esc-������������������ - ��������� � ��������
	// ���������� � ������ ����� "\"
	// � ������ ��������� ����� ��������� �������������� ������
	std::cout << "hello world" << std::endl;
	// ������ endl � �������� ����� ������������ ��� �� ����� ������ "\n" (������� �� ��������� ������)
	// � ��������� �������� ��������� ������ "\r\n" (r - return, n - newline)

	std::cout << "sizeof(int) = " << sizeof(int) << std::endl;
	// ������ cout ������ � ���������� << ��������� �������� ������������ ��� ������, ��� � ���������� ������ �����, �� �������� � �������������� �� � ������
	// ��� ������, ����� �������, ������������ ������ ����������

	std::cout << "sizeof(short) = " << sizeof(short) << std::endl;
	std::cout << "sizeof(char) = " << sizeof(char) << std::endl;
	std::cout << "sizeof(float) = " << sizeof(float) << std::endl;
	std::cout << "sizeof(double) = " << sizeof(double) << std::endl;

	std::cout << "sizeof(long int) = " << sizeof(long int) << std::endl;
	std::cout << "sizeof(long) = " << sizeof(long) << std::endl;
	std::cout << "sizeof(long long) = " << sizeof(long long) << std::endl;
	std::cout << "sizeof(long long int) = " << sizeof(long long int) << std::endl;

	std::cout << "sizeof(true) = " << sizeof(true) << std::endl;
	std::cout << "sizeof(false) = " << sizeof(false) << std::endl;
	std::cout << "sizeof(bool) = " << sizeof(bool) << std::endl;

	int a = INT_MAX; // 1) ���������� ���������� � ���� 
					 // 2) �������������
	std::cout << "a = " << std::bitset<32>(a) << std::endl;

	int b = 0b10000000000000000000000000000000;
	// ���� ��������� ����� 0b
	// ���� � ����������� ��������� ������ (�������������) ����� ��������� ������� - ���������� ������������ � ���������� ���������� ������������� �����

	unsigned int b_without_sign = 0b10000000000000000000000000000000; // ���� ��� ���������� unsigned,  ����� �� ���������� ������������� � ��� ���������� �������� �����������
	int oct = 0100;
	// ���� �������� � ������������ ���� 0
	int hex = 0xff;
	// ���� � ����������������� ���� 0x


	std::cout << "b = " << b << std::endl;

	std::cout << "b_without_sign = " << b_without_sign << std::endl;

	std::cout << "oct = " << oct << std::endl;
	std::cout << "hex = " << hex << std::endl;



	// ��������� �� ����� �������� � ���������� ������ � ���� � ���������� �������
	// � ���������� ���� 16 � 32 ��������� ������ ������, ���� �� ���������� char ��� bool �� ������� ������� ����� ��������

	getchar(); // �������� ����� ������������ (1 ������)
	return 0; // ������� �� main  �������� ���������� ������ ����������
}

// ����� ����������� ����� ������������ � ���������� � �������� ����