#ifndef AUTOSAVE_H
#define AUTOSAVE_H

#include <QSettings>
#include <QMap>
class Autosave
{
public:
    Autosave(){}
    void load();
    void save();
     QMap<QString, QVariant> map;
};



#endif // AUTOSAVE_H
