#ifndef OPENEDTROLLER_H
#define OPENEDCONTROLLER_H

#include <QObject>

#include <QDateTime>
#include <QUrl>
#include <QAbstractListModel>

class OpenedObject
{
public:
    OpenedObject(QDateTime p_datetime,
                 QUrl p_url);

    QDateTime datetime() const;

    QUrl url() const;

private:
    QDateTime m_datetime;
    QUrl m_url;
};

class OpenedModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataRoles {
        DatetimeRole = Qt::UserRole + 1,
        UrlRole
    };

    OpenedModel(QObject *parent = 0);

    void addOpened(const OpenedObject & newOpened);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;
    void clear();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<OpenedObject> m_opened_items;

};


class OpenedController : public QObject
{
    Q_OBJECT
public:
    explicit OpenedController(QObject *parent = nullptr);
    OpenedModel openedModel;
signals:

public slots:
};

#endif // OPENEDCONTROLLER_H
