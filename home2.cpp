// home2.cpp: ���������� ����� ����� ��� ����������� ����������.
// �������

#include "stdafx.h"
#include <iostream>


void slozenie_matrix(int matr[3][3], int matr1[3][3], int matr2[3][3])
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			matr[i][j] = matr1[i][j] + matr2[i][j];
		}
	}
}

void otvet_slozenie_matrix(int matr[3][3])
{
	std::cout << "slozenie matrix" << std::endl;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << matr[i][j] << " \t";
		}
		std::cout << std::endl;
	}
}





void slozenie_vectorov(int vect[3][1], int vect1[3][1], int vect2[3][1])
{
    for (int j = 0; j < 3; j++)
	{
		vect[0][j] = vect1[0][j] + vect2[0][j];
	}
}

void otvet_slozenie_vectorov(int vect[3][1])
{
	std::cout << "slozenie vectorov" << std::endl;
	for (int j = 0; j < 3; j++)
	{
		std::cout << vect[0][j] << " \t";
	}
}





void umnozenie_matrix(int matab[3][3], int mata[3][3], int matb[3][3])
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			matab[i][j] = mata[i][j] * matb[i][j];
		}
	}
}

void otvet_umnozenie_matrix(int matab[3][3])
{
	std::cout << "umnozenie matrix" << std::endl;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << matab[i][j] << " \t";
		}
		std::cout << std::endl;
	}
}






void matrixvector(int matvec[3][3], int mat[3][3], int vec[3][1])
{			
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			matvec[i][j] = matvec[i][j] + mat[i][j] * vec[i][0];
		}
	}
}

void otvet_matrixvector(int matvec[3][3])
{
	std::cout << "matrix na vector" << std::endl;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << matvec[i][j] << " \t";
		}
		std::cout << std::endl;
	}
}





void skalyarnoe_proizvedenie(int vectavectb, int vecta[3][1], int vectb[3][1])
{	
	vectavectb = 0;
	for (int i = 0; i < 3; i++)
	{
		vectavectb = vectavectb + vecta[i][0] * vectb[i][0];
	}
}

void otvet_skalyarnoe_proizvedenie(int vectavectb)
{
	std::cout << "skalyarnoe proizvedenie" << std::endl;
	std::cout << vectavectb << std::endl;
}






void vectornoe_proizvedenie(int vctrab[3][1], int vctra[3][1], int vctrb[3][1])
{
	for (int j = 0; j < 3; j++)
	{
		vctrab[j][0] = vctra[(j + 1) % 3][0] * vctrb[(j + 2) % 3][0] - vctra[(j + 2) % 3][0] * vctrb[(j + 1) % 3][0];
	}
}

void otvet_vectornoe_proizvedenie(int vctrab[3][1])
{

	std::cout << "vectornoe proizvedenie" << std::endl;
	for (int j = 0; j < 3; j++)
	{
		std::cout << vctrab[0][j] << std::endl;
	}
}






int main()
{
	int matr1[3][3] = { { 1, 2, 3 },
	{ 4, 5, 6 },
	{ 7, 8, 9 } };
	int matr2[3][3] = { { 1, 2, 3 },
	{ 4, 5, 6 },
	{ 7, 8, 9 } };
	int matr[3][3] = { 0 };
	slozenie_matrix(matr, matr1, matr2);
	otvet_slozenie_matrix(matr);


	int vect1[3][1] = { 1, 2, 3 };
	int vect2[3][1] = { 1, 2, 3 };
	int vect[3][1] = { 0 };
	slozenie_vectorov(vect, vect1, vect2);
	otvet_slozenie_vectorov(vect);
	std::cout << std::endl;

	int mata[3][3] = {{ 1, 2, 3 },
	{ 4, 5, 6 },
	{ 7, 8, 9 } };
	int matb[3][3] = { { 1, 2, 3 },
	{ 4, 5, 6 },
	{ 7, 8, 9 } };
	int matab[3][3] = { 0 };
	umnozenie_matrix(matab, mata, matb);
	otvet_umnozenie_matrix(matab);


	int mat[3][3] = { { 1, 2, 3 },
	{ 4, 5, 6 },
	{ 7, 8, 9 } };
	int vec[3][1] = { 1, 2, 3 };
	int matvec[3][3] = { 0 };
	matrixvector(matvec, mat, vec);
	otvet_matrixvector(matvec);


	int vecta[3][1] = { { 1 },{ 2 },{ 3 } };
	int vectb[3][1] = { { 1 },{ 2 },{ 3 } };
	int vectavectb =  0;
	skalyarnoe_proizvedenie(vectavectb, vecta, vectb);
	otvet_skalyarnoe_proizvedenie(vectavectb);


	int vctra[3][1] = { { 2 },{ -2 },{ -3 } };
	int vctrb[3][1] = { { 4 },{ 0 },{ 6 } };
	int vctrab[3][1] = { 0 };
	vectornoe_proizvedenie(vctrab, vctra, vctrb);
	otvet_vectornoe_proizvedenie(vctrab);

	getchar();
	return 0;
}

