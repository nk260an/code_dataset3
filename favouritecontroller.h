#ifndef FAVOURITECONTROLLER_H
#define FAVOURITECONTROLLER_H

#include <QObject>

#include <QDateTime>
#include <QUrl>
#include <QAbstractListModel>

class FavouriteObject
{
public:
    FavouriteObject(QDateTime p_datetime,
                 QUrl p_url);

    QDateTime datetime() const;

    QUrl url() const;

private:
    QDateTime m_datetime;
    QUrl m_url;
};

class FavouriteModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataRoles {
        DatetimeRole = Qt::UserRole + 1,
        UrlRole
    };

    FavouriteModel(QObject *parent = 0);

    void addFavourite(const FavouriteObject & newFavourite);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;
    void clear();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<FavouriteObject> m_favourite_items;

};


class FavouriteController : public QObject
{
    Q_OBJECT
public:
    explicit FavouriteController(QObject *parent = nullptr);
    FavouriteModel favouriteModel;
signals:

public slots:
};

#endif // FAVOURITECONTROLLER_H
