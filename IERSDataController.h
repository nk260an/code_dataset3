#pragma once

#include <list>
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>

#include "HttpAsioClient.h"
#include "HttpLibCURLClient.h"
#include "HttpWinsockClient.h"

// ��� ���������� .mil ������� https://astronomy.ru/forum/index.php/topic,127281.20.html?PHPSESSID=i7rg06l8k8kt36tpnrs7md7622

// ������ �� ����-����� ������ ������ ������
// https://drive.google.com/file/d/1nqWnJ7gwp3l5y3SBAWxmv_qrHAFasHoi/view?usp=sharing

using namespace boost;

class EOP_VECTOR // ����� ��� �������� ������ ����� ������ ���������� �������� ����� �� ���� ����
{
public:
	EOP_VECTOR()
	{

	}

	EOP_VECTOR(std::string str_in) // ������� ������ ���������� �������� ����� �� ����� ������ �����
	{
		std::istringstream input_stream(str_in);
		year = stoi(str_in.substr(0, 2));
		month = stoi(str_in.substr(2, 2));
		day = stoi(str_in.substr(4, 2));
		frac_mjd = stod(str_in.substr(7, 8));

		if (str_in.substr(16, 1) == "I")
		{
		A_Model.polar_motion_reliability = reliability_type::IERS;
		}
		else if (str_in.substr(16, 1) == "P")
		{
		A_Model.polar_motion_reliability = reliability_type::Predicted;
		}
		else
		{
		A_Model.polar_motion_reliability = reliability_type::Unknown;
		}

		A_Model.px = stod(str_in.substr(18, 8));
		A_Model.err_px = stod(str_in.substr(27, 8));
		A_Model.py = stod(str_in.substr(37, 8));
		A_Model.err_py = stod(str_in.substr(46, 8));

		if (str_in.substr(57, 1) == "I")
		{
		A_Model.time_reliability = reliability_type::IERS;
		}
		else if (str_in.substr(16, 1) == "P")
		{
		A_Model.time_reliability = reliability_type::Predicted;
		}
		else
		{
		A_Model.time_reliability = reliability_type::Unknown;
		}

		A_Model.dut1 = stod(str_in.substr(58, 10));
		A_Model.err_dut1 = stod(str_in.substr(69, 10));

		A_Model.lod = stod(str_in.substr(79, 7));
		A_Model.err_lod = stod(str_in.substr(86, 7));

		if (str_in.substr(95, 1) == "I")
		{
		A_Model.nutation_reliability = reliability_type::IERS;
		}
		else if (str_in.substr(95, 1) == "P")
		{
		A_Model.nutation_reliability = reliability_type::Predicted;
		}
		else
		{
		A_Model.nutation_reliability = reliability_type::Unknown;
		}

		A_Model.dpsi = stod(str_in.substr(97, 7));
		A_Model.err_dpsi = stod(str_in.substr(106, 5));

		A_Model.deps = stod(str_in.substr(116, 7));
		A_Model.err_deps = stod(str_in.substr(125, 5));

		B_Model.px = stod(str_in.substr(134, 10));
		B_Model.py = stod(str_in.substr(144, 10));
		B_Model.dut1 = stod(str_in.substr(154, 11));
		B_Model.dpsi = stod(str_in.substr(165, 10));
		B_Model.deps = stod(str_in.substr(175, 10));
	}

	enum reliability_type
	{
		IERS, // ������������� ����������� 
		Predicted, // �����������������
		Unknown
	};

	int year;
	int month;
	int day;
	double frac_mjd;

	class Class_A_Model // ��������� ��� ������ �
	{
	public:

		Class_A_Model()
		{

		}

		reliability_type polar_motion_reliability;
		double px;
		double err_px;
		double py;
		double err_py;
		reliability_type time_reliability;
		double dut1;
		double err_dut1;
		double lod;
		double err_lod;
		reliability_type nutation_reliability;
		double dpsi;
		double err_dpsi;
		double deps;
		double err_deps;

	} A_Model;

	class Class_B_Model // ��������� ��� ������ B
	{
	public:
		Class_B_Model()
		{

		}

		double px;
		double py;
		double dut1;
		double dpsi;
		double deps;
	} B_Model;
};

class EOP_TABLE
{
public:
	
	EOP_TABLE()
	{

	}

	EOP_TABLE(std::string raw_text)
	{
		// ������� ������� http://toshi.nofs.navy.mil/ser7/readme.finals
		// 1. ������� � 
		int str_count = 0;
		int http_body_begin = raw_text.find("\r\n\r\n"); // ����� ��������� � ������ ���� HTTP
		if (!(http_body_begin > 0))
		{
			return;// � http ��� �����������
		}
		std::string http_body(raw_text.substr(http_body_begin + 4));
		std::istringstream str_istream(http_body);
		std::list<std::string> content_lines;
		while (!str_istream.eof())
		{
			std::string tmp;
			std::getline(str_istream, tmp);
			if (tmp.length() != 0)
			{
				content_lines.push_back(tmp);
			}
			str_count++;
		}

		// �������� ������

		m_eop_table.resize(str_count);
		//str_istream.
		int i = 0;
		for (std::list<std::string>::iterator it = content_lines.begin(), content_end = content_lines.end();
			it != content_end;
			++it, ++i)
		{
			m_eop_table.at(i) = EOP_VECTOR(*it);
		}
		// ������ �������� �������� �� http://toshi.nofs.navy.mil/ser7/readme.finals
	}

	std::vector<EOP_VECTOR> m_eop_table;

	~EOP_TABLE()
	{

	}

};

/*class IERS_file
{
public:
	IERS_file()
	{
		return;
	}

	IERS_file(const string p_uri,
		const string p_filename)
		:m_uri(p_uri), m_filename(p_filename)
	{
		return;
	}

	string uri() { return m_uri; }
	string filename() { return m_filename; }

private:
	string m_uri = "";
	string m_filename = "";
};*/

class IERSDataController
{
public:
	IERSDataController();

	~IERSDataController();

	int update_files();

	int download_files(const std::string& host = "hpiers.obspm.fr"/*"toshi.nofs.navy.mil"*/,
		const std::string& uri = "/iers/bul/bulb_new/bulletinb.356" /*"/ser7/finals.daily"*/,
		const std::string& savefilepath = "finals.daily");

	int read_files();

	int parse();

	int getEOP();

	// ����� � url ��������� ������
	std::vector<std::map<std::string, std::string>> file_URI =
	{	{	{"prot", "http"},
			{"host","toshi.nofs.navy.mil"},
			{"uri","/ser7/"} },
		{	{"prot", "ftp"},
			{"host","toshi.nofs.navy.mil"},
			{"uri","/ser7/"}},
		{	{"prot", "http"},
			{"host","maia.usno.navy.mil"},
			{"uri","/ser7/"}},
		{	{"prot", "ftp"},
			{"host","maia.usno.navy.mil"},
			{"uri","/ser7/"}}
	};

	std::vector<std::string> file_names = { "finals2000A.all",
							"finals2000A.data",
							"finals2000A.daily" };

private:
	bool m_filesExists = false;
	bool m_filesUpToDate = false;
	bool m_filesUpdated = false;
	bool m_EOP_ready = false;
	bool m_isError = true;
	std::string m_LastError = "";
};

