#include <opencv2/opencv.hpp> 
#include <opencv2/highgui.hpp> 
#include <opencv2/opencv_modules.hpp> 
#include <opencv2/core.hpp> 
#include <opencv2/stitching.hpp> 
#include <opencv2/calib3d.hpp> 
#include <opencv2/features2d.hpp> 
#include <opencv2/flann.hpp> 
#include <opencv2/imgproc.hpp> 
#include <opencv2/ml.hpp> 
#include <opencv2/objdetect.hpp> 
#include <opencv2/photo.hpp> 
#include <opencv2/superres.hpp> 
#include <opencv2/video.hpp> 
#include <opencv2/videostab.hpp>
#include <iostream>
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/core/core_c.h"
#include "opencv2/imgproc/imgproc_c.h"
#include <stdlib.h>
#include <stdio.h>

#define _CRT_SECURE_NO_WARNINGS

using namespace std;
using namespace cv;

Mat src;
Mat dst, dst_o, dst_c;
int morph_elem;
int morph_size = 2;
int morph_operator;

void Morphology_Operations(int, void*)
{
	// Since MORPH_X : 2,3,4,5 and 6
	morph_size = 2;
	Mat element_o = getStructuringElement(2, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));
	morphologyEx(src, dst_o, 0, element_o);
	imshow("Topological opening", dst_o);

	morph_size = 20;
	Mat element_c = getStructuringElement(2, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));

	/// Apply the specified morphology operation
	morphologyEx(dst_o, dst_c, 1, element_c);
}

int main()
{
	src = imread("../../img/2.jpg", IMREAD_COLOR); // Load an image
	if (src.empty())
	{
		return -1;
	}
	// Show source image
	
	
	// Create a kernel that we will use for accuting/sharpening our image
	Mat kernel = (Mat_<float>(3, 3) <<
		10, 1, 1,
		1, -8, 1,
		1, 1, 1); // an approximation of second derivative, a quite strong kernel
				  // do the laplacian filtering as it is
				  // well, we need to convert everything in something more deeper then CV_8U
				  // because the kernel has some negative values,
				  // and we can expect in general to have a Laplacian image with negative values
				  // BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
				  // so the possible negative number will be truncated
	Mat imgLaplacian;
	Mat sharp = src; // copy source image to another temporary one
	filter2D(sharp, imgLaplacian, CV_32F, kernel);
	src.convertTo(sharp, CV_32F);
	Mat imgResult = sharp - imgLaplacian;
	// convert back to 8bits gray scale
	imgResult.convertTo(imgResult, CV_8UC3);
	imgLaplacian.convertTo(imgLaplacian, CV_8UC3);
	// imshow( "Laplace Filtered Image", imgLaplacian );
	src = imgResult; // copy back
					 // Create binary image from source image
	Mat bw;
	cvtColor(src, bw, CV_BGR2GRAY);
	threshold(bw, bw, 40, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
	// Perform the distance transform algorithm
	Mat dist;
	distanceTransform(bw, dist, CV_DIST_L2, 3);
	// Normalize the distance image for range = {0.0, 1.0}
	// so we can visualize and threshold it
	normalize(dist, dist, 0, 1., NORM_MINMAX);
	// Threshold to obtain the peaks
	// This will be the markers for the foreground objects
	threshold(dist, dist, .4, 1., CV_THRESH_BINARY);
	// Dilate a bit the dist image
	Mat kernel1 = Mat::ones(3, 3, CV_8UC1);
	dilate(dist, dist, kernel1);
	imshow("Peaks", dist);
	// Create the CV_8U version of the distance image
	// It is needed for findContours()
	Mat dist_8u;
	dist.convertTo(dist_8u, CV_8U);
	// Find total markers
	vector<vector<Point> > contours;
	findContours(dist_8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	src = dist;
	Morphology_Operations(0, 0);

	imshow("Topological closure", dst_c);

	waitKey(0);
	return 0;
}