#include "OpenedController.h"

OpenedController::OpenedController(QObject *parent) : QObject(parent)
{
    openedModel.addOpened(OpenedObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url.html")));
    openedModel.addOpened(OpenedObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url1.html")));
    openedModel.addOpened(OpenedObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url2.html")));
    openedModel.addOpened(OpenedObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url3.html")));
}

OpenedObject::OpenedObject(QDateTime p_datetime, QUrl p_url)
    : m_url(p_url),
      m_datetime(p_datetime)

{
    return;
}

QDateTime OpenedObject::datetime() const
{
    return m_datetime;
}

QUrl OpenedObject::url() const
{
    return m_url;
}

OpenedModel::OpenedModel(QObject *parent)
    : QAbstractListModel(parent)
{
    // TODO прописать загрузку из папок и XML
}

void OpenedModel::addOpened(const OpenedObject &newOpened)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_opened_items << newOpened;
    endInsertRows();
    // TODO создать папку и XML
}

int OpenedModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_opened_items.count();
}

QVariant OpenedModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= m_opened_items.count())
        return QVariant();

    const OpenedObject &openedToReturn = m_opened_items[index.row()];
    if (role == DatetimeRole)
        return openedToReturn.datetime();
    else if (role == UrlRole)
        return openedToReturn.url();

    return QVariant();
}

QHash<int, QByteArray> OpenedModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DatetimeRole] = "datetime";
    roles[UrlRole] = "url";

    return roles;
}

QVariantMap OpenedModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

void OpenedModel::clear() // код очистки ListView для Богомолова
{
    if(rowCount() > 0)
    {
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);
    m_opened_items.clear();
    endRemoveRows();
    }
}
