#include <math.h>
#include "landing.h"
//#include "common.h"
#include "aux_functions.h"




// double const VELOCITY_ALD_INCREMENT = 0.08; // при возрастании посадочной скорости на 5 узлов ALD +8%
// double const WEIGHT_SCALE_ROW = {40e03, 44e03, 48e03, 52e03, 56e03, 60e03, 64e03, 68e03};

/* таблица ALD из FCOP 2.03.10 P.3*/
//double const ALD_CONF_FULL_TABLE =
//{
//    {40e03,	44e03,  48e03, 52e03,   56e03,  60e03,  64e03,  68e03},
//    {660,	660,    670,     700,   740,    770,    810,    860},
//    {850,	860,    890,     950,   1010,	1070,	1140,	1210},
//    {1170,	1190,	1240,	1330,	1440,	1540,	1640,	1750},
//    {1130,	1150,	1200,	1280,	1380,	1470,	1570,	1670},
//    {1140,	1160,	1210,	1300,	1380,	1460,	1550,	1650},
//    {1110,	1130,	1170,	1250,	1340,	1410,	1500,	1590},
//    {1130,	1140,	1180,	1250,	1320,	1390,	1460,	1530},
//    {2310,	2340,	2410,	2530,	2650,	2770,	2900,	3030}
//};

/* таблица коррекций ALD из FCOP 2.03.10 P.3*/
//double const ALD_CONF_FULL_CORRECTION_TABLE =
//{
//    {0.03,  0.03,  0.04,  0.03,  0.05,  0.04,  0.03,  0.04},
//    {0.19,  0.22,  0.25,  0.23,  0.23,  0.21,  0.19,  0.37},
//   {-0.01, -0.06, -0.09, -0.08, -0.09, -0.08, -0.08, -0.23},
//};

/* таблица ALD из FCOP 2.03.10 P.3*/
//double const ALD_CONF_3_TABLE =
//{
//    {40e03,	44e03,  48e03, 52e03,   56e03,  60e03,  64e03,  68e03},
//    {660,	690,	720,	760,	800,	850,	920,	1010},
//    {880,	940,	1010,	1090,	1160,	1230,	1310,	1400},
//    {1220,	1310,	1430,	1560,	1680,	1810,	1940,	2080},
//    {1170,	1260,	1370,	1480,	1590,	1700,	1830,	1950},
//    {1190,	1280,	1380,	1480,	1580,	1700,	1830,	1950},
//    {1150,	1230,	1330,	1420,	1520,	1630,	1740,	1860},
//    {1160,	1240,	1320,	1400,	1480,	1550,	1630,	1720},
//    {2610,	2720,	2860,	3000,	3150,	3290,	3450,	3610}
//};

/* таблица коррекций ALD из FCOP 2.03.10 P.3*/
//double const ALD_CONF_3_CORRECTION_TABLE =
//{
//    {0.03,  0.04,  0.04,  0.04,  0.05,  0.05,  0.04,  0.04},
//    {0.16,  0.22,  0.26,  0.23,  0.24,  0.22,  0.19,  0.36},
//   {-0.01, -0.07, -0.11, -0.09, -0.11, -0.10, -0.09, -0.26},
//};

double getLDA_grip_correction(double const dblGripCoefficient) //параметр - нормативный коэффицент сцепления на ВПП, [безразмерный]
/*RRJ-95B РЛЭ, Рисунок 1.03.25 Коэффициент коррекции длины посадочной дистанции (pdf с. 183)*/
{
    double getLDA_grip_correction_table[2][5] =
    {
        {0.3,   0.35,   0.4,    0.45,   0.6},
        {1.87,  1.53,   1.26,  1.18,   1.0}
    };

    if(dblGripCoefficient<0.3 || dblGripCoefficient>0.6) // проверка на допустмый интервал
    {
        return 0.0;
    }
    else // если аргумент dblGripCoefficient в допустимом интервале
    {
        for(int i=0; i<4; i++) // поиск, между какими столбцами i и i+1 таблицы попадает аргумент dblGripCoefficient
        {

            bool bool_condition = (dblGripCoefficient >= getLDA_grip_correction_table[0][i]);
            bool_condition = bool_condition && (dblGripCoefficient <= getLDA_grip_correction_table[0][i+1]);// если true, то аргумент dblGripCoefficient попадает в интервал от i до i+1

            if(bool_condition)
            {
                /* формула для линейной интерполяции, записанная в несколько строк */
                double rslt = (getLDA_grip_correction_table[1][i+1]-getLDA_grip_correction_table[1][i]);
                rslt = rslt / (getLDA_grip_correction_table[0][i+1]-getLDA_grip_correction_table[0][i]);
                rslt = getLDA_grip_correction_table[1][i] + rslt*(dblGripCoefficient - getLDA_grip_correction_table[0][i]);
                return rslt;
            }
        }
    }
}



double getALD(double const dbl_weight, // посадочный вес, [кг]
              FlapsConf const enmFlapsConf, // конфигурация механизации крыла
              BrakingMode const enmBrakingMode, // режим торможения
              double const dblAltitude, // высота, [фт]
              double const dblWind, // попутный ветер, [уз.]
              double const dblTemperature) // температура, [град.C]
/*  Расчёт фактической посадочной дистанции,
    документ: RRJ-95B РЛЭ, 1.03.40, стр.14, pdf. С. 184  */
{
    double dblALDtable[7][9] =
    {
        {	26000.0, 	28000.0, 	30000.0, 	32000.0, 	34000.0, 	36000.0, 	38000.0, 	40000.0, 	42000.0	}, // посадочный вес, [кг]
        {	1246.0, 	1316.0, 	1387.0, 	1457.0, 	1527.0, 	1598.0, 	1668.0, 	1739.0, 	1809.0	}, // посадочная дистацния для FLAPS3, режим торможения LOW
        {	1039.0, 	1093.0, 	1147.0, 	1201.0, 	1255.0, 	1309.0, 	1364.0, 	1418.0, 	1472.0	}, // посадочная дистацния для FLAPS3, режим торможения MED
        {	927.0,      973.0,      1018.0, 	1083.0, 	1109.0, 	1154.0, 	1200.0, 	1245.0, 	1291.0	}, // посадочная дистацния для FLAPS3, режим торможения MAX
        {	1195.0, 	1261.0, 	1327.0, 	1394.0, 	1460.0, 	1526.0, 	1593.0, 	1659.0, 	1726.0	}, // посадочная дистацния для FLAPS FULL, режим торможения LOW
        {	999.0,      1050.0, 	1101.0, 	1152.0, 	1203.0, 	1254.0, 	1306.0, 	1357.0, 	1408.0	}, // посадочная дистацния для FLAPS FULL, режим торможения MED
        {	894.0,      937.0,      980.0,      1023.0, 	1065.0, 	1108.0, 	1151.0, 	1194.0, 	1237.0	} //  посадочная дистацния для FLAPS FULL, режим торможения MAX
    };

    double dblALD_altitude_correction_table[6] = {0.0249,	0.0229,	0.0215,	0.0270,	0.0255,	0.0243}; // поправка, %,  на каждые 1000ft высоты
    double dblALD_wind_correction_table[6] = {0.0197,	0.0182,	0.0171,	0.0200,	0.0184,	0.0173}; // поправка, %,  на каждый kt
    double dblALD_temp_correction_table[6] = {0.0028,	0.0027,	0.0025,	0.0028,	0.0026,	0.0025}; // поправка, %,  на каждый град.C
    int table_lngth = 9;
    int table_max_index = table_lngth -1;

    /* проверка веса на попадание в интервал таблицы */
    if((dbl_weight < dblALDtable[0][0]) || (dblALDtable[0][table_max_index] < dbl_weight))
    {
        return 0.0;
    }
    else
    {
        /* выбор строки в dblALDtable согласно режимам*/
        int j = 1;

        switch(enmFlapsConf)
        {
        case FLAPS_FULL:
        {
            j += 3;
            break;
        }
        default:
        {
            break;
        }
        }
        switch(enmBrakingMode)
        {
        case BM_LOW:
            break;
        case BM_MED:
            j++;
            break;
        case BM_MAX:
            j += 2;
            break;
        default:
            break;
        }

        /* поиск индексов, между которыми интерполируется значение*/
        for(int i = 0; i<table_max_index; i++)
        {
            if((dblALDtable[0][i] <= dbl_weight) && (dbl_weight <= dblALDtable[0][i+1]))
            {
                /*интерполяция по посадочной массе*/
                double ALD = getLinearInterpolationScalar1D(dbl_weight,
                                                            dblALDtable[0][i],
                        dblALDtable[0][i+1],
                        dblALDtable[j][i],
                        dblALDtable[j][i+1]);

                /*коррекции от высоты, ветра и температуры*/
                return ALD + ALD*(dblALD_altitude_correction_table[j] * dblAltitude/1000.0 +\
                                  dblALD_wind_correction_table[j] * dblWind +\
                                  dblALD_temp_correction_table[j] * dblTemperature);
            }
        }
    }
}

double getVref(double const dbl_weight, // посадочный вес, [кг]
               FBWCS_mode const enmFBWCSmode, // режим ЭДСУ
               FlapsConf const enmFlapsConf) // конфигурация механизации крыла
/*  посадочная скорость,
    документ: RRJ-95B РЛЭ, раздел 1.03.40, с.15-16, Таблицы 1.03.1-2,  pdf. с. 185-186 */
{
    double dblVrefTableDirectMode[6][11] =
    {
        {26000.0,	28000.0,	30000.0,	32000.0,	34000.0,	36000.0,	38000.0,	40000.0,	42000.0,	44000.0,	46000.0},
        {158.0,     164.0,      170.0,      176.0,      181.0,      187.0,      192.0,      198.0,      203.0,      208.0,      212.0},
        {138.0,     143.0,      148.0,      152.0,      156.0,      161.0,      165.0,      169.0,      173.0,      176.0,      180.0},
        {120.0,     125.0,      129.0,      133.0,      137.0,      141.0,      145.0,      149.0,      152.0,      156.0,      159.0},
        {118.0,     123.0,      127.0,      131.0,      135.0,      139.0,      143.0,      147.0,      150.0,      154.0,      157.0},
        {115.0,     119.0,      123.0,      128.0,      131.0,      135.0,      139.0,      143.0,      146.0,      150.0,      153.0}
    };

    double dblVrefTableNormalMode[7][11] =
    {
        {26000.0,	28000.0,	30000.0,	32000.0,	34000.0,	36000.0,	38000.0,	40000.0,	42000.0,	44000.0,	46000.0},
        {158.0,     164.0,      170.0,      176.0,      181.0,      187.0,      192.0,      198.0,      203.0,      208.0,      212.0},
        {138.0,     143.0,      148.0,      152.0,      156.0,      161.0,      165.0,      169.0,      173.0,      176.0,      180.0},
        {122.0,     127.0,      131.0,      136.0,      140.0,      144.0,      148.0,      152.0,      156.0,      160.0,      164.0},
        {120.0,     125.0,      129.0,      133.0,      137.0,      141.0,      145.0,      149.0,      152.0,      156.0,      159.0},
        {118.0,     123.0,      127.0,      131.0,      135.0,      139.0,      143.0,      147.0,      150.0,      154.0,      157.0},
        {115.0,     119.0,      123.0,      128.0,      131.0,      135.0,      139.0,      143.0,      146.0,      150.0,      153.0}
    };

    int table_lngth = 11;
    int table_max_index = table_lngth-1;
    double (* dblVrefTable)[11];
    int j=0;

    /* проверка веса на попадание в интервал таблицы */
    if((dbl_weight < dblVrefTableDirectMode[0][0]) || (dblVrefTableDirectMode[0][table_max_index] < dbl_weight))
    {
        // ПРЕДУСМОТРЕТЬ ОБРАБОТКУ ОШИБКИ
        return 0.0;
    }
    else
    {
        switch(enmFBWCSmode)
        {
        case FBWCS_DIRECT_MODE:
        {
            dblVrefTable = dblVrefTableDirectMode;

            switch(enmFlapsConf)
            {
            case FLAPS_0:
            {
                j = 1;
                break;
            }
            case FLAPS_1:
            {
                j = 2;
                break;
            }
            case FLAPS_1_PLUS_F:
            {
                // обработка ошибки - неверная конфигурация
                break;
            }
            case FLAPS_2:
            {
                j = 3;
                break;
            }
            case FLAPS_3:
            {
                j = 4;
                break;
            }
            case FLAPS_FULL:
            {
                j = 5;
                break;
            }
            default:
                // обработка ошибки - неверная конфигурация
                break;
            }

            break;
        }

        case FBWCS_NORMAL_MODE:
        {
            dblVrefTable = dblVrefTableNormalMode;

            switch(enmFlapsConf)
            {
            case FLAPS_0:
            {
                j = 1;
                break;
            }
            case FLAPS_1:
            {
                j = 2;
                break;
            }
            case FLAPS_1_PLUS_F:
            {
                j = 3;
                break;
            }
            case FLAPS_2:
            {
                j = 4;
                break;
            }
            case FLAPS_3:
            {
                j = 5;
                break;
            }
            case FLAPS_FULL:
            {
                j = 6;
                break;
            }
            default:
                // ОБРАБОТКА ОШИБКИ - НЕДОПУСТИМЫЙ enmFlapsConf
                break;
            }

            break;
        }

        default:
            // ОБРАБОТКА ОШИБКИ - НЕДОПУСТИМЫЙ enmFBWCSmode
            return 0.0;
        }

        /*поиск индексов, между которыми производить интерполирование*/
        for(int i = 0; i<table_max_index; i++)
        {
            if((dblVrefTable[0][i] <= dbl_weight) && (dbl_weight <= dblVrefTable[0][i+1]))
            {
                /*интерполяция по посадочной массе*/
                double Vref = getLinearInterpolationScalar1D(dbl_weight,
                                                             dblVrefTable[0][i],
                        dblVrefTable[0][i+1],
                        dblVrefTable[j][i],
                        dblVrefTable[j][i+1]);
                return Vref;

            }
        }


    }

}

double getVmin(double const dbl_weight, // посадочный вес, [кг]
               FlapsConf const enmFlapsConf) // режим ЭДСУ);
/* минимально допустимая скорость полета при полном отклонении бру на кабрирование,
    документ: RRJ-95B РЛЭ, раздел 1.03.40 стр.19, Таблица 1.03.3, pdf с. 189*/
{
    double dblVminTable[7][11] =
    {
        {26000.0,   28000.0,	30000.0,	32000.0,	34000.0,	36000.0,	38000.0,	40000.0,	42000.0,	44000.0,	46000.0}, // посадочный вес, [кг]
        {141.0,     146.0,      151.0,      156.0,      161.0,      166.0,      171.0,      175.0,      180.0,      184.0,      189.0}, // минимальная скорость для FLAPS0
        {122.0,     127.0,  	131.0,      136.0,      140.0,      144.0,      148.0,      152.0,  	156.0,      159.0,  	163.0}, // минимальная скорость для FLAPS1
        {108.0,     112.0,      116.0,      120.0,      124.0,      127.0,      131.0,      134.0,      138.0,      141.0,      144.0}, // минимальная скорость для FLAPS1+F
        {100.0,     104.0,      107.0,      111.0,      114.0,      118.0,      121.0,      124.0,      127.0,      130.0,      133.0}, // минимальная скорость для FLAPS2
        {98.0,      102.0,      106.0,      109.0,      113.0,      116.0,      119.0,      122.0,      125.0,      128.0,      131.0}, // минимальная скорость для FLAPS3
        {96.0,      99.0,       103.0,      106.0,      109.0,      113.0,      116.0,      119.0,      122.0,      125.0,      127.0} // минимальная скорость для FLAPS_FULL
    };

    int table_lngth = 11;
    int table_max_index = table_lngth-1;

    /* проверка веса на попадание в интервал таблицы */
    if((dbl_weight < dblVminTable[0][0]) || (dblVminTable[0][table_max_index] < dbl_weight))
    {
        // ПРЕДУСМОТРЕТЬ ОБРАБОТКУ ОШИБКИ
        return 0.0;
    }
    else
    {
        /* выбор строки в dblALDtable согласно режимам*/
        int j = 0;

        switch(enmFlapsConf)
        {
        case FLAPS_0:
            j += 1;
            break;
        case FLAPS_1:
            j += 2;
            break;
        case FLAPS_1_PLUS_F:
            j += 3;
            break;
        case FLAPS_2:
            j += 4;
            break;
        case FLAPS_3:
            j += 5;
            break;
        case FLAPS_FULL:
            j += 6;
            break;
        default:
            break;
        }

        /*поиск индексов, между которыми производить интерполирование*/
        for(int i = 0; i<table_max_index; i++)
        {
            if((dblVminTable[0][i] <= dbl_weight) && (dbl_weight <= dblVminTable[0][i+1]))
            {
                /*интерполяция по посадочной массе*/
                double Vmin = getLinearInterpolationScalar1D(dbl_weight,
                                                             dblVminTable[0][i],
                        dblVminTable[0][i+1],
                        dblVminTable[j][i],
                        dblVminTable[j][i+1]);
                return Vmin;

            }
        }

    }
}

double getVsr(double const dbl_weight, // вес самолёта, 30 000...45 000[кг]
              double const dbl_h, // высота полёта, <40 000 [фт]
              FlapsConf const enmFlapsConf) // конфигурация механизации
/*  скорость сваливания
    документ: RRJ-95B РЛЭ, раздел 1.03.40 стр.20-22, Рисунки 1.03.29-31, pdf с. 190-192 */
{
    double V_sr_kF = 0.0;
    //    if(dbl_weight<=11000)
    //    {
    switch(enmFlapsConf)
    {
    case FLAPS_0:
    {
        V_sr_kF = 37.0;
        break;
    }
    case FLAPS_1:
    {
        V_sr_kF = 10.0;
        break;
    }
    case FLAPS_1_PLUS_F:
    {
        V_sr_kF = 7.0;
        break;
    }
    case FLAPS_2:
    {
        V_sr_kF = 4.0;
        break;
    }
    case FLAPS_3:
    {
        V_sr_kF = 1.0;
        break;
    }
    case FLAPS_FULL:
    {
        break;
    }
    default:
        return 0.0;
        break;
    }
    //    }
    //    else
    //    {
    //        switch(enmFlapsConf)
    //        {
    //            case FLAPS_0:
    //            {
    //                V_sr_kF = 37.0
    //                break;
    //            }
    //            case FLAPS_1:
    //            {
    //                break;
    //            }
    //            default:
    //                break;
    //        }
    //    }

    // !!!! заменить
    double kh = 1.0;
    if(dbl_h<=11000)
    {
        kh = 1.0;
    }
    else
    {
        kh = dbl_h/11000;
        kh = kh*kh;
    }

    double V_sr_0 = 52.0;
    double k = 3.0/2000.0;



    return dbl_weight * kh * k + V_sr_kF + V_sr_0;

}



double getLandingWeight(double const dblBarAlt, // барометрическая высота аэродрома, [фт]
                        double const dblTemperature, // Температура воздуха на аэродроме, [град С]
                        EngineBleedAirMode const enmBleedAir) // режим отбора воздуха из двигателя
/* Определение максимальной посадочной массы, ограниченной градиентом набора высоты 2,1% при уходе на второй круг
    документ: RRJ-95B РЛЭ, раздел 1.03.40 с.2-3, рисунок 1.03.18-19, pdf с. 172-73 */
{
    // формирование массива, соотвестсвующего dblBarAlt
    // приближение по индексу массива
    // определение
    // определение коррекции от отбора тяги двигателя
    return 0.0;
}

double getLandingWeightNminus1(double const dblBarAlt, // барометрическая высота аэродрома, [фт]
                               double const dblTemperature, // Температура воздуха на аэродроме, [град С]
                               EngineBleedAirMode const enmBleedAir) // режим отбора воздуха из двигателя
/* Масса самолёта, ограниченная полным градиентом набора высоты при уходе на второй круг с одним неработающим двигателем
    документ: RRJ-95B РЛЭ, раздел 1.03.40 с.5-7, рисунок 1.03.22-22, pdf с. 175-177 */
{
    return 0.0;
}

double getM_max(double const dblTemperature,
                double const dblBarAltRW,
                double const dblALD,
                double const dbl_RWslope,
                double const dblWind,
                double const dblSafetyCoeff)
/* Ограничение посадочной массы по располагаемой посадочной дистанции
    документ: RRJ-95B РЛЭ, раздел 1.03.40 с.9-11, рисунок 1.03.23-24, pdf с. 179-181 */
{
    double kt = 100.0/25.0;
    double k_slope = 80.0/4.0;
    double k_wind = 100.0/20.0;
    double hk = dblALD * dblSafetyCoeff + dbl_RWslope*k_slope + dblWind * k_wind;
    double h00 = 1560.0 - 100.0/22.0 * 40;
    double h0 = 100.0/22.0 * dblTemperature + dblBarAltRW * 420.0/12000.0 + h00;
    return 32000 + (hk-h0)*6000.0/200.0;
}

double getVmax(FlapsConf const enmFlapsConf)
/* pdf с. 2440 */
{
    switch(enmFlapsConf)
    {
    case FLAPS_0:
    {
        return 235.0;
        break;
    }
    case FLAPS_1:
    {
        return 195.0;
        break;
    }
    case FLAPS_1_PLUS_F:
    {
        return 185.0;
        break;
    }
    case FLAPS_2:
    {
        return 185.0;
        break;
    }
    case FLAPS_3:
    {
        return 175.0;
        break;
    }
    case FLAPS_FULL:
    {
        return 165.0;
        break;
    }
    default:
        return 0.0;
        break;
    }
}
