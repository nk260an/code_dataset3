// laba13.cpp: ���������� ����� ����� ��� ����������� ����������.
// ��������� 25.12.2017

#include "stdafx.h"
#include <array>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <string>
#include <cmath>
#include <exception>

// STD - ����������� ���������� ����� �++
// ������� print_f, getchar, fopen, read - �� ������ ����������
//�������� ��������� ������ 
// [C++] -> [STD] -> [OC]

// STL - standard template librery (template - ������)
// ��� ���������� STD, ���������� ��������� ������
// � ������� �� ������ � ���
// ������ �� STL ������������ ������ cout, cin, fstream, ifstream, ofstream,
// ���������� array, vector, list, map
// ���������
// ��������� 

// std::array

// std::vector

// std::list

// std::map

int main()
{

	// std::array
	//������������� ������� ���������� �����, ����������. �������� ��������
	std::array<double, 10> double_array1; // ������, ���������� �� ������� std::array<>, ��������������� ������
	double_array1[5] = 10.5;
	double_array1.fill(0.0);
	int array_length = double_array1.size();
	// double_array.swap() - ������� ��� ������ ����� ���������� ���� ��������
	std::sort(double_array1.begin(), double_array1.end());
	std::array<double, 10> double_array2 = { 0 };
	std::copy(double_array1.begin(), double_array1.end(), double_array2.begin());
	
	// ��� �������� ��������� ��������
	std::array<std::array<int, 3>, 3> mat33 = {0};
	mat33[1][1] = 100;

	// �������� - ������, ����������� �� ������� �������, ������ ��� ������
	// "����������" ������� ����������-��������
	// ����� ��� ����� �������� STL 
	std::array<double, 10>::iterator array_iterator;

	// std::vector
	// ������������� ���������� ������ � ������������� �������� ��� ��������� ��� �����
	// ����� ������� � std::vector �� ����������� �������� std::string
	// �������� ����������
	// ������ ������ ��� ��� ���������� ����� ������ ����������� � ��
	// ����� ������� ������ � �������� ���� ������ (������ �����������)
	// �������� ������������
	// ����������� ��������� �������������� ������ �������� �� ������ [�����_1 + i]
	// �����
	// ������ �������, � ������� ��� ���������� 
	std::vector<long long> long_vector = {50, 10 , 20};

	std::cout << "vector<> = ";
	for (long long value : long_vector)
		std::cout << value;
		 std::cout << std::endl;

	long_vector.push_back(25);
	long_vector.push_back(0);

	std::cout << "vector<> after push() ";
	for (long long value : long_vector)
		std::cout << '\t' << value;
    	std::cout << std::endl;

		// ����� � std::vector ������������ ���� ���������� std::array:
		// ������, ����������, ���������, �����������, �����, � ��

		std::sort(long_vector.begin(), long_vector.end());

		std::cout << "vector<> after sort() ";
		for (long long value : long_vector)
			std::cout << '\t' << value;
		std::cout << std::endl;
		
		long_vector[2] = 11;

		// ������� � ������
		long_vector.insert(long_vector.begin() + 2, 100500 /* � ������� ������� ����� ��������� ��������� �������� */);
		std::cout << "vector<> after insert() ";
		for (long long value : long_vector)
			std::cout << '\t' << value;
		std::cout << std::endl;

		// std::list
		// ��� ��������: ������� � ���������� ������ ������������ �������, ��� � ������,
		// � ������ - ���������, ������ ��� ������ �� 1-�� �������� - ��� ���������
		// ������ ��� ������� � i-���� �������� (����������� ��� ������ � ��) ����������
		// ������ ��������� �� 1-�� ��������

		// � ������� �� array � vector �� �������� ���������� �������� � �����,
		// �������� std::list ������������ ���������� ���������, ���������� ���� � ������
		// �����������, ������� ��� ������
		// [null|������|���������] ---> [���������|������|���������]
		//                                                /
		//                                              /
		//                                            /
		//                                          /
		//                                        /
		//                            [���������|������|���������] <---> [���������|������|null]

		std::list<char> char_list = {'b'};
		char_list.push_front('a'); // ������� ����� ������ ���������
		char_list.push_back('c'); // ������� ����� ����������

		for (char ch : char_list)
		{
			std::cout << ch << std::endl;
		}

		// ���� ����� ������� � ���������� ������������ ������
		class sample_class
		{
			int a;
			double b;
		};

		std::list<sample_class> obj_list1;
		// � ��������� ������ - �� ���������� �� �������
		std::list<sample_class> obj_list2;

		// std::map
		// ������� �� ��� "����" -"��������"
		// �������� "Tu-154" - 5000, "B777" - 6000, "A320" - 6500
		std::map<std::string, double> plane_and_range =
		{ { "Tu - 154", 5000 }, { "B777" , 6000}, { "A320", 6500 } };
		plane_and_range.insert(std::pair<std::string, double>("A320", 10000.0));

		std::cout << "the range of Boeing 777 is " << plane_and_range["B777"] << std::endl;
		std::cout << "the range of A320 is " << plane_and_range["A320"] << std::endl;
		// plane_and_range.insert({"A320", 10000});
		std::cout << "the range of A320 is " << plane_and_range["A320"] << std::endl;


		double d_array[5] = { 1,2,3,4,5 };
		int i = 60000;
		try // � try ���������� ���, ������������ ��������� �������� ����������:
			// �������, �� ���������� ������� � ������, ������, ����������� � ��
			// �������, ����������� ���������� � �������
		{
			d_array[i] = 5;
		}
		catch (std::exception& ex)
		{
			std::cerr << ex.what() << std::endl;
		}

		catch (...)
		{
			std::cout << "eror in \"d_array[i] = 15\"" << std::endl;
		}

	getchar();
    return 0;
}

// � ���� �� ����� ����, ����������� ��� ������� ������������ ���������� ��:
// - ����-������������ (�� ������ ������ ��� ������ ��������� ������, �������
//        ���������� ������������, ������ � ������ sln ������������� ����� �������, ���
//             � ������ ������� ���������� ������� ������, ����� ���������� ��� ����� � �����������)
// - ����������, �������� � ������� ��������� (SCRUM, WATERFALL, Agile, ...)
// - �������� ���������� ���������� 
// - ������������� � ����������� ����������
// - 