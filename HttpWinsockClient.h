#pragma once
#include <stdio.h>
#include <iostream>
//#include <fstream>
#include <string>
#include <winsock2.h>

class HttpWinsockClient
{
public:
	HttpWinsockClient();
	~HttpWinsockClient();
	int connect(const char * url);
	int get_synchronous(const char * url/* = "toshi.nofs.navy.mil"*/,
		const char * uri/* = "/ser7/finals.daily"*/,
		std::iostream & reply_stream);
	std::string get_last_error_string() const;
	bool isError() const;
private:
	WSADATA wsaData;
	SOCKET Socket;
	std::string m_last_error_string;
	bool m_isError;
};

