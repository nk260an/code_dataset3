#include "helphint.h"


helphint::helphint(QWidget *parent)
    : QFrame(parent)
{
    this->setParent(parent);
    this->setStyleSheet("helphint { border: 2px solid black;"
                        "border-radius: 4px;"
                        "padding: 2px;"
                        "background-color: orange;"
                        "font : 18pt ""Arial"";}");
    qDebug() << this->styleSheet();
    this->setWindowFlags(Qt::FramelessWindowHint);
    layGrid = new QGridLayout(this);

    lblCaption = new QLabel();
    this->setStyleSheet("background-color: orange;"
                        "font : 75 18pt ""Arial"";");
    layGrid->addWidget(lblCaption,
                       0,1);

    //TODO настроить стиль: размер шрифта, жирный
    lblText = new QLabel();
    this->setStyleSheet("background-color: orange;"
                        "font : 18pt ""Arial"";");
    layGrid->addWidget(lblText,
                       1,1);
    //TODO настроить стиль: размер шрифта, выравнивание
    lblImage = new QLabel();
    layGrid->addWidget(lblImage,
                       0,0,
                       3, 1);
    // TODO настроить размеры

    lblHint = new QLabel("Для перехода к основной справке нажмите подсказку");
    layGrid->addWidget(lblHint,
                       2, 1);

    lblImage->setPixmap(pixImage);
    lblImage->resize(QSize(50, 50));

    this->setLayout(layGrid);
    wgtParent = parent;

}

void helphint::show(strucHintItem hintItem,
                    QRect targetrect)
{
    //qDebug() << "hintItem " << hintItem;

    lblCaption->setText( hintItem.caption);
    qDebug() << "hintItem.caption " << hintItem.caption;

    lblText->setText( hintItem.text );
    qDebug() << "hintItem.caption " << hintItem.caption;


    pixImage.load("help.png");

    lblImage->setPixmap(pixImage.scaled(QSize(100,100),
                                        Qt::KeepAspectRatio));
    qDebug() << "hintItem.imageFilename" << "help.png";
    qDebug() << "pixImage " << pixImage;

    this->helpPageFilename = hintItem.helpPageFilename;

    // позиция всплывающей подсказк
    QSize hitnsize;
    if(targetrect.bottom() < wgtParent->size().height()/2) // если сверху - подсказка внизу
    {
        hitnsize = QSize(wgtParent->size().width(),
                         wgtParent->size().height()/2) -
                   QSize(200,
                         200);
        this->setGeometry( QRect(QPoint(100,
                                        wgtParent->size().height()/2 + 100), hitnsize) );
    }
    else // если снизу -подсказка вверху
    {
        hitnsize = QSize(wgtParent->size().width(),
                         wgtParent->size().height()/2) -
                   QSize(200,
                         200);
        this->setGeometry( QRect(QPoint(100, 100), hitnsize) );
    }
    QFrame::show();

}

strucHintItem::strucHintItem()
    : caption(),
      text(),
      imageFilename(),
      helpPageFilename()
{

}

strucHintItem::strucHintItem(QString p_caption, QString p_text, QString p_imageFilename, QString p_helpPageFilename)
    : caption(p_caption),
      text(p_text),
      imageFilename(p_imageFilename),
      helpPageFilename(p_helpPageFilename)
{
    //        this->caption = p_caption;
    //        this->text = p_text;
    //        this->imageFilename = p_imageFilename;
    //        this->helpPageFilename = p_helpPageFilename;
}
