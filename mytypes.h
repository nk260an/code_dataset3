#ifndef MYTYPES_H
#define MYTYPES_H

#endif // MYTYPES_H

#include <QtCore>
#include <QtDebug>
#include <QDir>
class NoteAttachmentClass: public QObject
{
    Q_OBJECT
    Q_ENUMS(enmNoteAttachment)

public:
    enum class enmNoteAttachment
    {
        ATCH_TEXT = 0x01,
        ATCH_SCREEN = 0x02,
        ATCH_PHOTO = 0x04,
        ATCH_AUDIO = 0x08,
        ATCH_VIDEO = 0x0F
    };
    Q_ENUMS(enmNoteAttachment)
};

class NoteIconClass: public QObject
{
    Q_OBJECT


public:
    enum class enmNoteIconClass
    {
        ATCH_DEFAULT = 0x01,
        ATCH_CORRECTION = 0x02,
        ATCH_MISTAKE = 0x04,
        ATCH_CRITICAL_MISTAKE = 0x08
    };
    Q_ENUMS(enmNoteIconClass)
};

// создать папку с именем, производным от даты/времени и добавлять в лист
// считывать из папок в лист при старте приложения
class NotesListOrganizer : public QObject
{
    Q_OBJECT

public:
    QDir notedir;// = "Notes/";
    QDir rptdir;// = "Reports/";

    explicit NotesListOrganizer(QObject* parent = 0)
        : QObject(parent)
    {
        notedir = "Notes/";
        rptdir = "Reports/";
        if (!notedir.exists()){
            qDebug("Папка ""Notes/"" не существует");
            notedir.mkdir(".");
        }
        else
        {
            // TODO загрузить список
        }

        if (!rptdir.exists()){
            qDebug("Папка ""Reports/"" не существует");
            rptdir.mkdir(".");
        }
        else
        {
            // TODO загрузить список отчётов
        }
}

Q_INVOKABLE QString createMomentDirectory(void)
{
    QString dirname = notedir.path() +
            QDate::currentDate().toString("'/rkknote_'yyyy_MM_dd") +
            QTime::currentTime().toString("_hh_mm_ss");// строка с текущей датой
    qDebug(dirname.toLatin1());
    QDir dir(dirname);
    if (!dir.exists()){
        qDebug("Папка не существует");
        dir.mkdir(".");
    }
    if (!dir.exists()){
        qDebug("Папка не существует");
        dir.mkdir(".");
    }
    qDebug() << dir.absolutePath();
    return dir.absolutePath();
}

};
