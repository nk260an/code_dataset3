#include "mousem.h"

mousem::mousem(QObject *parent) : QObject(parent)
{

}

void mousem::test()
{

}

void mousem::clear()
{
    qDebug() << "clear the data";
}

void mousem::save()
{
    qDebug () << "save the data";
}

void mousem::add(double x, double y)
{
    QPoint p(x,y);
    qDebug () << "Adding" <<p;
}

void mousem::add(QPointF point)
{
    qDebug () << "Adding float" << point;
}


