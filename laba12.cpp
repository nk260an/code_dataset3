// laba12.cpp: ���������� ����� ����� ��� ����������� ����������.
// enscryption
// https://www.openssl.org/

#include "stdafx.h"
#include <iostream>
#include <openssl/conf.h> // �������, ��������� � ��������� ��������� openssl
#include <openssl/evp.h> // ���� ����������������� ������� https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // ���� ���������� ������ openssl � �� �����������
#include <fstream>
#include <openssl/aes.h>
#include <bitset>


using namespace std;

int main()
{
	// ��� �������, � ������� ����������, ��������� ������������ �� �������� ������ ������
	// �� ����� � ������� � �������������/�������������
	struct name_of_my_struct // ������ ������
	{
		name_of_my_struct()
		{

		}
		int a;
		double b;
		int fnc1()
		{
			return a;
		}
	};

	// ��������� ��� �����, ������ � �������

	// ������ ��� �����
	// ��� ����
	// ��� ������������� ������

	unsigned char *plaintext = (unsigned char *)"some text some text some text some text"; // �������� �����
	int plaintext_lenght = strlen((char *)plaintext); // ����� ������
	unsigned char *key = (unsigned char *)"0123456789"; // ������ (�����)
	unsigned char *iv = (unsigned char *)"0123456789012345"; // ���������������� ������
	unsigned char cryptedtext[256]; // ������������� ���������
	unsigned char decryptedtext[256]; // �������������� ���������


		// 1. ��������� erfpfntkmyf ytceotcnde.oee cnhernehe �������� � �������� ����������
	    EVP_CIPHER_CTX *ctx; // structure

		// 2. ��� ��������� ��������� ������ ��������� �������� (�����, ����, ������ ������������� � �.�.)
		ctx = EVP_CIPHER_CTX_new(); //�������� ��������� � ����������� ������

		//3. ��������� 	EVP_CIPHER_CTX  ����������� �����������
		EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // ������������� ������� aes, ������ � ��������

		// 4. ��� ������� ���������� - ������� EVP_EncryptUpdate
		int lenght;
		EVP_EncryptUpdate(ctx, cryptedtext, &lenght, plaintext, plaintext_lenght); // ����������, ����������
		int cryptedtext_lenght = lenght;

		// 5. ����������� �������� ����������
		EVP_EncryptFinal_ex(ctx, cryptedtext + lenght, &lenght);
		cryptedtext_lenght += lenght;

		// 6. �������� ���������
		EVP_CIPHER_CTX_free(ctx);
		for (int i = 0; i < cryptedtext_lenght; i++)
		{
			cout << hex << cryptedtext[i];
			if ((i + 1) % 32 == 0) cout << endl;
		}
		cout << endl;
		// BIO_dump_fp(stdout, (char*)cryptedtext, cryptedtext_lenght-1);
		/*
 ///////////////////////////////////////////////////////////////////////////
|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		*/
		

		// ����������
		// 1)
		ctx = EVP_CIPHER_CTX_new(); // �������� ���������  � �����������
		
		// 2)
		EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);

		// 3)
		EVP_EncryptUpdate(ctx, cryptedtext, &lenght, plaintext, plaintext_lenght);

		// 4)
		int dectypted_lenght = lenght;
		EVP_DecryptFinal_ex(ctx, decryptedtext + lenght, &lenght);

		// 5)
		dectypted_lenght += lenght;
		EVP_CIPHER_CTX_free(ctx);
		decryptedtext[dectypted_lenght] = '\0';
		cout << decryptedtext << endl;



		// ���������� �����
		// ������������ ����� ��� ��, �� ��������, � �����

		/*
		1. �������� ������ � ��������� ���������� openssl
		2. ���������� ������� �����
		3. while(�����������_�������� > 0)
		{
		4. ���������� ����������
		5. ������ �������������� ������� � ����
		6. ���������� ���������� ���������
		}
		7. ���������� �������������� �������
		8. ������ ��������������� ����� � ����
		9. �������� ������
		*/

		fstream f0, f_enctypted, f_decrypted;
		f0.open("f0.txt", std::fstream::in | std::fstream::binary); // ���� � ��������� �������

		//���� ��� ������������ ������
		f_enctypted.open("f_enctypted.txt",
			std::fstream::out | std::fstream::trunc | std::fstream::binary);

		/*
		unsigned int number_of_bytes = 0;
		unsigned int filesize = 0;
		*/

		char buffer[256] = { 0 };
		char out_buf[256] = { 0 };

		ctx = EVP_CIPHER_CTX_new();
		EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
		lenght = 0;
		f0.read(buffer, 256);
		while (f0.gcount() > 0) // ����, ���� �� ����� ���-�� ����������� (���� ������ ��������� ������ > 0)
		{
		// ���������� ������
			EVP_EncryptUpdate(ctx, // ������ � �����������
				(unsigned char *)out_buf, // ������� ��������: ������, ���� �������� ������������� ������
				&lenght, // �������� ��������: ����� ����������� ����� 
				(unsigned char *)buffer, // ������� ��������: ��� ���������
				f0.gcount()); // ������� ��������: ����� ������� ������
		// ����� ������������� ������ � ����
			f_enctypted.write(out_buf, lenght);
		// ���������� ��������� ������
			f0.read(buffer, 256);
		}
	    /*
    	AES_KEY aes_key;
		AES_set_encrypt_key(key, 128, &aes_key);
		while (true) // ����������� ���� ���������� ������ �����, ���������� � ������ � ������ ����
		{
			// ����������
			f0.read(buffer, 256);
			number_of_bytes += AES_BLOCK_SIZE;
		*/
		
		EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf, &lenght);
		f_enctypted.write(out_buf, lenght);
		f_enctypted.close();
		f0.close();


		getchar();
	return 0;
}
