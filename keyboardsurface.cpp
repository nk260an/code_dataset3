#include "keyboardsurface.h"

KeyboardSurface::KeyboardSurface(QWidget *parent)
{
    _parent = parent;
    frame = new QFrame(parent->parentWidget());

    QPoint p1;
    p1.setX(_parent->parentWidget()->size().width()/8);
    p1.setY(_parent->parentWidget()->size().height()/4);

    QPoint p2;
    p2.setX((_parent->parentWidget()->size().width()/8) * 6);
    p2.setY((_parent->parentWidget()->size().height()/4) * 3);

    frame->setGeometry(QRect(p1, p2));
    frame->setStyleSheet("padding: 5px;"
                         "color: white;"
                         "margin: 0px;"
                         "font-size: 20px;"
                         "background: #08C;");
                         
    grid = new QGridLayout(frame);
    lblName = new QLabel();
    lblMin = new QLabel();
    lblMax = new QLabel();
    
    input.styleSheet().clear();
    input.setStyleSheet("color: white;"
                        "background: transparent;"
                        "border-bottom: 1px dotted black;");
    
    btn = new QPushButton *[9];
    for(int i = 0;i < 9;i++){
        btn[i] = new QPushButton();
        btn[i]->setText(QString::number(i+1));
        connect(btn[i], SIGNAL(clicked()), this, SLOT(AppendText()));
        btn[i]->setGeometry(0,0,60,45);
        btn[i]->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    }
    
    QPushButton *btnRemove = new QPushButton();
    btnRemove->setText("⌫");
    btnRemove->setGeometry(0,0,60,45);
    btnRemove->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(btnRemove, SIGNAL(clicked()), this, SLOT(RemoveText()));
    
    QPushButton *enterbutton = new QPushButton();
    enterbutton->setText("⏎");
    connect(enterbutton, SIGNAL(clicked()), this, SLOT(Ex()));
    enterbutton->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    
    QPushButton *btnMinus = new QPushButton();
    btnMinus->setText("-");
    btnMinus->setGeometry(0,0,60,45);
    btnMinus->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(btnMinus, SIGNAL(clicked()), this, SLOT(AppendText()));
    
    QPushButton *btnLeft = new QPushButton();
    btnLeft->setText("←");
    btnLeft->setGeometry(0,0,60,45);
    btnLeft->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(btnLeft, SIGNAL(clicked()), this, SLOT(CursorLeft()));
    
    QPushButton *btnRight = new QPushButton();
    btnRight->setText("→");
    btnRight->setGeometry(0,0,60,45);
    btnRight->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(btnRight, SIGNAL(clicked()), this, SLOT(CursorRight()));
    
    grid->addWidget(lblName, 0, 0, 1, 2);
    grid->addWidget(lblMin, 0, 2, 1, 1);
    grid->addWidget(lblMax, 0, 3, 1, 1);
    grid->addWidget(&input, 1, 0, 1, 4);
    grid->addWidget(btnRemove, 5, 3, 1, 1);
    grid->addWidget(enterbutton, 2, 3, 3, 1);
    grid->addWidget(btnMinus, 5, 2, 1, 1);
    grid->addWidget(btnLeft, 6, 0, 1, 2);
    grid->addWidget(btnRight, 6, 2, 1, 2);
    int c = 0;
    for(int i = 0;i < 3;i++){
        for(int j = 0;j < 3;j++){
            grid->addWidget(btn[c++], i+2, j, 1, 1);
        }
    }
    
    frame->hide();
}
void KeyboardSurface::CursorLeft(){
    if(cursorPos > 0){
        cursorPos--;
        input.setFocus();
        input.setCursorPosition(cursorPos);
    }
}
void KeyboardSurface::CursorRight(){
    if(input.cursorPosition() < input.text().length()){
        cursorPos++;
        input.setFocus();
        input.setCursorPosition(cursorPos);
    }
}

void KeyboardSurface::show(QObject *sender){
    _sender = sender;
    frame->show();
    
    lblName->setText(_sender->property("toolTip").toString());
    lblMin->setText("Min: " + _sender->property("minimum").toString());
    lblMax->setText("Max: " + _sender->property("maximum").toString());
    
    btnZero = new QPushButton(frame);
    btnZero->setText("0");
    btnZero->setGeometry(0,0,60,45);
    btnZero->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(btnZero, SIGNAL(clicked()), this, SLOT(AppendText()));
    
    if(!strcmp(sender->metaObject()->className(), "QSpinBox")){
        kbt = spnbox;
        grid->addWidget(btnZero, 5, 0, 1, 2);
    }
    if(!strcmp(sender->metaObject()->className(), "QDoubleSpinBox")){
        kbt = dblspnbox;
        btnDot = new QPushButton(frame);
        btnDot->setText(".");
        btnDot->setGeometry(0,0,60,45);
        btnDot->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        connect(btnDot, SIGNAL(clicked()), this, SLOT(AppendText()));
        grid->addWidget(btnDot, 5, 0, 1, 1);
        grid->addWidget(btnZero, 5, 1, 1, 1);
    }
    if(!strcmp(sender->metaObject()->className(), "QLineEdit")){
        kbt = lineedit;
    }
    
    if(_sender->property("value").toString() == "0"){
        input.text().clear();
    }else{
        input.setText(_sender->property("value").toString());
    }
    
    cursorPos = input.text().length();
    
    frame->show();
}
void KeyboardSurface::Ex(){
    switch (kbt) {
    case spnbox:
        if(input.text().isEmpty()){
            input.setStyleSheet("background-color: red;");
            return;
        }
        _sender->setProperty("value", input.text().toInt());
        break;
    case dblspnbox:
        if(input.text().isEmpty()){
            input.setStyleSheet("background-color: red;");
            return;
        }
        _sender->setProperty("value", input.text().toDouble());
        break;
    case lineedit:
        ((QLineEdit*)_sender)->setText(input.text());
        break;
    default:
        break;
    }
    
    frame->hide();
}
void KeyboardSurface::AppendText(){
    QString temp = input.text();
    
    if(((QPushButton*)QObject::sender())->text() == "-"){
        if(temp.contains("-")){
            temp.remove(0, 1);
            cursorPos--;
        }else {
            temp.insert(0, '-');
            cursorPos++;
        }
    }else{
        if(((QPushButton*)QObject::sender())->text() == "."){
            if(temp.contains(".")){
                return;
            }
            if(temp.isEmpty()){
                return;
            }
        }
        temp.insert(cursorPos,((QPushButton*)QObject::sender())->text());
        cursorPos++;
    }
    input.setText(temp);
    Check();
    input.setFocus();
    input.setCursorPosition(cursorPos);
}
void KeyboardSurface::RemoveText(){
    QString temp = input.text();
    
    if(!temp.isEmpty() && input.cursorPosition() != 0){
        temp.remove(cursorPos-1, 1);
        cursorPos--;
    }
    input.setText(temp);
    Check();
    input.setFocus();
    input.setCursorPosition(cursorPos);
}
void KeyboardSurface::Check(){
    QString temp = input.text();
    
    if(temp.toDouble() < _sender->property("minimum").toString().toDouble()){
        input.setStyleSheet("background-color: red;");
    }else{
        if(temp.toDouble() > _sender->property("maximum").toString().toDouble()){
            input.setStyleSheet("background-color: red;");
        }else{
            input.setStyleSheet("background-color: transparent;");
        }
    }
}
