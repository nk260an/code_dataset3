#ifndef RECENTCONTROLLER_H
#define RECENTCONTROLLER_H

#include <QObject>

#include <QDateTime>
#include <QUrl>
#include <QAbstractListModel>

class RecentObject
{
public:
    RecentObject(QDateTime p_datetime,
                 QUrl p_url);

    QDateTime datetime() const;

    QUrl url() const;

private:
    QDateTime m_datetime;
    QUrl m_url;
};

class RecentModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataRoles {
        DatetimeRole = Qt::UserRole + 1,
        UrlRole
    };

    RecentModel(QObject *parent = 0);

    void addRecent(const RecentObject & newRecent);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;
    void clear();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<RecentObject> m_recent_items;

};


class RecentController : public QObject
{
    Q_OBJECT
public:
    explicit RecentController(QObject *parent = nullptr);
    RecentModel recentModel;
signals:

public slots:
};

#endif // RECENTCONTROLLER_H
