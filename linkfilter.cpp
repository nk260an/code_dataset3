#include "linkfilter.h"

Linkfilter::Linkfilter(QObject *parent) : QObject(parent)
{

}
bool Linkfilter::eventFilter(QObject *object, QEvent *event)//Задаем для WhatsThis класс для нажатия на гиперссылку
{
    if(event->type() == QEvent::WhatsThisClicked){
        QWhatsThisClickedEvent *wtcEvent = static_cast<QWhatsThisClickedEvent*>(event);//Создаем ивент нажатия
        emit linkClicked(wtcEvent->href());
        return true;
    }
    return false;
}
