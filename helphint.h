#ifndef HELPHINT_H
#define HELPHINT_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QFrame>
#include <QMouseEvent>
#include <QtDebug>
#include <QLayout>
#include <QLabel>

struct strucHintItem
{
    strucHintItem();

    strucHintItem(QString p_caption,
                  QString p_text,
                  QString p_imageFilename,
                  QString p_helpPageFilename);

    friend QDebug operator<<(QDebug debug, const strucHintItem& strc)
    {
        const bool oldSetting = debug.autoInsertSpaces();
        debug.nospace() << "strucHintItem(";
        debug << "caption " << strc.caption;
        debug << ", text " << strc.text;
        debug << ", imageFilename " << strc.imageFilename;
        debug << ", helpPageFilename " << strc.helpPageFilename;
        debug << ")";
        debug.setAutoInsertSpaces(oldSetting);
        return debug.maybeSpace();
    }

    QString caption;
    QString text;
    QString imageFilename;
    QString helpPageFilename;
};

class helphint : public QFrame
{
    Q_OBJECT
public:
    //explicit helphint(QWidget *parent = 0);
    explicit helphint(QWidget *parent = 0);

    QFrame * hlpHint = 0;
    QWidget * wgtParent;
    QLabel * lblCaption;
    QLabel * lblText;
    QLabel * lblImage;
    QLabel * lblHint;
    QPixmap pixImage;
    QGridLayout * layGrid;
    QString helpPageFilename;
    //QSize hitnsize;

    //void show();

    void show(strucHintItem hintItem,
              QRect targetrect);
    void goToHelpPage()
    {
        // TODO прописать переход к основной справке
    }

signals:

public slots:

protected:
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE
    {
        goToHelpPage();
    }

};

#endif // HELPHINT_H
