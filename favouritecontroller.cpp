#include "FavouriteController.h"
#include <QDebug>
FavouriteController::FavouriteController(QObject *parent) : QObject(parent)
{
    favouriteModel.addFavourite(FavouriteObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url.html")));
    favouriteModel.addFavourite(FavouriteObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url1.html")));
    favouriteModel.addFavourite(FavouriteObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url2.html")));
    favouriteModel.addFavourite(FavouriteObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url3.html")));
}

FavouriteObject::FavouriteObject(QDateTime p_datetime, QUrl p_url)
    : m_url(p_url),
      m_datetime(p_datetime)

{
    return;
}

QDateTime FavouriteObject::datetime() const
{
    return m_datetime;
}

QUrl FavouriteObject::url() const
{
    return m_url;
}

FavouriteModel::FavouriteModel(QObject *parent)
    : QAbstractListModel(parent)
{
    // TODO прописать загрузку из папок и XML
}

void FavouriteModel::addFavourite(const FavouriteObject &newFavourite)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_favourite_items << newFavourite;
    endInsertRows();
    // TODO создать папку и XML
}

int FavouriteModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_favourite_items.count();
}

QVariant FavouriteModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= m_favourite_items.count())
        return QVariant();

    const FavouriteObject &favouriteToReturn = m_favourite_items[index.row()];
    if (role == DatetimeRole)
        return favouriteToReturn.datetime();
    else if (role == UrlRole)
        return favouriteToReturn.url();

    return QVariant();
}

QHash<int, QByteArray> FavouriteModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DatetimeRole] = "datetime";
    roles[UrlRole] = "url";

    return roles;
}

QVariantMap FavouriteModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

void FavouriteModel::clear() // код очистки ListView для Богомолова
{
    if(rowCount() > 0)
    {
        beginRemoveRows(QModelIndex(), 0, rowCount()-1);
        m_favourite_items.clear();
        endRemoveRows();
    }
}
