
#include "stdafx.h"
#define _CRT_SECURE_NO_WARNINGS

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv_modules.hpp>
#include <opencv2/core.hpp>
#include <opencv2/stitching.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/flann.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ml.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/photo.hpp>
#include <opencv2/superres.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videostab.hpp>
#include <stdlib.h>
#include <stdio.h>

using namespace cv;
using namespace std;

IplImage* image = 0;

Mat canny_output;
vector<vector<Point> > contours;
vector<Vec4i> hierarchy;

RNG rng(12345);
// для хранения каналов RGB
IplImage* rgb = 0;
IplImage* r_plane = 0;
IplImage* g_plane = 0;
IplImage* b_plane = 0;
// для хранения каналов RGB после преобразования
IplImage* r_range = 0;
IplImage* g_range = 0;
IplImage* b_range = 0;
// для хранения суммарной картинки
IplImage* rgb_and = 0;
IplImage* erode_image = 0;
IplImage* dilate_image = 0;


int thresh = 100;
int max_thresh = 255;


int Rmin = 0;
int Rmax = 52;

int Gmin = 0;
int Gmax = 53;

int Bmin = 1;
int Bmax = 256;

int RGBmax = 256;

int radius_erode = 1;
int radius_max_erode = 10;

int radius_dilate = 1;
int radius_max_dilate = 10;

int iterations_erode = 1;
int iterations_max_erode = 20;

int iterations_dilate = 1;
int iterations_max_dilate = 20;


void myTrackbarRadius_dilate(int pos) {
	radius_dilate = pos;
}
void myTrackbarIterations_dilate(int pos) {
	iterations_dilate = pos;

}
void myTrackbarRadius_erode(int pos) {
	radius_erode = pos;
}
void myTrackbarIterations_erode(int pos) {
	iterations_erode = pos;
}




//
// функции-обработчики ползунка
//

void myTrackbarRmin(int pos) {
	Rmin = pos;
	cvInRangeS(r_plane, cvScalar(Rmin), cvScalar(Rmax), r_range);
}


void myTrackbarRmax(int pos) {
	Rmax = pos;
	cvInRangeS(r_plane, cvScalar(Rmin), cvScalar(Rmax), r_range);
}

void myTrackbarBmin(int pos) {
	Bmin = pos;
	cvInRangeS(b_plane, cvScalar(Bmin), cvScalar(Bmax), b_range);
}

void myTrackbarBmax(int pos) {
	Bmax = pos;
	cvInRangeS(b_plane, cvScalar(Bmin), cvScalar(Bmax), b_range);
}


int main(int argc, char* argv[])
{
	// имя картинки задаётся первым параметром
	char* filename = argc == 2 ? argv[1] : "../../img/2.jpg";
	// получаем картинку
	image = cvLoadImage(filename, 1);

	// создаём картинки
	rgb = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 3);
	r_plane = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	g_plane = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	b_plane = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	r_range = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	g_range = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	b_range = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	rgb_and = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);

	//  копируем
	cvCopy(image, rgb);
	// разбиваем на отельные каналы
	cvSplit(rgb, b_plane, g_plane, r_plane, 0);

	//
	// определяем минимальное и максимальное значение
	// у каналов HSV
	double framemin = 0;
	double framemax = 0;

	cvMinMaxLoc(r_plane, &framemin, &framemax);
	printf("[R] %f x %f\n", framemin, framemax);
	Rmin = framemin;
	Rmax = framemax;

	/*cvMinMaxLoc(g_plane, &framemin, &framemax);
	printf("[G] %f x %f\n", framemin, framemax);
	Gmin = framemin;
	Gmax = framemax;
	*/
	cvMinMaxLoc(b_plane, &framemin, &framemax);
	printf("[B] %f x %f\n", framemin, framemax);
	Bmin = framemin;
	Bmax = framemax;

	// окна для отображения картинки
	cvNamedWindow("original", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("R range", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("G range", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("B range", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("rgb and", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("erode", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("dilate", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("end", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("contours", CV_WINDOW_AUTOSIZE);


	cvCreateTrackbar("Rmin", "R range", &Rmin, RGBmax, myTrackbarRmin);
	cvCreateTrackbar("Rmax", "R range", &Rmax, RGBmax, myTrackbarRmax);
	//cvCreateTrackbar("Gmin", "G range", &Gmin, RGBmax, myTrackbarGmin);
	//cvCreateTrackbar("Gmax", "G range", &Gmax, RGBmax, myTrackbarGmax);
	cvCreateTrackbar("Bmin", "B range", &Gmin, RGBmax, myTrackbarBmin);
	cvCreateTrackbar("Bmax", "B range", &Gmax, RGBmax, myTrackbarBmax);
	cvCreateTrackbar("Radius", "erode", &radius_erode, radius_max_erode, myTrackbarRadius_erode);
	cvCreateTrackbar("Iterations", "erode", &iterations_erode, iterations_max_erode, myTrackbarIterations_erode);
	cvCreateTrackbar("Radius", "dilate", &radius_dilate, radius_max_dilate, myTrackbarRadius_dilate);
	cvCreateTrackbar("Iterations", "dilate", &iterations_dilate, iterations_max_dilate, myTrackbarIterations_dilate);


	while (true) {

		// показываем картинку
		cvShowImage("original", image);

		// показываем результат порогового преобразования
		cvShowImage("R range", r_range);
		//cvShowImage("G range", g_range);
		cvShowImage("B range", b_range);

		// складываем 
		cvAnd(r_range, g_range, rgb_and);
		cvAnd(rgb_and, b_range, rgb_and);

		// показываем результат
		cvShowImage("rgb and", rgb_and);


		erode_image = cvCloneImage(rgb_and);
		dilate_image = cvCloneImage(rgb_and);

		IplConvKernel* Kern_dilate = cvCreateStructuringElementEx(radius_dilate * 2 + 1, radius_dilate * 2 + 1, radius_dilate, radius_dilate, CV_SHAPE_ELLIPSE);
		IplConvKernel* Kern_erode = cvCreateStructuringElementEx(radius_erode * 2 + 1, radius_erode * 2 + 1, radius_erode, radius_erode, CV_SHAPE_ELLIPSE);

		// выполняем преобразования
		
		
		cvErode(rgb_and, erode_image, Kern_erode, iterations_erode); // дорисовывает
		cvDilate(erode_image, dilate_image, Kern_dilate, iterations_dilate); // убирает лишнее

		// показываем результат
		cvShowImage("erode", erode_image);
		cvShowImage("dilate", dilate_image);
		cvReleaseStructuringElement(&Kern_dilate);
		cvReleaseStructuringElement(&Kern_erode);

		

		char c = cvWaitKey(33);
		if (c == 27) { // если нажата ESC - находим контуры
			cv::Mat merode_image = cv::cvarrToMat(dilate_image);
			
			Canny(merode_image, canny_output, thresh, thresh * 2, 3);
			findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

			/// Draw contours
			Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
			for (int i = 0; i < contours.size(); i++)
			{
				Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
				drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
			}
			imshow("contours", drawing);
		}
	}

	// освобождаем ресурсы
	cvReleaseImage(&image);
	cvReleaseImage(&rgb);
	cvReleaseImage(&r_plane);
	cvReleaseImage(&g_plane);
	cvReleaseImage(&b_plane);
	cvReleaseImage(&r_range);
	cvReleaseImage(&g_range);
	cvReleaseImage(&b_range);
	cvReleaseImage(&rgb_and);
	// удаляем окна
	cvDestroyAllWindows();
	return 0;
}