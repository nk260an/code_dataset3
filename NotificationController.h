#ifndef NOTIFICATIONCONTROLLER_H
#define NOTIFICATIONCONTROLLER_H

#include <QObject>
#include <QAbstractListModel>
#include <QVariant>
#include <QList>
#include <QDebug>

// TODO изменение цвета индикатора (картинка png)
// TODO сортировка в QList по важности (importance) фукнция std::sort()
// TODO отдельная GUI-страница со списком уведомлений в PageMainGUI.qml в Swipe
// список аналогично как на странице заметок (см. PageNoteList.qml и его c++ класс)

// TODO убирать накопленные уведомления по Swipe (напишу, как дойдёте до этого момента)

class NotificationObject // класс для описания одного уведомления
{
public:
	NotificationObject();
	NotificationObject(QString _pInfotext, int _pimportance)
	{
		Infotext = _pInfotext;
		importance = _pimportance;
	}
	void setinfo(QString _pInfotext)
	{
		Infotext = _pInfotext;
	}
	void setimp(int _pimportance)
	{
		importance = _pimportance;
	}


	QString getinfo() const
	{
		return Infotext;
	}
	int getimp() const
	{
		return importance;
	}


protected:
	QString Infotext;
	int importance; // 0, 1, 2
};


class NotificationModel : public QAbstractListModel
{
	Q_OBJECT
public:
	explicit NotificationModel(QObject *parent = 0);
	enum ContentRolse {
		InfoRole = Qt::DisplayRole,
		importanceRole = Qt::UserRole + 1,
	};
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	int rowCount(const QModelIndex &parent) const;
	QVariantMap get(int idx) const;
	void add(NotificationObject ct);
	void Delete(int d);
protected:
	QHash<int, QByteArray> roleNames() const;
private:
	QList<NotificationObject> Notifications;

};

class NotificationController : public QObject // класс для описания списка уведомлений и автоматизации работы с ним
{
	Q_OBJECT
public:
	explicit NotificationController(QObject *parent = nullptr);

	void addNotification(QString pInfotext, int pimportance); // добавление уведомления от других модулей кода

	NotificationModel notificationList;

private:

	signals :
	void notifSignal(const int &msg);
public slots:
	void notifSlot(const int &msg);
};

#endif // NOTIFICATIONCONTROLLER_H
