#include "ContensController.h" // TODO переименовать в contentscontroller
#include <QDebug>

ContentsObject::ContentsObject(ContentsObject *parent)
{
    m_parent = parent;
}

ContentsObject::ContentsObject(const ContentsObject &p_copy)
    /*: m_caption(p_copy.getCaption()),
      m_url(p_copy.getUrl())*/
{
    m_parent = p_copy.getParent();
    m_children = p_copy.getChildren();
    m_caption = p_copy.getCaption();
    m_url = p_copy.getUrl();
}

ContentsObject::ContentsObject(ContentsObject *parent,
                               QString p_caption,
                               QUrl p_url)
    : m_caption(p_caption),
      m_url(p_url)
{
    m_parent = parent;
    m_children.clear();
}

QList<ContentsObject*> ContentsObject::getChildren() const
{
    return m_children;
}

void ContentsObject::addChild(ContentsObject *child)
{
    if(child != nullptr)
    {
        try
        {
            m_children.append(child);
            child->setParent(this);
        }
        catch(...) // TODO прописать конкретное исключение
        {
            qDebug() << "В ObjectContents::addChild передан некорректный дочерний элемент";
            return;
        }
    }
}

void ContentsObject::setParent(ContentsObject *child)
{
    m_parent = child;
}


QUrl ContentsObject::getUrl() const//+
{
    return m_url;
}

QString ContentsObject::getCaption() const//+
{
    return m_caption;
}

ContentsObject *ContentsObject::getParent() const
{
    return m_parent;
}
/*
ContentsObject &ContentsObject::operator=(const ContentsObject &rhs)
{
    this->m_caption = rhs.getCaption();
    this->m_url = rhs.getUrl();
    this->m_parent = rhs.getParent();
    this->m_children = rhs.getChildren();
}*/

void ContentsModel::loadTreePathToList(ContentsObject * treeNode)
{
    if(rowCount() > 0)
    {
    beginRemoveRows(QModelIndex(), 0, this->rowCount()-1);
    m_content_items.clear(); // очистка дерева
    endRemoveRows();
    }
    recursiveTrackTreeRoot(treeNode); // рекурсия в отдельную функцию

    return;
}

void ContentsModel::recursiveTrackTreeRoot(ContentsObject * treeNode)
{
    if(treeNode->getParent() != nullptr)
    {
        beginInsertRows(QModelIndex(), 0, 0);
        m_content_items.push_front(treeNode);
        endInsertRows();

        recursiveTrackTreeRoot(treeNode->getParent());
        qDebug()<< treeNode->getParent();
    }

    return;
}
/****************************************************************************************************/

ContentsModel::ContentsModel(QObject *parent)
    : QAbstractListModel(parent)
{
    // TODO прописать загрузку из папок и XML
}

void ContentsModel::addContentItem(ContentsObject *newContentItem) //+
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_content_items << newContentItem;
    endInsertRows();
    // TODO создать папку и XMLl
}

int ContentsModel::rowCount(const QModelIndex & parent) const//+
{
    Q_UNUSED(parent);
    return m_content_items.count();
}

QVariant ContentsModel::data(const QModelIndex & index, int role) const//+
{
    if (index.row() < 0 || index.row() >= m_content_items.count())
        return QVariant();

    ContentsObject * itemToReturn = m_content_items[index.row()];
    if (role == CaptionRole)
        return itemToReturn->getCaption();
    else if (role == ReferenceRole)
        return itemToReturn->getUrl();
    else if (role == PointerRole)
    {
        //qDebug() << itemToReturn;
        qDebug() << &itemToReturn;
        return  (uint64_t)(m_content_items[index.row()]); // TODO reinterpret_cast<uint64_t>(&(m_content_items[index.row()]));
    }

    return QVariant();
}

QHash<int, QByteArray> ContentsModel::roleNames() const //+
{
    QHash<int, QByteArray> roles;
    roles[CaptionRole] = "caption";
    roles[ReferenceRole] = "reference";
    roles[PointerRole] = "thispointer";

    return roles;
}

QVariantMap ContentsModel::get(int idx) const //+
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

void ContentsModel::loadCurrentLevelItems(QList<ContentsObject *> p_items)
{
    if(rowCount() > 0)
    {
        beginRemoveRows(QModelIndex(), 0, this->rowCount()-1);
        m_content_items.clear();
        endRemoveRows();
    }

    // TODO корректная очистка

    for (QList<ContentsObject *>::const_iterator i = p_items.begin();
         i != p_items.end();
         i++)
    {
        ContentsObject * tmp = (*i);
        qDebug() << "loadCurrentLevelItems: caption = " << tmp->getCaption();

        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_content_items.push_back( *i);
        endInsertRows();
    }

}

/****************************************************************************************************/

ContensController::ContensController(QObject *parent)// TODO конструктор
    : QObject(parent)
{
    // загрузить дерево

    rootContentsItem = ContentsObject(nullptr,
                                      QString("Виртуальный корень дерева документации"),
                                      QUrl("")); // корень дерев документов

    rootContentsItem.addChild(new ContentsObject(nullptr, QString("Документ 1"), QUrl("")));
    rootContentsItem.addChild(new ContentsObject(nullptr, QString("Документ 2"), QUrl("")));
    rootContentsItem.addChild(new ContentsObject(nullptr, QString("Документ 3"), QUrl("")));

    m_currentItem = new ContentsObject(nullptr, QString("Параграф 1.1"), QUrl(""));

    openedItems << m_currentItem;

    rootContentsItem.getChildren().at(0)->
            addChild(m_currentItem);
    rootContentsItem.getChildren().at(0)->
            addChild(new ContentsObject(nullptr, QString("Параграф 1.2"), QUrl("")));
    rootContentsItem.getChildren().at(0)->
            addChild(new ContentsObject(nullptr, QString("Параграф 1.3"), QUrl("")));

    m_currentItem->addChild(
                new ContentsObject(nullptr,
                                   QString("Заголовок 1.1.1"),
                                   QUrl("Content/example_doc_1.html")
                                   )
                );

    m_currentItem->addChild(
                new ContentsObject(nullptr,
                                   QString("Заголовок 1.1.2"),
                                   QUrl("Content/example_doc_2.html"))
                );

    m_currentItem->addChild(
                new ContentsObject(nullptr,
                                   QString("Заголовок 1.1.3"),
                                   QUrl("Content/example_doc_3.html")
                                   )
                );

    m_currentItem->addChild(
                new ContentsObject(nullptr,
                                   QString("Заголовок 1.1.4"),
                                   QUrl("Content/example_doc_4.html")
                                   )
                );
    m_currentItem->addChild(
                new ContentsObject(nullptr,
                                   QString("Заголовок 1.1.5"),
                                   QUrl("Content/example_doc_5.html")
                                   )
                );
    m_currentItem->addChild(
                new ContentsObject(nullptr,
                                   QString("Заголовок 1.1.6"),
                                   QUrl("Content/example_doc_6.html")
                                   )
                );

    rootContentsItem.getChildren().at(1)->addChild(new ContentsObject(nullptr, QString("Параграф 2.1"), QUrl("")));
    rootContentsItem.getChildren().at(2)->addChild(new ContentsObject(nullptr, QString("Параграф 3.1"), QUrl("")));

    setCurrentContentsItem(m_currentItem);
}

void ContensController::setCurrentContentsItem(ContentsObject * ptr)
{
    if(ptr != nullptr)
    {
        try
        {
            verticalContentsList.loadCurrentLevelItems(ptr->getChildren());
            horizontalContentsList.loadTreePathToList(ptr);
            m_currentItem = ptr;

            // TODO дополнение списка недавно открытых
        }
        catch(...)// TODO конкретные исключения
        {
            qDebug() << "Ошибка при доступе к текущему элементу содержания, переданному из QML";
            return;
        }
    }
    // TODO если был открыт файл - занесение в список недавно открытых
}

void ContensController::slotSetCurrentContentsItem(int ptr)
{
    if(ptr != 0)
    {
        setCurrentContentsItem(reinterpret_cast<ContentsObject *>(ptr));
    }
}
