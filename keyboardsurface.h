#ifndef KEYBOARDSURFACE_H
#define KEYBOARDSURFACE_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QFrame>
#include <QtDebug>
#include <QLayout>
#include <QPoint>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QMessageBox>

class KeyboardSurface : public QFrame
{
    Q_OBJECT
public:
    enum KeybType{
        spnbox,
        dblspnbox,
        lineedit
    };
    KeyboardSurface(QWidget *parent = 0);
    QLineEdit input;
    QWidget *_parent;
    QPushButton **btn;
    QPushButton *btnDot;
    QPushButton *btnZero;
    QObject *_sender;
    QGridLayout *grid;
    KeybType kbt;
    QLabel *lblName ;
    QLabel *lblMin;
    QLabel *lblMax;
    int cursorPos = 0;
    QFrame *frame;
public slots:
    void show(QObject *sender = 0);
private slots:
    void Ex();
    void AppendText();
    void RemoveText();
    void CursorLeft();
    void CursorRight();
    void Check();
};

#endif // KEYBOARDSURFACE_H
