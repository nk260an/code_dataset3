#include "RecentController.h"

RecentController::RecentController(QObject *parent) : QObject(parent)
{
    recentModel.addRecent(RecentObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url.html")));
    recentModel.addRecent(RecentObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url1.html")));
    recentModel.addRecent(RecentObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url2.html")));
    recentModel.addRecent(RecentObject(QDateTime::currentDateTime(),
                                       QUrl("file:///some_url3.html")));
}

RecentObject::RecentObject(QDateTime p_datetime, QUrl p_url)
    : m_url(p_url),
      m_datetime(p_datetime)

{
    return;
}

QDateTime RecentObject::datetime() const
{
    return m_datetime;
}

QUrl RecentObject::url() const
{
    return m_url;
}

RecentModel::RecentModel(QObject *parent)
    : QAbstractListModel(parent)
{
    // TODO прописать загрузку из папок и XML
}

void RecentModel::addRecent(const RecentObject &newRecent)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_recent_items << newRecent;
    endInsertRows();
    // TODO создать папку и XML
}

int RecentModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_recent_items.count();
}

QVariant RecentModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= m_recent_items.count())
        return QVariant();

    const RecentObject &recentToReturn = m_recent_items[index.row()];
    if (role == DatetimeRole)
        return recentToReturn.datetime();
    else if (role == UrlRole)
        return recentToReturn.url();

    return QVariant();
}

QHash<int, QByteArray> RecentModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DatetimeRole] = "datetime";
    roles[UrlRole] = "url";

    return roles;
}

QVariantMap RecentModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

void RecentModel::clear() // код очистки ListView для Богомолова
{
    if(rowCount() > 0)
    {
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);
    m_recent_items.clear();
    endRemoveRows();
    }
}
