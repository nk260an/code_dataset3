#include "customtype.h"

CustomType::CustomType(QObject *parent) : QObject(parent), myIndentation(0)
{
}

CustomType::CustomType(const CustomType &other)
{
    myUrl = other.myUrl;
    myText = other.myText;
    myIndentation = other.myIndentation;
}

CustomType::~CustomType()
{
}

QUrl CustomType::url()
{
    return myUrl;
}

QString CustomType::text()
{
    return myText;
}

void CustomType::setText(QString text)
{
    myText = text;
    emit textChanged();
}

void CustomType::setUrl(QUrl url)
{
    myUrl = url;
    emit urlChanged();
}

int CustomType::indentation()
{
    return myIndentation;
}

void CustomType::setIndentation(int indentation)
{
    myIndentation = indentation;
    emit indentationChanged();
}
