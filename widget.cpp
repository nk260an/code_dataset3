#include "widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
//    progressBar = new QProgressBar(this);

    tcpSocket = new QTcpSocket(this);

    fileLabel = new QLabel(this);
    progressLabel = new QLabel(this);

    startBtn = new QPushButton(this);
    startBtn->setText("Connect");

    fileBtn = new QPushButton(this);
    fileBtn->setText("Open");

    sendBtn = new QPushButton(this);
    sendBtn->setText("Send");
//    tmr = new QTimer();
//    tmr->setInterval(1000);

    layout = new QGridLayout;
    layout->addWidget(startBtn, 1, 1);
    layout->addWidget(fileBtn, 0, 0);
    layout->addWidget(sendBtn, 0, 1);
    layout->addWidget(fileLabel, 1, 0);
    layout->addWidget(fileLabel, 1, 0);
//    layout->addWidget(progressBar, 2, 0);

    connect(fileBtn, &QPushButton::clicked, this, &Widget::fileOpened);
    connect(sendBtn, &QPushButton::clicked, this, &Widget::onSend);
    connect(startBtn, &QPushButton::clicked, this, &Widget::on_starting_clicked);
//    connect(tmr, SIGNAL(timeout()), this, SLOT(slotRead()));

    setLayout(layout);
}

Widget::~Widget()
{
}

void Widget::on_starting_clicked()
{
    startBtn->setText("Connecting...");
    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(acceptConnection()));
    if (!tcpServer->listen(QHostAddress::Any, 44444) && server_status==0) {
        qDebug() <<  QObject::tr("Unable to start the server: %1.").arg(tcpServer->errorString());
    } else {
        server_status=1;
        qDebug() << QString::fromUtf8("Сервер запущен!");
        startBtn->setText("Running");
    }
}
void Widget::acceptConnection()
{

    qDebug() << QString::fromUtf8("У нас новое соединение!");
    tcpServerConnection = tcpServer->nextPendingConnection();
    connect(tcpServerConnection,SIGNAL(readyRead()),this, SLOT(slotRead()));
//    tcpServer->close();

    QDir::setCurrent("/Users/egor/Desktop/");
    QString fileName();
    QString fileSize;

}

void Widget::slotRead()
{
    QDataStream in(tcpServerConnection);
    QByteArray tmpByteArray;
    if (!isInfoGot) {
        in>>ba;
        double * d2;
        d2 = new double[2];
        d2 = reinterpret_cast<double*>(ba.data());
        qDebug() << d2[0] << " " << d2[1] << " " << d2[2];
        in >> fileName;
        qDebug() << fileName;
        in >> fileSize;
        qDebug() << fileSize;

    }
    QFile loadedFile(fileName);

    if (loadedFile.open(QIODevice::Append))
    {
        while (tcpServerConnection->bytesAvailable())
        {
            qDebug() << "bytesAvailable:" << tcpServerConnection->bytesAvailable();
            in >> tmpByteArray;
            QString some(tmpByteArray);
            if (some=="#END") {
                isInfoGot = false;
                QFileInfo fileInfo(loadedFile);
                qDebug() << "File size:" << loadedFile.size();
                if (loadedFile.size() == fileSize.toInt()) {
                    qDebug() << "Success upload";
                    sendMessage(1);
                } else {
                    qDebug() << "Fail upload";
                    sendMessage(0);
                }

                break;
            }
            loadedFile.write(tmpByteArray);
        }
        loadedFile.close();
    }
}

// TODO: add hostAddr, hostPort to the function
void Widget::sendMessage(int code) {
    QTcpSocket *sock = new QTcpSocket(this);
    sock->connectToHost("127.0.0.1", 33333);
//    QDataStream out(sock);
//    out << code << "hello";
    sock->write(QString::number(code).toUtf8().constData());
    sock->write("hren");
}
void Widget::fileOpened() {
    fileName = QFileDialog::getOpenFileName(this, tr("Open file"));
    QFileInfo fileInfo(fileName);
    QString tmp = fileInfo.fileName();
    tmp +=  " : " + QString::number(fileInfo.size());
    tmp +=  " : " + fileInfo.suffix();
    tmp +=  " : " + fileInfo.baseName();
    tmp +=  " : " + fileInfo.absoluteFilePath();
    fileLabel->setText(tmp);
    qDebug() << fileName;
}

void Widget::onSend() {
    tcpSocket->connectToHost("127.0.0.1", 33333);
    QFile file(fileName);

    QDataStream out(tcpSocket);
    int size = 0;

    if (file.open(QIODevice::ReadOnly))
        {
            QFileInfo fileInfo(file);
            QString fileName(fileInfo.fileName());

            out << fileName;
            qDebug() << fileName;
            out << QString::number(fileInfo.size());
            qDebug() << fileInfo.size();
            out << fileInfo.baseName();
            qDebug() << fileInfo.baseName();
            out << fileInfo.absoluteFilePath();
            qDebug() << fileInfo.absoluteFilePath();

//            progressBar->setMaximum(fileInfo.size());

            while (!file.atEnd())
            {
                QByteArray rawFile;
                rawFile = file.read(5000);
                //false size inc
                QFileInfo rawFileInfo(rawFile);
                size += rawFileInfo.size();
                out << rawFile;
//                progressBar->setValue(rawFile.size());
                qDebug() << "ToSend:"<< rawFile.size();
            }
            out << "#END";
            startServer();
        }
}


void Widget::startServer() {
    qDebug() << "waiting for reply...";
    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(acceptReplyConnection()));
    if (!tcpServer->listen(QHostAddress::Any, 44444)) {
        qDebug() <<  QObject::tr("Unable to start the server: %1.").arg(tcpServer->errorString());
    } else {
        qDebug() << QString::fromUtf8("Сервер-ожидальщик-ответа запущен!");
    }
}


void Widget::acceptReplyConnection() {
    qDebug() << QString::fromUtf8("ответное соединение");
    tcpServerConnection = tcpServer->nextPendingConnection();
    connect(tcpServerConnection,SIGNAL(readyRead()),this, SLOT(slotReadClient()));
    tcpServer->close();

}


void Widget::slotReadClient() {
    QDataStream in(tcpServerConnection);
    QByteArray tmpArr;
    QString tmpString;
    in << tmpArr;
    qDebug() << "bytesAvailable:" << tcpServerConnection->bytesAvailable();
    qDebug() << tmpArr;


    if (tmpString.toInt()==1){
        qDebug() << "successfully sent";
    }else {
        qDebug() << "fail";
    }
}
