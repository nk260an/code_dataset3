#ifndef CONTEXTHELP_H
#define CONTEXTHELP_H

#include <QDialog>
#include <QDir>
#include <QDebug>
#include <QRegExp>

namespace Ui {
class ContextHelp;
}

class ContextHelp : public QDialog
{
    Q_OBJECT

public:
    explicit ContextHelp(QWidget *parent = 0);
    ~ContextHelp();

private slots:

    void on_btnExitFromContextHelp_clicked();

    void on_listWidget_itemClicked();

    void on_lineEdit_textChanged(const QString &arg1);

private:
    Ui::ContextHelp *ui;
    ContextHelp *contexthelp;
    QStringList myList;
};

#endif // CONTEXTHELP_H
