#include "helpsurface.h"

#include <QtDebug>

helpsurface::helpsurface(QWidget *parent)
    : QFrame(parent)
{
    qDebug() << "Вызов конструктора helpsurface";
    this->setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
    this->setStyleSheet("background-color: rgba(255, 255, 255, 50);");
    this->setAttribute(Qt::WA_QuitOnClose, true);
    this->setAttribute( Qt::WA_TranslucentBackground );

    this->hide();

    qDebug() << "Вызов конструктора helphint";
    hlpHint = new helphint(this);
    qDebug() << "Конструктор helphint сработал";

    hlpHint->hide();
}

helpsurface::helpsurface(QWidget *parent,
                         QPushButton *initButton,
                         QMainWindow * mainWindow)
: QFrame(parent)
{
    this->setWindowFlags(/*Qt::Popup |*/ Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    qDebug() << "setWindowFlags OK";
    this->setStyleSheet("background-color: rgba(255, 255, 255, 150);");

    this->setObjectName("helpsurface");
    btnInitializingButton = initButton;
    wgtAppWidget = ((QMainWindow*)(this->parentWidget()))->centralWidget();
    qDebug() << "this->parentWidget() = " << this->parentWidget();
    this->setLayout(new QHBoxLayout);

    this->setGeometry(QRect(QPoint(0, 0),
                            parent->size()));
    qDebug() << "setGeometry OK";

    hlpFrame = new QFrame(this);
    qDebug() << "hlpFrame OK";
    hlpFrame->setStyleSheet("border: 3px solid red;");
    hlpFrame->hide();
    qDebug() << "hlpFrame->hide() OK";
    parent1 = btnInitializingButton->parentWidget();

    qDebug() << "pointGlobal = " << pointGlobal;
    this->hide();


    dictionary.insert(QObject::tr("btnContextHelpOpen"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Показать основную справку"),
                                    QObject::tr("C:/Qt/QProjects/Irkut5/help.png"),
                                    QObject::tr("url")));

    dictionary.insert(QObject::tr("pushButton_37"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Изменить шрифт"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowCalculations"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Сохранить расчеты"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowAircraftLibraryPage"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Просмотр БД самолетов"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowAirportLibraryPage"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Просмотр БД аэропортов"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowSettingsPage"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Изменить настройки"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnExit"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Выход из приложения"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowCruisePage"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Просмотр крейсерского режима"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowLandingPage"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Просмотр режима посадки"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowTakeoffPage"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Просмотр режима взлет"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowGeneralPage"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Задать начальные параметы"),
                                    QObject::tr("C:/Qt/QProjects/Irkut5/help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnShowGeneralPage_2"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Вернуться к начальным параметрам"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("btnAirportLibrary"),
                            strucHintItem(QObject::tr("<b>Кнопка</b>"),
                                    QObject::tr("Просмотр дополнительной информации"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));
    dictionary.insert(QObject::tr("lineEdit"),
                            strucHintItem(QObject::tr("<b>Поле ввода</b>"),
                                    QObject::tr("Сюда вводим название аэропорта"),
                                    QObject::tr("help.png"),
                                    QObject::tr("url")));



    qDebug() << "dictionary " << dictionary;

    hlpHint = new helphint(this);
    qDebug() << "hlpHint OK";
    hlpHint->hide();
}

void helpsurface::drawHint(QPoint pos)
{
    // определяем, на каком компоненте в главном окне произошёл щелчёк
    wgtTarget = wgtAppWidget->childAt(pos);

    if(!wgtTarget)
    {
        hlpFrame->hide();
        hlpHint->hide();
        return;
    }

    // если виджет в списке - рисуется красная рамка и подсказка
    if(wgtTarget != wgtAppWidget)
    {
        qDebug() << "выбран " << wgtTarget;
        QPoint globalTargetPoint = wgtTarget->mapToGlobal(QPoint(0,0));
        qDebug() << globalTargetPoint;

        QPoint pointInMain = parentWidget()->mapFromGlobal(globalTargetPoint);
        qDebug() << pointInMain;
        // рисуем новую рамку
        hlpFrame->setGeometry(QRect(pointInMain,
                                    wgtTarget->geometry().size())); // TODO прописать координаты отосительно главного окна
        hlpFrame->show();

        helpDictionary::const_iterator i;
        qDebug() <<"objectName() " << wgtTarget->objectName();
        i = dictionary.find( wgtTarget->objectName() );
        if( i != dictionary.end() )
        {
            qDebug() <<"caption " << i.value().caption; //reference.caption;
            hlpHint->show(i.value(), wgtTarget->geometry());
        }
        else
        {
            hlpHint->hide();
        }
    }
    else
    {
        hlpFrame->hide();
        hlpHint->hide();
    }


}

void helpsurface::show()
{
    QFrame::show();
    pointGlobal = btnInitializingButton->mapToGlobal(QPoint(0,0));
    btnInitializingButton->setParent(this);
    btnInitializingButton->move(this->mapFromGlobal(pointGlobal));
    btnInitializingButton->show();
}

void helpsurface::hide()
{

    // вернуть кнопку на старое место
    btnInitializingButton->setParent(parent1); // TODO тот виджет, кторый держит место
    qDebug() << "кнопка восстановлена в " << parent1;

    // спозиционировать кнопку обратно
    hlpFrame->hide();
    btnInitializingButton->show();
    qDebug() << "pointGlobal = " << pointGlobal;
    qDebug() << "parent1->mapFromGlobal() = " << parent1->mapFromGlobal(pointGlobal);
    btnInitializingButton->move(parent1->mapFromGlobal(pointGlobal));
    QFrame::hide();
    qDebug() << "видимость окна справки " << this->isVisible();
    parent1->setVisible(true);
    parent1->setMinimumSize(QSize(70,70));
}
