#ifndef WORDTOLATEX_H
#define WORDTOLATEX_H

#include <QObject>
#include <QHash>
#include <QVector>

class ReplaceItem
{
public:
    /*ReplaceItem()
        :rawSymbol(""),newSymbol("")
    {
        unicode_number = 0;
        return;
    }*/

    /*ReplaceItem(uint p_unicode_number = 0,
                QString p_rawSymbol = "",
                QString p_newSymbol = "");*/

    ReplaceItem(QString p_unicode_number = "0",
                QString p_rawSymbol = "",
                QString p_newSymbol = "");

    uint unicode_number = 0;
    QString rawSymbol = "";
    QString newSymbol = "";

};

class WordToLatex : public QObject
{
    Q_OBJECT
public:
    explicit WordToLatex(QObject *parent = nullptr);
    int convert(QString const strInput,
                QString & strOutput);


    void convert_symbols();

    void convert_upper_lower_indices();

    void convert_cyrillic();

    void convert_fractions();

    void convert_functions();

    void convert_array();

    void convert_cases();

    void convert_braces();

    void convert_dia();
private:
    //QHash<QString, QString> m_hashGereek;
    QVector<ReplaceItem> m_greek_table;
    QVector<QString> m_math_fun_table;
    QVector<ReplaceItem> m_brackets_table;
    QVector<ReplaceItem> m_diacritic_table;
    int findMatchBracket(QString const &str,
                                      int pos,
                                      QString smb1,
                                      QString smb2);
    QString strTmp;
signals:

public slots:
};

#endif // WORDTOLATEX_H
