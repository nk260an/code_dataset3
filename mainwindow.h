#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QtSql/QSqlRelationalTableModel>
#include <QSortFilterProxyModel>
#include <QSqlRelationalDelegate>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QTableWidget>
#include <QCompleter>
#include <QVariant>
#include <QSlider>
#include <QPlainTextEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QComboBox>

#include "autosave.h"
#include "keyboardsurface.h"
#include "doublespinbox1.h"
#include "spinbox1.h"
#include "helpsurface.h"
#include "contexthelp.h"

class MyUndoRedoStack;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QStringList *myList;
    helpsurface * helpwindow;
    QSqlDatabase db;
    
    QString filterBuffCodeICAO;
    QString filterBuffAirpName;
    QString filterBuffCountry;
    QString filterBuffRW_lngth;
    QString filterBuffBarAlt;
    QString filterBuffCoord;
    bool dbOpenChecker = false;
//    QSqlRelationalTableModel * modelAir;
//    QSortFilterProxyModel * proxymodelAir;
//    QCompleter * completerAir;
    bool showBD();
private slots:
    void on_btnShowTakeoffPage_pressed();
    void on_btnShowGeneralPage_pressed();
    void on_btnShowLandingPage_pressed();
    void on_btnShowSettingsPage_pressed();
    void on_btnShowCruisePage_pressed();
    void on_btnShowCalculations_pressed();
    void on_btnShowAirportLibraryPage_pressed();
    void on_btnExit_clicked();
    void on_btnResults_Land_pressed();
    void on_btnDataEnterLand_pressed();
    void on_btnResults_pressed();
    void on_cmbAircraftType_currentIndexChanged();
    void on_cmbDepAirport_currentIndexChanged();
    void on_cmbDestAirport_currentIndexChanged();
    void on_btnDataEnter_pressed();
    void on_btnRunwayStatusUP_pressed();
    void on_btnRunwayStatusDOWN_pressed();
    void on_btnEngineConfUP_pressed();
    void on_btnEngineConfDOWN_pressed();
    void on_btnPickUP_pressed();
    void on_btnPickDOWN_pressed();
    void on_btnModeEdsuUP_pressed();
    void on_btnModeEdsuDOWN_pressed();
    void on_btnConfMechUP_pressed();
    void on_btnConfMechDOWN_pressed();
    void on_btnRunwayStatusUP_Land_pressed();
    void on_btnRunwayStatusDOWN_Land_pressed();
    void on_btnModeEdsuUP_Land_pressed();
    void on_btnModeEdsuDOWN_Land_pressed();
    void on_btnEngineConfUP_Land_pressed();
    void on_btnEngineConfDOWN_Land_pressed();
    void on_btnEngineModeUP_Land_pressed();
    void on_btnEngineModeDOWN_Land_pressed();
    void on_btnSettingStyle_pressed();
    void on_btnSettingWeather_pressed();
    void on_btnSettingSave_pressed();
    void on_btnSettingProfile_pressed();
    void on_btnReport_pressed();
    void on_btnStyleBack_pressed();
    void on_btnWeatherBack_pressed();
    void on_btnSaveBack_pressed();
    void on_btnStyleFontDOWN_pressed();
    void on_btnStyleFontUP_pressed();
    void on_lblStyleColourDOWN_pressed();
    void on_lblStyleColourUP_pressed();
    void on_btnWeatherserverDOWN_pressed();
    void on_btnWeatherserverUP_pressed();
    void on_btnWeathermeteoDOWN_pressed();
    void on_btnWeathermeteoUP_pressed();
    void on_btnAvtoSaveDOWN_pressed();
    void on_btnAvtoSaveUP_pressed();
    void on_btnMainProfileBack_pressed();
    void on_btnLogBack_pressed();
    void on_btnRegBack_pressed();
    void on_btnMainLog_pressed();
    void on_btnMainReg_pressed();
    void on_btnReg_pressed();
    void on_btnLog_pressed();
    void on_btnReportBack_pressed();
    void on_btnAirportLibrary_pressed();
    void on_btnAirportLibraryBack_pressed();
    void ShowLink(const QString &Link);
    void on_btnContextHelpMode_clicked(bool checked);
    void on_btnContextHelpOpen_clicked();
    void on_btnCreateReport_clicked();
    void on_lstWidget_doubleClicked();
    void on_btnShowReport_Report_pressed();
    void on_btnShowReport_Confirm_pressed();
    void on_btnShowReport_Cancel_pressed();
    void on_Undo_clicked();
    void on_cmbAircraftType_currentIndexChanged(int index);
    void on_spnWeightTakeoff_valueChanged(double arg1);
    void on_cmbRWSurfStatusTakeoff_currentIndexChanged(int index);
    void on_cmbDepAirport_currentIndexChanged(int index);
    void on_cmbDestAirport_currentIndexChanged(int index);
    void on_swcmbAltAirport_currentIndexChanged(int index);
    void on_spnWindTakeoff_valueChanged(double arg1);
    void on_spnAltitudeBarTakeoff_valueChanged(double arg1);
    void on_cmbEngineIntactTakeoff_currentIndexChanged(int index);
    void on_cmbEngineBleedTakeoff_currentIndexChanged(int index);
    void on_spnSlopeTakeoffRW_valueChanged(double arg1);
    void on_spinTemperatureTakeoffRW_valueChanged(int arg1);
    void on_cmbFBWCSmodeTakeoff_currentIndexChanged(int index);
    void on_cmbFlapsModeTakeoff_currentIndexChanged(int index);
    void on_cmbRWSurfStatusLanding_currentIndexChanged(int index);
    void on_spnWindLanding_valueChanged(double arg1);
    void on_spnWeightLanding_valueChanged(double arg1);
    void on_spinAltitudeBarLanding_valueChanged(double arg1);
    void on_spinSlopeLandingRW_valueChanged(double arg1);
    void on_spinTemperatureLanding_valueChanged(const QString &arg1);
    void on_spinTemperatureLanding_valueChanged(int arg1);
    void on_cmbFBWCSmodeLanding_currentIndexChanged(int index);
    void on_cmbFlapsModeLanding_currentIndexChanged(int index);
    void on_cmbEngineIntactLanding_currentIndexChanged(int index);
    void on_spnCruiseDistance_valueChanged(double arg1);
    void on_spnCruiseFuel_valueChanged(double arg1);
    void on_spnCruiseMass_valueChanged(double arg1);
    void on_cmbStyleshrift_currentIndexChanged(int index);
    void on_spinTemperatureTakeoffRW_3_valueChanged(int arg1);
    void on_cmbStylesxema_currentIndexChanged(int index);
    void on_cmbWeatherserver_currentIndexChanged(int index);
    void on_spinTemperatureTakeoffRW_4_valueChanged(int arg1);
    void on_cmbWeathermeteo_currentIndexChanged(int index);
    void on_cmbSaveavtosave_currentIndexChanged(int index);
    void on_spinTemperatureTakeoffRW_5_valueChanged(int arg1);
    void on_spinTemperatureTakeoffRW_6_valueChanged(int arg1);
    void on_btnSettingDefault_clicked();
    void on_btnContextHelpMode_clicked();
    void on_spnWindTakeoff_clicked(); 
    void on_btn_ClearFilterBD_clicked();
    void on_btn_Ch_Dep_Airport_clicked();
    void on_btn_Ch_Dest_Airport_clicked();
    void on_btn_Change_Dep_Airp_clicked();
    void on_btn_Change_Dest_Airp_clicked();
    void on_twAirLibFilter_cellChanged(int row, int column);
    
    //БД
    void on_btn_loadAirBD_clicked();
    void on_twAirLib_activated(const QModelIndex &index);
    //Фильтр БД
    void on_lineEditBD_textChanged(const QString &arg1);
    
    //Autosave
    void timerevent();
    void init();
    
    void IfInitialDataAssigned();
    void CalculateLanding();
private:
    QPushButton * currentairportbtn;
    MyUndoRedoStack * UndoRedoStack;
    QString reportFolder = QDir::currentPath() + "/reports";
    QString currentReportDirectory = "";
    Autosave *config;
    ContextHelp *contexthelp;
    Ui::MainWindow *ui;
	QString reportDirname;
};

class FlightData
{
public:
    enum enmFlightDataType
    {
        typeDouble,
        typeString,
        typeInteger
    };
public:
    FlightData()
    {

    }

    FlightData(double p_data)
    {
        data = p_data;
        data_type = typeDouble;
    }

    FlightData(QString p_data)
    {
        data = p_data;
        data_type = typeString;
    }

    FlightData(int p_data)
    {
        data = p_data;
        data_type = typeInteger;
    }

    QVariant data;
    enmFlightDataType data_type;


};

class MyUndoRedoStack
{
    //Класс для записи объекта и его значения при изменении
    class MyUndoRedoRecord
    {
    public:
        QObject * Object;
        QString ClassName;
        QVariant PrevValue;
    };
    
    //Для Switch()
    enum ENM_CLASS
    {
        ENM_QPushButton,
        ENM_QCheckBox,
        ENM_QRadioButton,
        ENM_QLineEdit,
        ENM_QSlider,
        ENM_QSpinBox,
        ENM_QDoubleSpinBox,
        ENM_QComboBox
    };
public:
    MyUndoRedoStack(int _maxsteps)
    {
        CastTypeToEnum();
        if(_maxsteps > 0)
        this->maxsteps = _maxsteps;
    }
    bool restoring = false;
    
    //UndoRedoRecordList для откатов, UndoRedoBuffer содержит значение(значение после загрузки программы не сохраняется)
    QList<MyUndoRedoRecord *> UndoRedoRecordList;
    QList<MyUndoRedoRecord *> UndoRedoBuffer;
    int maxsteps = 30; //Максимальное число шагов
    int steps = 0;
    
    //Маппинг классов со значениями для Switch()
    std::map <QString, ENM_CLASS> mapping_class;

    void CastTypeToEnum()
    {
        //Инициализация маппинга
        mapping_class["QPushButton"] = ENM_QPushButton;
        mapping_class["QCheckBox"] = ENM_QCheckBox;
        mapping_class["QRadioButton"] = ENM_QRadioButton;
        mapping_class["QLineEdit"] = ENM_QLineEdit;
        mapping_class["QSlider"] = ENM_QSlider;
        mapping_class["QSpinBox"] = ENM_QSpinBox;
        mapping_class["QDoubleSpinBox"] = ENM_QDoubleSpinBox;
        mapping_class["QComboBox"] = ENM_QComboBox;
    }

    void RestoreValue()
    {
        //Если не осталось шагов return
        if(steps == 0)
            return;
            
        //Извлкаем последний элемент из стека
        MyUndoRedoRecord * undorecord = UndoRedoRecordList.takeLast(); // восстановить значение
        foreach (MyUndoRedoRecord* a, UndoRedoBuffer) {
            if(a->Object == undorecord->Object){
                a->PrevValue = undorecord->PrevValue; //в буффере текущее значение, заменяем его
                break;
            }
        }
        
        //При изменении значения предотвращаем выполнение MemoValue()
        restoring = true;
        steps--; //"Освобождаем" шаг
        
        //Восстановление значения
        switch(mapping_class[undorecord->ClassName])
        {
        case ENM_QPushButton:
        {
            QPushButton((QWidget *)undorecord->Object).setChecked(undorecord->PrevValue.toBool());
            break;
        }
        case ENM_QCheckBox:
            ((QCheckBox *)undorecord->Object)->setChecked(undorecord->PrevValue.toBool());
            break;
        case ENM_QRadioButton:
            // TODO разобраться с группами
            break;
        case ENM_QLineEdit:
            ((QLineEdit *)undorecord->Object)->setText(undorecord->PrevValue.toString());
            break;
        case ENM_QSlider:
            ((QSlider *)undorecord->Object)->setValue(undorecord->PrevValue.toInt());
            break;
        case ENM_QSpinBox:
            ((QSpinBox *)undorecord->Object)->setValue(undorecord->PrevValue.toDouble());
            break;
        case ENM_QDoubleSpinBox:
            ((QDoubleSpinBox *)undorecord->Object)->setValue(undorecord->PrevValue.toDouble());
            break;
        case ENM_QComboBox:
            ((QComboBox *)undorecord->Object)->setCurrentIndex(undorecord->PrevValue.toInt());
            break;
        default:
            break;
        }
    }
    
    void MemoValue(QObject * object_sender)
    {
        //Если эта функция вызвана при восстановлении значения
        if(restoring)
        {
            restoring = false;
            return;
        }
        else
        {
            MyUndoRedoRecord *record = new MyUndoRedoRecord();
            record->ClassName = object_sender->metaObject()->className(); //Имя класса
            record->Object = object_sender; //Object
            switch (mapping_class[object_sender->metaObject()->className()]) {
            case ENM_QCheckBox:
                record->PrevValue = object_sender->property("checked");
                break;
            case ENM_QSpinBox:
                record->PrevValue = object_sender->property("value");
                break;
            case ENM_QDoubleSpinBox:
                record->PrevValue = object_sender->property("value");
                break;
            case ENM_QLineEdit:
                record->PrevValue = object_sender->property("text");
                break;
            case ENM_QPushButton:
                record->PrevValue = object_sender->property("checked");
                break;
            case ENM_QRadioButton:
                record->PrevValue = object_sender->property("checked");
                break;
            case ENM_QSlider:
                record->PrevValue = object_sender->property("value");
                break;
            case ENM_QComboBox:
                record->PrevValue = object_sender->property("currentIndex");
                break;
            default:
                break;
            }
            
            //Поиск элеммента в буффере
            foreach (MyUndoRedoRecord* a, UndoRedoBuffer) {
                if(a->Object == record->Object){
                    MyUndoRedoRecord *b = new MyUndoRedoRecord; //QList НЕ КОПИРУЕТ в стек поэтому делаем копию и сохраняем ее в конце MyUndoRedoList
                    b->ClassName = a->ClassName;
                    b->Object = a->Object;
                    b->PrevValue = a->PrevValue;
                    //Если достигнуто максимальное количество шагов
                    if(maxsteps == steps){
                        UndoRedoRecordList.pop_front();
                    }else{
                        //"Занимаем" шаг
                        steps++;
                    }
                    UndoRedoRecordList.push_back(b);
                    //перезаписываем значение в буффере
                    a->PrevValue = record->PrevValue;
                    return;
                }
            }
            
            //Помещаем элемент в буффер если его значение изменяется первый раз
            UndoRedoBuffer.push_back(record);
        }
    }
};

#endif // MAINWINDOW_H
