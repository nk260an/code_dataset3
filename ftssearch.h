#ifndef FTSSEARCH_H
#define FTSSEARCH_H

#include <QObject>
#include <QList>
#include <QString>
#include <QUrl>
#include <QAbstractListModel>
#include <QVariant>
#include <QtSql>

class Content
{
public:
    Content(QString _caption, QString _slovo, QUrl _url)
    {
        caption = _caption;
        slovo = _slovo;
        url = _url;
    }
    void setcaption (QString _caption)
    {
        caption = _caption;
    }
    void setslovo (QString _slovo)
    {
        slovo = _slovo;
    }
    void seturl (QUrl _url)
    {
        url = _url;
    }
    QString getcaption() const
    {
        return caption;
    }
    QString getslovo() const
    {
        return slovo;
    }
    QUrl geturl() const
    {
        return url;
    }
protected:
    QString caption;
    QString slovo;
    QUrl url;
};

//------------------------

class FtsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit FtsModel(QObject *parent = 0);
enum ContentRolse {
        CaptionRole = Qt::DisplayRole,
        SlovoRole = Qt::UserRole + 1,
        ReferenceRole = Qt::UserRole + 2
    };
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent) const;
    QVariantMap get(int idx) const;
    void add(Content ct);
    void Delete();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<Content> Contents;

};

//----------------------------


class ftssearch : public QObject
{
    Q_OBJECT
public:
   explicit ftssearch(QObject *parent = 0);
    QSqlDatabase db;

FtsModel ftsList;
signals:
    void ftssearchSignal(const QString &msg);
public slots:
    void ftsSlot(const QString &msg);
};

#endif // FTSSEARCH_H
