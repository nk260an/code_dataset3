#ifndef MODELCONTENTS_H
#define MODELCONTENTS_H

#include <QObject>
#include <QList>
#include <QString>
#include <QUrl>
#include <QAbstractListModel>
#include <QVariant>
//#pragma once

class ContentsObject/* : public QObject*/
{
   /* Q_OBJECT*/
public:
    ContentsObject(ContentsObject *parent = 0);

    ContentsObject(const ContentsObject &p_copy);

    ContentsObject(ContentsObject *parent,
                            QString p_caption,
                            QUrl p_url);

    QString getCaption() const;
    QUrl getUrl()  const;
    ContentsObject* getParent() const;
    QList<ContentsObject *> getChildren() const;
    //QVariant getThisPointer() const;

    void addChild(ContentsObject * child);
    void setParent(ContentsObject * child);

    //ContentsObject & operator=(const ContentsObject & rhs);


protected:
    QString m_caption;
    QUrl m_url;
    ContentsObject * m_parent;
    QList<ContentsObject*> m_children;
};

class ContentsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataRoles {
        CaptionRole = Qt::UserRole + 1,
        ReferenceRole,
        PointerRole
    };

    ContentsModel(QObject *parent = 0);

    void addContentItem(ContentsObject * newContentItem); //+

    int rowCount(const QModelIndex & parent = QModelIndex()) const;//+

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const; //+
    QVariantMap get(int idx) const; //+
    void loadTreePathToList(ContentsObject * treeNode); // трассирует до корневого узла дерева и составляет список (для горизонтального меню)
    void loadCurrentLevelItems(QList<ContentsObject*> p_items); // для вертикального списка (содержание открытого узла)
    void recursiveTrackTreeRoot(ContentsObject *treeNode);
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<ContentsObject*> m_content_items;

};

class ContensController : public QObject// !!! унаследовать от QObject TODO
{
    Q_OBJECT

/* === ВОПРОС О ПЕРЕДАЧЕ ВЫБРАННОГО ЭЛЕМЕНТА ИЗ QML ===
 *
 * 1) передавать ObjectContents* через QVariant
 *
 * НЕДОСТАТКИ:
 *  - преобразование типов и возможные ошибки
 *  - нужно добавить в ObjectContents интерфейс для QML
 *
 * ПРЕИМУЩЕСТВА
 *  + самый быстрый переход
 *
 * 2) передавать из QML URL, по URL находить в QHash указатель,
 * QHash из пар "URL" - "ObjectContents*" формируется при построении дерева
 *
 * НЕДОСТАТКИ
 *  - замедление перехода из-за поиска по QHash по сравнению с 1)
 *  - формировать и хранить QHash, дублирующий информацию дерева
 *  - возможность ошибок из за вольного формата URL
 *
 * ПРЕИМУЩЕСТВА
 *  + не нужно преобразовывать типы ObjectContents* <-> QVariant
 *
 * 3) ввести уникальный ID для каждого узла дерева содержания и передавать его из QML,
 * искать указатель опять же по QHash среди пар "id" - "ObjectContents*"
 *
 * НЕДОСТАТКИ
 *  - замедление перехода из-за поиска по QHash по сравнению с 1)
 *  - необходимость уже при формировании контента назначать каждому узлу содержания уникальный ID
 *  - формировать и хранить QHash, дублирующий информацию дерева
 *
 * ПРЕИМУЩЕСТВА
 *  + не нужно преобразовывать типы ObjectContents* <-> QVariant
 *  + по сравнению с 2) ID точно уникальные
*/

public:
    ContensController(QObject *parent = nullptr);

    ContentsObject rootContentsItem; // корневой элемент дерева содержания
    QList<ContentsObject*> openedItems; // (на будущее) хранение открытых документов (например, для вкладок)

    QList<ContentsObject> lastDocuments; // список последних открытых
    QList<ContentsObject> favoritesItems; // список избранных

    ContentsModel verticalContentsList; // модель вертикального списка для активной вкладки - только для обновления GUI
    ContentsModel horizontalContentsList; // модель горизонтального списка для активной вкладки - только для обновления GUI

    ContentsObject * getCurrent() // GET- метод для текущего элемента
    {
        return m_currentItem;
    }

    void setCurrentContentsItem(ContentsObject* ptr); // SET- метод для текущего элемента
public slots:
    void slotSetCurrentContentsItem(int ptr); // из QML указатель на текущий элемент передаётся в виде int для предотвращения ошибок при неявном преобразовании типов

protected:
    ContentsObject * m_currentItem;

};


#endif // MODELCONTENTS_H
