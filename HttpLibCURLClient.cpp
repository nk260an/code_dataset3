#include "HttpLibCURLClient.h"
#define CURL_STATICLIB
#include <curl/curl.h>

#ifdef _WIN64
	#pragma comment (lib, "Normaliz.lib")
	#pragma comment (lib, "Ws2_32.lib")
	#pragma comment (lib, "Wldap32.lib")
	#pragma comment (lib, "Crypt32.lib")
	#pragma comment (lib, "advapi32.lib")
#elif _WIN32
	#pragma comment (lib, "Normaliz.lib")
	#pragma comment (lib, "Ws2_32.lib")
	#pragma comment (lib, "Wldap32.lib")
	#pragma comment (lib, "Crypt32.lib")
	#pragma comment (lib, "advapi32.lib")
#endif

// TODO ������ ������������ curl ��� Linux ������ ���� �������

#if defined(_WIN64) && defined(_DEBUG)
	#pragma comment (lib, "./curl-7.61.1/builds/libcurl-vc15-x64-debug-static-ipv6-sspi-winssl/lib/libcurl_a_debug.lib")
#elif defined(_WIN64) && defined(_RELEASE)
	#pragma comment (lib, "./curl-7.61.1/builds/libcurl-vc15-x64-release-static-ipv6-sspi-winssl/lib/libcurl_a.lib")
#elif defined(_WIN32) && defined(_DEBUG)
	#pragma comment (lib, "./curl-7.61.1/builds/libcurl-vc15-x86-release-static-ipv6-sspi-winssl/lib/libcurl_a_debug.lib")
#elif defined(_WIN32) && defined(_RELEASE)
	#pragma comment (lib, "./curl-7.61.1/builds/libcurl-vc15-x86-release-static-ipv6-sspi-winssl/lib/libcurl_a.lib")
#endif

// TODO ������ ������������ curl ��� Linux ������ ���� �������

HttpLibCURLClient::HttpLibCURLClient()
{
}

HttpLibCURLClient::~HttpLibCURLClient()
{
}

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

int download_files_libcurl(
	const char * url = "toshi.nofs.navy.mil",
	const char * uri = "/ser7/finals.daily",
	const char * filepath = "")
{
	CURL *curl = nullptr;
	FILE *fp;
	CURLcode res;
	//const char *url = "http://localhost/aaa.txt";
	char outfilename[FILENAME_MAX] = "C:\\bbb.txt";
	curl = curl_easy_init();
	if (curl) {
		fopen_s(&fp, outfilename, "wb");
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		res = curl_easy_perform(curl);
		/* always cleanup */
		curl_easy_cleanup(curl);
		fclose(fp);
	}
	return 0;
}