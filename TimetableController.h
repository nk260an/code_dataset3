#ifndef TIMETABLECONTROLLER_H
#define TIMETABLECONTROLLER_H
#include <QObject>
#include <QTimer>
#include <QTime>
#include <QDir>
#include <QStringList>
#include <QString>
#include <QDate>
#include <QFileInfo>
#include <QTextStream>
#include <QTextCodec>
#include <QAbstractListModel>
#include <QVariant>
#include <QDateTime>
#include <QUrl>
#include <QDesktopServices>

class TimetableObject
{
public:
    TimetableObject(const QTime &datetime1,
                     const QTime &datetime2,
                     const QString &rawdatetime1,
                     const QString &rawdatetime2,
                     const QString &caption,
                      const QUrl &reference,
                     const QString &status,
                    const double &start,
                    const double &duration,
                    const int &notice);//+

    QTime dateTime1() const; //+
    QTime dateTime2() const; //+
    QString rawDateTime1() const;//+
    QString rawDateTime2() const;//+
    QString caption() const;//+
    QUrl reference() const;//+
    QString status()const;
    double start() const;
    double duration() const;
    int notice() const;

private:
    QTime m_datetime1;
    QTime m_datetime2;

    QString m_raw_datetime1;
    QString m_raw_datetime2;

    QString m_caption;
    QUrl m_reference;
    QString m_status;
    double m_start;
    double m_duration;
    int m_notice;

};

// несмотря на то, что в группе делали модель на QList, оказалось, что она не обновляет ListView в GUI
// поэтому создали аналогичный класс, наследованный от QAbstractListModel

class TimetableModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataRoles {
        Datetime1Role = Qt::UserRole + 1,
        DateTime2Role,
        RawDateTime1Role,
        RawDateTime2Role,
        CaptionRole,
        ReferenceRole,
        StatusRole,
        StartRole,
        DurationRole,
        NoticeRole
    };

    TimetableModel(QObject *parent = 0);

    void addTask(const TimetableObject & newTask);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    Q_INVOKABLE void clearModel();
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;
    Q_INVOKABLE void openUrl(const QUrl file);
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<TimetableObject> m_timetable_items;

};

class TimetableController : public QObject
{
    Q_OBJECT
public:
    explicit TimetableController(QObject *parent = 0);
    void loadModel(); // загрузить расписание из файла
    TimetableModel timetableModel;
    QTimer tmrDayShift;
    QDate dateOldDate;
    void lookForTimetables();
    int Times;
    int It;
    QStringList listofevents;
public slots:
    void processDateShift();
};

#endif // TIMETABLECONTROLLER_H
