#include "timetablecontroller.h"

TimetableController::TimetableController(QObject *parent)
 : QObject(parent)
{
    dateOldDate = QDate::currentDate();// задать текущую дату
    lookForTimetables();
    connect(&tmrDayShift, SIGNAL(timeout()),
            this, SLOT(processDateShift()));
    tmrDayShift.setInterval(30*1000);
    tmrDayShift.start();
}

void TimetableController::loadModel()
{
QStringList listofevents;
QStringList parslist;
QFile file("../bin/Content/Timetable/timetable_05.04.2017.txt");
QTextStream textStream(&file);
QString Time;
QString Caption;
QString Reference;
while (true)
{
    QString line = textStream.readLine();
    if (line.isNull())
        break;
    else
        listofevents.append(line);
}
file.close();
for (int i = 0; i < listofevents.count(); i++) //Парс
{
parslist = listofevents[i].split('  ');
 if (parslist.count()<=3) for (int j = 0; j < parslist.count(); j++)
 {
     if (j == 0) Time = parslist[j]; else
         if (j == 1) Caption = parslist[j]; else
              Reference = parslist[j];
 }
 TimetableObject TmpData(QTime::fromString(Time.left(5), "HH:mm"),
                                        QTime::fromString(Time.right(5), "HH:mm"),
                                        Time.left(5),
                                        Time.right(5),
                                        Caption,
                                        QUrl(Reference));
 timetableModel.addTask(TmpData);
}

}

void TimetableController::processDateShift()
{
   // проверить, произошла ли смена дат
   if (dateOldDate != QDate::currentDate())
   {
       dateOldDate = QDate::currentDate();
       lookForTimetables();// если произошла - вызвать всю последовательность
   }
    return;
}

void TimetableController::lookForTimetables()
{
   QDir dir("../bin/Content/Timetable");
   int iter = 0;
   QStringList files;
   dir.setNameFilters(QStringList("timetable*.txt"));
   dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
   files = dir.entryList();
   for (int i=0; i<files.count(); i++)
   {
       if (files[i] == "timetable_" + dateOldDate.toString() + ".txt" && iter == 0)
       {
           loadModel();
           iter++;
       }
   }
  // if (n == 0)
    return;

}



