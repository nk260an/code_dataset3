// FFTOpenCV.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

//http://docs.opencv.org/trunk/d8/d01/tutorial_discrete_fourier_transform.html

using namespace cv;
using namespace std;

int main()
{
	//help();
	//const char* filename = argc >= 2 ? argv[1] : "../data/lena.jpg";
	const char* filename = "../../img/2.jpg";
	Mat I = imread(filename, IMREAD_GRAYSCALE);
	imshow("Input Image", I);
	imshow("",I(Rect(400, 400, 400, 400)));

	if (I.empty()) {
		cout << "Error opening image" << endl;
		return -1;
	}

	/* настройка для DFT */

                          //expand input image to optimal size




	/* вырезка окна */
	int window_width = 200; // ширина окна 200
	int window_height = 200; // высота окна 200
	Rect maxRect(0, 0, window_width, window_height);
	double max_mag = 0.0, min_mag = 0.0;
	/*
	for (int i = 0; i < (I.rows - window_height); i += 50) //y
	{
		for (int j = 0; j < (I.cols - window_width); j += 50) //x
		{
			
			//Mat tmp = I(cv::Rect(j, i, window_height, window_height));
			//B.copyTo(tmp);

			// подсчёт размера изображения с пустоё каёмкой, необъодимой для DFT 
			int m = getOptimalDFTSize(window_width); // I.rows высота исходного изображения
			int n = getOptimalDFTSize(window_height); // I.cols - ширина исходного изображения

			// по краю изображения добавляется пустая каёмка (нулевые пиксели) - эта добавка нужна для нормальной работы DFT
			//copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));
			Mat padded;  
			copyMakeBorder(I(Rect(j, i, window_width, window_width)), padded,
				i, m- window_width,
				j, n- window_height,
				BORDER_CONSTANT, Scalar::all(0));

			imshow("Cropped", I(Rect(j, i, window_width, window_width)));
			imshow("Padded", padded);

			// массив из 2 матриц
			Mat planes[] = { Mat_<float>(padded),//в planes[0] - матрица исходного изображения в каёмкой, причём тип пикселей перев
							Mat::zeros(padded.size(), CV_32F)//в planes[1] - пока пустая матрица
			};

			Mat complexI; // создаётся матрица для хранения действительной и комплексной части спектра после DFT
			merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

			// собственно, функция для Дискретного Перобразования Фурье
			dft(complexI, complexI);            // this way the result may fit in the source matrix
												// compute the magnitude and switch to logarithmic scale
												// => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))

			split(complexI, planes);                   // разделить в planes[0] дейсвительную часть спектра Re(DFT(I)),
														//а в planes[1] - мнимую часть спектра Im(DFT(I))

			magnitude(planes[0], planes[1], planes[0]); // в planes[0] записывается амплитуда

			Mat magI = planes[0];

			// TODO максимальная амплитуда
			double tmp_max = 0.0, tmp_min = 0.0;
			cv::minMaxLoc(magI, &tmp_min, &tmp_max);
			if (tmp_max > max_mag)
			{
				max_mag = tmp_max;
				maxRect = Rect(i, j, window_width, window_height);
			}

			/* выключить графику из примера
				magI += Scalar::all(1);                    // шкала переключается из линейной в логарифмическую
				log(magI, magI);

				// crop the spectrum, if it has an odd number of rows or columns
				magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

				// rearrange the quadrants of Fourier image  so that the origin is at the image center
				int cx = magI.cols / 2;
				int cy = magI.rows / 2;

				Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
				Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
				Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
				Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

				Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
				q0.copyTo(tmp);
				q3.copyTo(q0);
				tmp.copyTo(q3);
				q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
				q2.copyTo(q1);
				tmp.copyTo(q2);
				normalize(magI, magI, 0, 1, NORM_MINMAX); // Transform the matrix with float values into a
															// viewable image form (float between values 0 and 1).

				imshow("Input Image", I);    // Show the result
				imshow("spectrum magnitude", magI);
			*/
	/*	}
	}
	*/
	//rectangle( I, maxRect, Scalar(0, 0, 255), 4);
	/*rectangle( I,// само Mat- изображение
		Rect(x,y, width, height),// 
		Scalar(0, 0, 255),// цвет
		4);// вроде толщина обводки*/
	imshow("Input Image", I);    // Show the result

	waitKey();

	return 0;

}