#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

// ������� �� boost:asio
// https://www.boost.org/doc/libs/1_45_0/doc/html/boost_asio/example/http/client/async_client.cpp
// beast https://www.boost.org/doc/libs/1_68_0/libs/beast/example/http/client/async/http_client_async.cpp

using boost::asio::ip::tcp;

class HttpAsioClient
{
public:
	~HttpAsioClient();

	HttpAsioClient(boost::asio::io_context& io_context);
	int download_file(const std::string& host,
						const std::string& path,
						std::ostream * pclient_output); // TODO �������� �� ��� �����
private:
	void handle_resolve(const boost::system::error_code& err,
		const tcp::resolver::results_type& endpoints);

	void handle_connect(const boost::system::error_code& err);

	void handle_write_request(const boost::system::error_code& err);

	void handle_read_status_line(const boost::system::error_code& err);

	void handle_read_headers(const boost::system::error_code& err);

	void handle_read_content(const boost::system::error_code& err);

	tcp::resolver resolver_;
	tcp::socket socket_;
	boost::asio::streambuf request_;
	boost::asio::streambuf response_;
	std::ostream * client_output;
};