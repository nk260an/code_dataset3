#ifndef VELOCITYINDICATORWIDGET_H
#define VELOCITYINDICATORWIDGET_H

#include <QWidget>

class VelocityIndicatorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit VelocityIndicatorWidget(QWidget *parent = 0);
    void SetDisplay(double dblVmax, double Vls, double dblV_AOA_prot, double dblV_AOA_max, double dblV_AOA_sw);

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
};

#endif // VELOCITYINDICATORWIDGET_H
