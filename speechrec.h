#ifndef SPEECHREC_H
#define SPEECHREC_H

#include <QMediaRecorder>
#include <QUrl>
#include <QAudioRecorder>
#include <QObject>

#ifdef __MINGW64__
    #include <pocketsphinx.h>
#elif _MSC_VER

#endif


class speechrec : public QObject
{
    Q_OBJECT
public:
    explicit speechrec(QObject *parent = nullptr);

    QUrl c = QUrl::fromLocalFile(QString(PRO_FILE_PWD) + "/lib/test.raw");

    FILE *fh;
    char const *hyp, *uttid;
    int rv;
    #ifdef __MINGW64__
        ps_decoder_t *ps;
        cmd_ln_t *config;
        int16 buf[512];
        int32 score;
    #elif _MSC_VER

    #endif


signals:
    void speechSignal(const QString &msg);
public slots:
    void speechSlot(const bool &msg);
private:
    QAudioRecorder *m_audioRecorder = nullptr;
};

#endif // SPEECHREC_H
