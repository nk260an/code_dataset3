#ifndef COMMON_H
#define COMMON_H

#endif // COMMON_H

enum FlapsConf // тип данных для обозначения конфигурации механизации
{
    FLAPS_0,
    FLAPS_1,
    FLAPS_1_PLUS_F,
    FLAPS_2,
    FLAPS_3,
    FLAPS_3sv,
    FLAPS_FULL,
    FLAPS_FULLsv
};

enum RunwayStatus // тип данных для обозначения качества поверхности ВВП
{
    RW_STATUS_DRY,
    RW_STATUS_WET,
    RW_STATUS_63_WATER,
    RW_STATUS_127_WATER,
    RW_STATUS_63_SLUSH,
    RW_STATUS_127_SLUSH,
    RW_STATUS_COMPACTED_SNOW,
    RW_STATUS_ICE
};

enum EFBErrCodes // тип данных для обозначения типа ошибки
{
    ERR_OUT_OF_RANGE
};


enum BrakingMode // режим работы системы торможения
{
    BM_LOW,
    BM_MED,
    BM_MAX
};

enum FBWCS_mode // режим работы СДУ (сисдемы дистанционного управления) (также ЭДСУ, FBWCS - fly-by-wire control system)
{
    FBWCS_DIRECT_MODE,
    FBWCS_NORMAL_MODE
};

enum EngineBleedAirMode // рехим отбора из двигателя
{
    BLEED_AIRCOND, // только кондиционирование воздуха
    BLEED_AIRCOND_ENGINE_ANTI_ICE, // кондиционирование + противооблединительная система мотогондол
    BLEED_AIRCOND_ENGINE_WING_ANTIICE // кондиционирование + противооблединительная система мотогондол и крыльев
};
