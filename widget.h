#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QDebug>
#include <QString>
#include <QLabel>
#include <QNetworkAccessManager>
#include <QTcpSocket>
#include <QFile>
#include <QDataStream>
#include <QProgressBar>
#include <QTcpServer>
#include <QTime>
#include <QTimer>


namespace Ui {
class Widget;
}
class QTcpServer;
class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    QFileDialog *fileDialog;
    QGridLayout *layout;
    QPushButton *fileBtn;
    QPushButton *sendBtn;
    QPushButton *startBtn;
    QLabel *fileLabel;
    QLabel *progressLabel;
    QProgressBar *progressBar;
    QTcpServer *tcpServer;
    QTcpSocket *tcpServerConnection;
    QTimer *tmr;
    QByteArray ba;

    QString fileName;
    QString str1;
    QString str2;
    QString str3;
    QTcpSocket *tcpSocket;
     int server_status;
     bool isInfoGot = false;
     QString fileSize;
     bool isset;

    void startServer();
    void sendMessage(int code);

    ~Widget();
public slots:
    void fileOpened();
    void onSend();
    void acceptReplyConnection();
    void slotReadClient();
    void on_starting_clicked();
    void acceptConnection();
    void slotRead();

};

#endif // WIDGET_H
