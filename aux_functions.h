#ifndef AUX_FUNCTIONS_H
#define AUX_FUNCTIONS_H

#endif // AUX_FUNCTIONS_H

double getLinearInterpolationScalar1D( double const dblX,
                                      double const dblX1,
                                      double const dblX2,
                                      double const dblY1,
                                      double const dblY2);
/* +++функция для линейной одномерной интерполяции */

double getLinearInterpolationVector1D(double const dblXArray[], // массив значений аргумента, для которых известно значение функции; массив должен быть монотонно возрастающий
                                      double const dblYArray[], // массив значений функции для аргументов dblXArray[]
                                      double const dblX, // аргумент, для которого нужно проинтерполировать функцию
                                      int const intImax); // максимальный индекс массивов, [длина массива - 1]
/* фукнция для линейной одномерной интерполяции в массиве */


/*+++ функция для вычисления квадрата гипотенузы */
double getHypotenuse2(double const dblX,
                     double const dblY);

double getRmin(double const dblXArray[], // массив значений аргумента, для которых известно значение функции; массив должен быть монотонно возрастающий
               double const dblYArray[], // массив значений функции для аргументов dblXArray[]
               double const dblX, // координата X точки, до которой нужно определить расстояние
               double const dblY, // координата Y точки, до которой нужно определить расстояние
               int const intImax); // максимальный индекс массивов, [длина массива - 1]

double getDistanceLinePoint(double const A[3], // точка, до которой определяется расстояние, [x, y, z]
                            double const P1[3], // первая точка кривой, [x1, y1, z1]
                            double const P2[3]); // вторая точка кривой, [x2, y2, z2]
/* +++расстояние от точки до прямой, заданной двумя точками */
