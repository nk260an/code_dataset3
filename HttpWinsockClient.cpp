#include "HttpWinsockClient.h"

#include <windows.h>
#include <ws2tcpip.h>
#include <urlmon.h>
#include <sys/types.h>
#pragma comment (lib, "urlmon.lib")
#pragma comment(lib,"ws2_32.lib")

HttpWinsockClient::HttpWinsockClient()
{
	// TODO � �����������
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cout << "WSAStartup failed.\n";
		return;
	}

	/* ������� ����� �� ��������� */
	Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cout << "socket() failed.\n";
		return;
	}
}

HttpWinsockClient::~HttpWinsockClient()
{
	closesocket(Socket);
	WSACleanup();
}

int HttpWinsockClient::connect(const char * url = "toshi.nofs.navy.mil")
{
	m_isError = false;
	//struct hostent *host;

	/* �������� IP �� ��������� ����� */
	ADDRINFOA hints;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_flags = AI_ALL;
	hints.ai_family = PF_INET;
	hints.ai_protocol = IPPROTO_IPV4;
	hints.ai_socktype = SOCK_STREAM; // TCP stream-sockets
	hints.ai_flags = AI_PASSIVE;     // ��������� ��� IP-����� �� ����
	ADDRINFOA* pResult = NULL;
	std::string address(url);
	int errcode = getaddrinfo(address.c_str(), "http", &hints, &pResult);
	if (errcode != 0)
	{
		m_last_error_string = "getaddrinfo() failed.\n";
		m_isError = true;// ERROR;
		return -1;
	}

	struct sockaddr_in servAddr;
	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.S_un.S_addr = *((ULONG*)&(((sockaddr_in*)pResult->ai_addr)->sin_addr));
	int port = 80;
	servAddr.sin_port = htons(port);
	if (::connect(Socket, (struct sockaddr*) &servAddr, sizeof(servAddr)) < 0)
	{
		m_last_error_string = "connect() failed.\n";
		m_isError = true;// ERROR;
		return -1;
	}
	//std::cout << "Connected.\n";
	return 0;
}

// ������ ���� ������� ������������ URLDownloadToFile �� <Urlmon.h>
int HttpWinsockClient::get_synchronous(const char * url/* = "toshi.nofs.navy.mil"*/,
	const char * uri/* = "/ser7/finals.daily"*/,
	std::iostream & reply_stream)
{
	// � ����� get_synchronous
	/* ������� HTTP-������*/
	std::string request_daily = "GET "
		+ std::string(uri) + " HTTP/1.1\r\n"
		"Host:" + std::string(url) + "\r\n"
		"Connection : close\r\n"
		"\r\n";
	send(Socket, request_daily.c_str(), request_daily.length(), 0);
	// TODO �������� �� ������

	/* ��������� ������ */
	char buffer[512] = { 0 };
	int nDataLength;
	std::string reply;

	/*std::ofstream outfile;
	std::string outfile_name = filepath;
	outfile.open("finals2000A.daily",
		std::ios::out | std::ios::trunc);*/

	// Receive until the peer closes the connection
	do {

		nDataLength = recv(Socket, buffer, 512, 0);
		if (nDataLength > 0)
		{
			reply.append(buffer, nDataLength);
			reply_stream << buffer;
			//std::cout << "Bytes received: " << nDataLength << std::endl;
		}
		else if (nDataLength == 0)
		{
			//std::cout << "Connection closed" << std::endl;
		}
		else {
			m_last_error_string = "recv() failed: ";
			m_last_error_string += WSAGetLastError();
			return -1;
		}
	} while (nDataLength > 0);
	//outfile.close();

	return 0;
}

std::string HttpWinsockClient::get_last_error_string() const
{
	return m_last_error_string;
}

bool HttpWinsockClient::isError() const
{
	return m_isError;
}
