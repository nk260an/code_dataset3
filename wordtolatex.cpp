#include "wordtolatex.h"
#include <QFile>
#include <QDebug>
#include <QRegularExpression>
/*
 * Разметка LaTeX
 * http://www.colorado.edu/physics/phys4610/phys4610_sp15/PHYS4610_sp15/Home_files/LaTeXSymbols.pdf
 *
 * Онлайн-отладчик регулярных выражений https://regex101.com/
 *
 * Онлайн-редактор LaTeX https://www.tutorialspoint.com/latex_equation_editor.htm
*/

WordToLatex::WordToLatex(QObject *parent) : QObject(parent)
{
    // открыть файл

    QFile file_greek_table("Character table.txt");
    if (!file_greek_table.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    while (!file_greek_table.atEnd())
    {
        QString line = file_greek_table.readLine();
        line = line.trimmed();
        //qDebug() << line.left(4) << "\t" << line.mid(5, 1) << "\t"<< line.mid(7, -1);
        m_greek_table.append(ReplaceItem(line.left(4),
                                           line.mid(5, 1),
                                           line.mid(7, -1)));
        //m_hashGereek[line.mid(5,6)] = line.mid(8,-1);
    }
    file_greek_table.close();

    QFile file_math_fun_table("Math fun table.txt");
    if (!file_math_fun_table.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    while (!file_math_fun_table.atEnd())
    {
        QString line = file_math_fun_table.readLine();
        line = line.trimmed();
        m_math_fun_table.append(line);
    }
    file_math_fun_table.close();

    QFile file_brackets_table("Brackets_table.txt");
    if (!file_brackets_table.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    while (!file_brackets_table.atEnd())
    {
        QString line = file_brackets_table.readLine();
        line = line.trimmed();
        m_brackets_table.append(ReplaceItem(line.left(4),
                                            line.mid(5, 1),
                                            line.mid(7, -1)));
    }
    file_brackets_table.close();

    QFile file_diacritic_table("Diacritic table.txt");
    if (!file_diacritic_table.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    while (!file_diacritic_table.atEnd())
    {
        QString line = file_diacritic_table.readLine();
        line = line.trimmed();
        #ifdef QT_DEBUG
        QString s1 = line.left(4);
        QString s2 = line.mid(5, 1);
        QString s3 = line.mid(7, -1);
        #endif
        m_diacritic_table.append(ReplaceItem(line.left(4),
                                            line.mid(5, 1),
                                            line.mid(7, -1)));
    }
    file_diacritic_table.close();

}

void WordToLatex::convert_symbols()
{
    for(int i = 0; i < m_greek_table.length(); i++)// замена греческих букв
    {
        int idx = strTmp.indexOf(m_greek_table.at(i).rawSymbol);
        while(idx > -1)
        {

            strTmp.replace(idx, 1, m_greek_table.at(i).newSymbol);
            idx = strTmp.indexOf(m_greek_table.at(i).rawSymbol, idx + m_greek_table.at(i).newSymbol.length());
        }
    }
}

void WordToLatex::convert_upper_lower_indices()
{
    int idx_index = strTmp.indexOf(QRegularExpression("(\\^|\\_)"));
    while(idx_index > -1)
    {
        int idx_index_close = -1;

        if(strTmp.at(idx_index+1)=="(")// если выражение большое и начинается со скобки
        {
            // цикл поиска последней скобки
            idx_index_close = findMatchBracket(strTmp,idx_index+1,"(",")");//TODO
            strTmp.replace(idx_index_close, 1, "}");// TODO ???
            strTmp.replace(idx_index + 1, 1, "{");// TODO ???
        }
        else// если выражение не начинается со скобки
        {
            idx_index_close = strTmp.indexOf(QRegularExpression("($|\\s|\\.|\\,|\\=|\\+|\\-|\\/|\\^|\\_|\\&|\\@|\\x{2061}|\\))"),idx_index+1); // TODO индексы без скобок могут завершаться большим набором символов
            strTmp.insert(idx_index_close, "}");// TODO ???
            strTmp.insert(idx_index + 1, "{");// TODO ???
        }

        idx_index = strTmp.indexOf(QRegularExpression("(\\^|\\_)"), idx_index_close); // следующий нижний индекс
    }
}

void WordToLatex::convert_cyrillic()
{
    QRegularExpressionMatch rem;
    int idx_of_cyr = strTmp.indexOf(QRegularExpression("[а-яА-Я]+"), 0, &rem);
    while(idx_of_cyr > -1)
    {
//        if(rem->hasMatch())
//        {

//        }
        strTmp.insert( rem.capturedEnd(), "}");
        strTmp.insert( idx_of_cyr, "\\text{");
        //qDebug() << rem;
        // следующая дробь
        idx_of_cyr = strTmp.indexOf(QRegularExpression("[а-яА-Я]+"), rem.capturedEnd()+7, &rem);
    }
}

void WordToLatex::convert_fractions()
{
    int idx_of_frac = strTmp.indexOf("/"); // распознавание начинается с нахождения символа деления посередине
    while(idx_of_frac > -1) // если дробь найдена
    {
        bool is_brackets_left_of_frac = strTmp.at(idx_of_frac-1)==")",
             is_brackets_right_of_frac = strTmp.at(idx_of_frac+1)=="(";

        // ищется место для закрывающих скобок
        int idx_frac_close = -1;
        if(is_brackets_right_of_frac) // если знаменатель большой и окружён скобками xxx/(xxx+xxx) - найти закрывающую скобку
        {
            idx_frac_close = findMatchBracket(strTmp,idx_of_frac+1,"(",")");
            strTmp.replace(idx_frac_close, 1, "}"); // найденная скобка ")" заменяется на "}"
        }
        else // ищется пробел или другие символы окончания выражения
        {
            idx_frac_close = strTmp.indexOf(QRegularExpression("($|\\s|\\.|\\,|\\&|\\@|\\+|\\-|\\=|\\*|\\x{3017})"), idx_of_frac+1); // !!!
            strTmp.insert(idx_frac_close, "}"); // перед найденным символом ставится "}"
        }

        // ищется место для тега и открывающих скобок "\frac{"
        int idx_frac_open = -1;
        if(is_brackets_left_of_frac) // если числитель большой и окружён скобками (xxx+xxx)/xxx - найти открывающую скобку
        {
            idx_frac_open = findMatchBracket(strTmp,idx_of_frac-1,"(",")");
            strTmp.replace(idx_frac_open, 1, "\\frac{"); // найденная скобка ")" заменяется на "}"
        }
        else // ищется пробел или другие символы окончания выражения
        {
            idx_frac_open = strTmp.lastIndexOf(QRegularExpression("(^|\\s|\\&|\\@|\\+|\\-|\\=|\\*|\\x{3016})"), idx_of_frac - 1); // !!!
            strTmp.insert(idx_frac_open+1, "\\frac{"); // перед найденным символом ставится "}"
        }

        int num_to_replace = 1 + is_brackets_right_of_frac + is_brackets_left_of_frac;
        strTmp.replace(idx_of_frac + 6 - is_brackets_left_of_frac*2,
                       num_to_replace,"}{");// скобки  вместо '/'

        // следующая дробь
        idx_of_frac = strTmp.indexOf("/", idx_of_frac);
    }
}

void WordToLatex::convert_functions()
{
    for(int i = 0; i < m_math_fun_table.length(); i++)// отдельный цикл для простановки слеша в функции
    {
        int idx_function = strTmp.indexOf(QRegularExpression("(^|\\s|\\W)" + m_math_fun_table.at(i) + "(\\s|\\W|$)"));
        while(idx_function > -1)
        {
            strTmp.insert(idx_function + 1, "\\");
            idx_function = strTmp.indexOf(QRegularExpression("(^|\\s|\\W)" + m_math_fun_table.at(i) + "(\\s|\\W|$)"),
                                          idx_function + 2 + m_math_fun_table.at(i).length());
        }
    }

    // отдельный цикл для простановки границ функций \u2061 и знаков суммы/интеграла \u2592
    int idx_fun_symbol = strTmp.indexOf(QRegularExpression("(\\x{2061}|\\x{2592})"));
    while(idx_fun_symbol > -1)
    {
        int idx_fun_close = -1;
        if(strTmp.at(idx_fun_symbol + 1) == "\u3016") // если содержимое функции окружено скобками \begin
        {
            idx_fun_close = findMatchBracket(strTmp,idx_fun_symbol+1,"\u3016","\u3017");// найти закрывающую скобку
            strTmp.replace(idx_fun_close, 1, "}");
            strTmp.replace(idx_fun_symbol, 2, "{"); // заменить "\u2061{"
        }
        else // если у функции нет скобок
        {
            idx_fun_close = strTmp.indexOf(QRegularExpression("($|\\s|\\,|\\.|\\&|\\@|\\x{2061}|\\x{3017})"), idx_fun_symbol+1); // найти следующий символ/разделитель
            strTmp.insert(idx_fun_close,  "}");
            strTmp.replace(idx_fun_symbol, 1, "{");
        }

        idx_fun_symbol = strTmp.indexOf(QRegularExpression("(\\x{2061}|\\x{2592})"),
                                        idx_fun_symbol);
    }

    // отдельно обрабатывается квадратный корень \u221A
    int idx_sqrt = strTmp.indexOf("\u221A");
    while(idx_sqrt > -1)
    {
        int idx_sqrt_close = -1;
        if(strTmp.at(idx_sqrt + 1) == "(") // если содержимое функции окружено скобками \begin
        {
            idx_sqrt_close = findMatchBracket(strTmp,idx_sqrt+1,"(",")");// найти закрывающую скобку
            strTmp.replace(idx_sqrt_close, 1, "}");
            strTmp.replace(idx_sqrt + 1, 1, "{");
        }
        else // если у функции нет скобок
        {
            idx_sqrt_close = strTmp.indexOf(QRegularExpression("($|\\s|\\,|\\.|\\_|\\^|\\&|\\@|\\x{2061}|\\x{3017})"), idx_fun_symbol+1); // найти следующий символ/разделитель
            strTmp.insert(idx_sqrt_close,  "}");
            strTmp.replace(idx_sqrt + 1, 1, "{");
        }

        strTmp.replace(idx_sqrt, 1, "\\sqrt");


        idx_sqrt = strTmp.indexOf("\u221A", idx_sqrt + 1);
    }
}

void WordToLatex::convert_array()
{
    int idx_array_begin = strTmp.indexOf("\u25A0(");// матрицы
    while(idx_array_begin > -1)
    {
        int idx_array_end = findMatchBracket(strTmp,idx_array_begin+1,"(",")");// найти закрывающую скобку

        if((idx_array_end>idx_array_begin) && (idx_array_end < strTmp.length()))
        {
            // найти разделители @ и заменить на
            strTmp.replace(idx_array_end, 1, "\r\n\\end{array}\r\n");
            int idx_row_delimeter = strTmp.indexOf("@", idx_array_begin);
            while((idx_row_delimeter < idx_array_end)&&(idx_row_delimeter > -1))
            {
                strTmp.replace(idx_row_delimeter, 1, "\\\\\r\n");
                idx_row_delimeter = strTmp.indexOf("@", idx_row_delimeter);
            }
            strTmp.replace(idx_array_begin, 2, "\r\n\\begin{array}{}\r\n");
        }

        idx_array_begin = strTmp.indexOf("\u25A0(", idx_array_begin); // следующий нижний индекс
    }
}

void WordToLatex::convert_cases()
{
    int idx_cases_begin = strTmp.indexOf("\u2588(");// матрицы
    while(idx_cases_begin > -1)
    {
        int idx_cases_end = findMatchBracket(strTmp,idx_cases_begin+1,"(",")");// найти закрывающую скобку

        if((idx_cases_end>idx_cases_begin) && (idx_cases_end < strTmp.length()))
        {
            // найти разделители @ и заменить на
            strTmp.replace(idx_cases_end, 1, "\r\n\\end{cases}\r\n");
            int idx_row_delimeter = strTmp.indexOf("@");
            while((idx_row_delimeter < idx_cases_end)&&(idx_row_delimeter > -1))
            {
                strTmp.replace(idx_row_delimeter, 1, "\\\\\r\n");
                idx_row_delimeter = strTmp.indexOf("@", idx_row_delimeter);
            }
            strTmp.replace(idx_cases_begin, 2, "\r\n\\begin{cases}\r\n");
        }

        idx_cases_begin = strTmp.indexOf("\u2588(", idx_cases_end); // следующий нижний индекс
    }
}

void WordToLatex::convert_braces()
{
    for(int i = 0; i < m_brackets_table.length(); i++)// замена греческих букв
    {
        int idx = strTmp.indexOf(m_brackets_table.at(i).rawSymbol);
        while(idx > -1)
        {
            strTmp.replace(idx, 1, m_brackets_table.at(i).newSymbol);
            idx = strTmp.indexOf(m_brackets_table.at(i).rawSymbol, idx + m_brackets_table.at(i).newSymbol.length());
        }
    }
}

void WordToLatex::convert_dia()
{
    // поиск знака из базы
    for(int i = 0; i < m_diacritic_table.length(); i++) // преобразование диактирических знаков
    {
        int idx_dia = strTmp.indexOf(m_diacritic_table.at(i).rawSymbol);
        int idx_dia_open = -1;

        while(idx_dia > -1)
        {
            if(strTmp.at(idx_dia - 2)==")")// если выражение большое и в скобках
            {
                // цикл поиска последней скобки
                idx_dia_open = findMatchBracket(strTmp,idx_dia-2,"(",")");
                strTmp.replace(idx_dia-2, 3, "}");
                strTmp.replace(idx_dia_open, 1, m_diacritic_table.at(i).newSymbol + "{");
            }
            else// если выражение не в скобках
            {
                idx_dia_open = idx_dia - 2; // TODO индексы без скобок могут завершаться большим набором символов
                strTmp.replace(idx_dia-1, 2, "}");
                strTmp.insert(idx_dia_open, m_diacritic_table.at(i).newSymbol + "{");// TODO ???
                /*if(idx_dia_open > 0)
                {
                    strTmp.insert(idx_dia_open + 1, m_diacritic_table.at(i).newSymbol + "{");// TODO ???
                }
                else if(idx_dia_open == 0)
                {
                }
                else
                {

                }*/
            }

            idx_dia = strTmp.indexOf(m_diacritic_table.at(i).rawSymbol, idx_dia + 5); // следующий нижний индекс
        }
    }
}

int WordToLatex::convert(QString const strInput,
                         QString & strOutput)
{
    // TODO ПЕРЕВОД ТАБЛИЦ
    // TODO Функция от функции, проблемы с закрывающими скобками
    // TODO Диакритический знак над содержимым функции - арбитраж скобок
    // TODO код возврата метода
    // TODO остальные символы
    // TODO Интегралы
    // TODO ПРОВЕРКА ЧЕРЕЗ LATEX РЕНДЕР
    // TODO Синтаксические отступы и переносы на новую строку
    // TODO \bfmath
    // TODO cdot как знак-разделитель
    // TODO пробел перед символами (чтобы, например, не получалось \deltaA)
    // !!! может вызывать проблемы в дальнейшем цикле обработки,
    // т.к. пробел - опорный символ-разделитель, и появление лишних пробелов нарушит вид исходной формулы

    strTmp = strInput;// копия входной строки

    // сконвертировать все скобки {}
    int idx = strTmp.indexOf("}");
    while(idx > -1)
    {
        strTmp.insert(idx, "\\right");
        idx = strTmp.indexOf("}", idx + 6);
    }

    idx = strTmp.indexOf("{");
    while(idx > -1)
    {
        strTmp.insert(idx, "\\left");
        idx = strTmp.indexOf("{", idx + 5);
    }

    convert_dia(); // диакритические символы

    convert_symbols(); // символы и греческие буквы

    convert_upper_lower_indices(); // верхние и нижние индексы

    convert_cyrillic(); // кириллица - \text{}

    convert_fractions(); // дроби - \frac{}{}

    convert_functions(); // функции

    convert_array(); // array - "\u25A0"

    convert_cases(); // cases - "\u2588"

    convert_braces(); // скобки

    strOutput = strTmp;

    return 0;
}

/*
ReplaceItem::ReplaceItem(uint p_unicode_number,
                         QString p_rawSymbol,
                         QString p_newSymbol)
    :rawSymbol(p_rawSymbol),
     newSymbol(p_newSymbol)
{
    unicode_number = p_unicode_number;
    return;
}*/

ReplaceItem::ReplaceItem(QString p_unicode_number,
                         QString p_rawSymbol,
                         QString p_newSymbol)
    :rawSymbol(p_rawSymbol),
     newSymbol(p_newSymbol)
{
    unicode_number = p_unicode_number.toInt();
    return;
}


/*
int markBorders(QString const &str, int pos)
{
    if
}*/

int WordToLatex::findMatchBracket(QString const &str,
                                  int pos,
                                  QString smb1 = "(",
                                  QString smb2 = ")")
{
    int opened = 1;
    int dpos = 0;
    if(str.at(pos) == smb1)
    {
        opened = 1;
        dpos = 1;

    }
    else if (str.at(pos) == smb2)
    {
        opened = -1;
        dpos = -1;
    }
    else  return -1;

    //pos += dpos;
    while((opened != 0) && (pos > -1) && (pos < str.length()))
    {
        pos += dpos;

        if(str.at(pos) == smb1)
        {
            opened++;
        }
        else if (str.at(pos) == smb2)
        {
            opened--;
        }

        /*if((opened == 0) || (pos < 0) || (pos >= str.length()))
        {
            return pos;
        }*/

    }
    return pos;
}

/*
int findNextClosingBracket(QString const &str, int pos)
{
    int opened = 1; // уже есть одна открытая скобка
    pos++;

    // пока не найдена закрывающая скобка,
    // либо пока не достигнут конец строки
    while((opened > 0) && (pos < str.length()))
    {
        if(str.at(pos) == "(")
        {
            opened++; // число открытых скобок увеличить
        }
        else if (str.at(pos) == ")")
        {
            opened--; // уменьшить число открытых скобок
        }
        pos++;
    }
    return pos;
}

int findPrevOpeningBracket(QString const &str, int pos)
{

    int opened = 1; // уже есть одна открытая скобка
    pos++;

    // пока не найдена закрывающая скобка,
    // либо пока не достигнут конец строки
    while((opened > 0) && (pos < str.length()))
    {
        if(str.at(pos) == "(")
        {
            opened++; // число открытых скобок увеличить
        }
        else if (str.at(pos) == ")")
        {
            opened--; // уменьшить число открытых скобок
        }
        pos++;
    }
    return pos;
}
*/
