#ifndef COMBOBOX1_H
#define COMBOBOX1_H
#include <QComboBox>
#include <QMouseEvent>
#include <listsurface.h>

class ComboBox1 : public QComboBox
{
public:
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE
    {
        lst = new ListSurface(this);
        lst->show(this);
    }
    ComboBox1(QWidget *parent = 0);
    ListSurface * lst;
};

#endif // COMBOBOX1_H
