#ifndef LISTSURFACE_H
#define LISTSURFACE_H

#include <QObject>
#include <QWidget>
#include <QFrame>
#include <QtDebug>
#include <QLayout>
#include <QListView>

class ListSurface : public QFrame
{
    Q_OBJECT
public:
    ListSurface(QWidget *parent = 0);
    QWidget *_parent;
    QObject *_sender;
    QGridLayout *grid;
    QListView *lstview;
    QFrame *frame;
public slots:
    void show(QObject *sender = 0);
private slots:
    void Ex();
};

#endif // LISTSURFACE_H
