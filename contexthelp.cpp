#include "contexthelp.h"
#include "ui_contexthelp.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QDebug>
#include <QRegExp>

ContextHelp::ContextHelp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ContextHelp)
{
    ui->setupUi(this);

    QDir myPath(QDir::currentPath() + "Spr"); //Указываем папку для просмотра файлов и их фильтрования
    myPath.setFilter(QDir::Files | QDir::NoDotAndDotDot); //Запускаем фильтр
    myList = myPath.entryList(); //Загружаем список файлов в myList
    ui->listWidget->addItems(myList); //Отображаем myList в listWidget
}

ContextHelp::~ContextHelp()
{
    delete ui;
}
void ContextHelp::on_lineEdit_textChanged(const QString &arg1)
//Фильтр айтемов в listWidget
{
    QRegExp regExp(arg1, Qt::CaseInsensitive, QRegExp::Wildcard);
    ui->listWidget->clear();
    ui->listWidget->addItems(myList.filter(regExp));
}

void ContextHelp::on_btnExitFromContextHelp_clicked()
//Закрываем справку
{
    this->close();
}

void ContextHelp::on_listWidget_itemClicked()
{
    //Загружаем файл HTML в TextBrowser
    QListWidgetItem *item = ui->listWidget->currentItem();
    const QString FileName = QDir::currentPath() + "//res//" + item->text();
    QFile file(FileName);
    if(!file.open(QIODevice::ReadOnly)){
        //сообщение о ошибке
    }
    QTextStream ContextHelpText(&file);
    ui->ContextHelpLabel->setText(FileName);
    ui->ContextHelpTextBrowser->setText(ContextHelpText.readAll());
    //ContextHelpText.reset();

    file.close();
}
