#ifndef MOUSEM_H
#define MOUSEM_H

#include <QObject>
#include <QDebug>
#include<QPoint>

class mousem : public QObject
{
    Q_OBJECT
public:
    explicit mousem(QObject *parent = 0);
Q_INVOKABLE void test ();
    Q_INVOKABLE void clear();
    Q_INVOKABLE void save();
    Q_INVOKABLE void add(double x, double y);
    Q_INVOKABLE void add(QPointF point);
signals:

public slots:
};

#endif // MOUSEM_H
