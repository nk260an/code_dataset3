#include <QDebug>
#include "treeitem.h"
#include "treemodel.h"
#include "ContensController.h"
#include "customtype.h"

#include <QStringList>


TreeModel::TreeModel(QObject *parent)
    : QAbstractItemModel(parent)
{
    m_roleNameMapping[TreeModelRoleName] = "title";


    QList<QVariant> rootData;
    rootData << "Title";
    rootItem = new TreeItem(rootData);


}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != TreeModelRoleName && role != TreeModelRoleDescription)
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

    return item->data(role - Qt::UserRole - 1);
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}


QHash<int, QByteArray> TreeModel::roleNames() const
{
    return m_roleNameMapping;
}

QVariant TreeModel::newCustomType(const QString &text, QUrl url, int position)
{
    CustomType *t = new CustomType(this);
    t->setText(text);
    t->setIndentation(position);
    t->setUrl(url);
    QVariant v;
    v.setValue(t);
    return v;
}



int xx = 0;
void bottree(ContentsObject *tree, QString search)//Заголовок не тру
{
    for(int i = 0; i<tree->getChildren().size(); i++)
    {
        QString str = tree->getChildren().at(i)->getCaption().toLower();
        if(str.indexOf(search.toLower(), 0)>-1) xx++;
        else bottree(tree->getChildren().at(i), search);
    }
}



void TreeModel::prov(ContentsObject *tree, TreeItem *tree2, QString search)
{
    QVector<int> sdv;
    sdv.reserve(tree->getChildren().size());
    for(int i = 0; i<tree->getChildren().size(); i++)
    {
        QString str = tree->getChildren().at(i)->getCaption().toLower();
        if(str.indexOf(search.toLower(), 0)>-1)
        {

            beginResetModel();
            QList<QVariant> columnData;
            columnData << newCustomType(tree->getChildren().at(i)->getCaption(),tree->getChildren().at(i)->getUrl() ,i);
            tree2->appendChild(new TreeItem(columnData, tree2));
            endResetModel();
            sdv.insert(sdv.end(), i);

        }
        else
        {
            bottree(tree->getChildren().at(i), search);
            if(xx > 0)
            { xx = 0;
                beginResetModel();
                QList<QVariant> columnData;
                columnData << newCustomType(tree->getChildren().at(i)->getCaption(),tree->getChildren().at(i)->getUrl() ,i);
                tree2->appendChild(new TreeItem(columnData, tree2));
                endResetModel();
                sdv.insert(sdv.end(), i);
            }
        }
    }

    for(int i = 0; i<sdv.size(); i++)//size вектора
    {
        prov(tree->getChildren().at(sdv[i]),tree2->child(i) , search);// идти по тем числам, по которым были созданы потомки(для первого дерева)
    }
}

void TreeModel::cppSlot(const QString &msg) {
    qDebug() << msg << endl;
    beginResetModel();
    rootItem->m_childItems.clear();
    endResetModel();
    ContensController l;
    prov(&l.rootContentsItem, rootItem, msg);

}


