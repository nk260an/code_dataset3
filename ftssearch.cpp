#include "ftssearch.h"
#include <QDebug>



QHash<int, QByteArray> FtsModel::roleNames() const //+
{
    QHash<int, QByteArray> roles;
    roles[CaptionRole] = "caption";
    roles[SlovoRole] = "slovo";
    roles[ReferenceRole] = "reference";

    return roles;
}

QVariantMap FtsModel::get(int idx) const //+
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

QVariant FtsModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > Contents.count())
        return QVariant();
    const Content & ct = Contents[index.row()];
        if (role == CaptionRole)
            return ct.getcaption();
        else if (role == SlovoRole)
            return ct.getslovo();
        else if (role == ReferenceRole)
            return ct.geturl();
        return QVariant();
}

void FtsModel::add(Content ct)
{
    beginResetModel();
    Contents << ct;
    endResetModel();
}

void FtsModel::Delete()
{
    beginResetModel();
    Contents.clear();
    endResetModel();
}

int FtsModel::rowCount(const QModelIndex &parent) const
{
    return Contents.size();
}


FtsModel::FtsModel(QObject *parent)
    : QAbstractListModel(parent)
{

}


ftssearch::ftssearch(QObject *parent) : QObject(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("Content/fts.sqlite");
    db.open();
}

void ftssearch::ftsSlot(const QString &msg) {
    qDebug() << msg << endl;
    ftsList.Delete();
    QSqlQuery query;
    query.exec("SELECT title, url, content FROM FTS WHERE content MATCH '" + msg + "'");
    while (query.next())
    {
QString str = query.value(2).toString();

int j = 0;
while ((j = str.indexOf(msg, j)) != -1) {

    QString slova = str.mid(j, str.indexOf(" ",j+25) - j);
    slova.replace("\n", "");
qDebug() << j;
qDebug() << str.indexOf(" ",j);
    ftsList.add(Content(QString(query.value(0).toString()), QString(slova), QUrl(query.value(1).toString())));
   // qDebug() << j;
    ++j;
}
    }


}
