#ifndef HELPSURFACE_H
#define HELPSURFACE_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QFrame>
#include <QMouseEvent>
#include <QtDebug>
#include <QLayout>
#include <QLabel>
#include <QHash>
#include <QPoint>
#include "helphint.h"

//typedef std::map<QString, strucHintItem> helpDictionary;

typedef QHash<QString, strucHintItem> helpDictionary;

class helpsurface : public QFrame
{
    Q_OBJECT
public:
    explicit helpsurface(QWidget *parent = 0);
    explicit helpsurface(QWidget *parent = 0,
                         QPushButton * initButton = 0,
                         QMainWindow *mainWindow = 0);

    QPushButton * btnInitializingButton;
    QWidget * wgtAppWidget;
    QWidget * wgtTarget;
    QFrame * hlpFrame;
    helphint * hlpHint;
    helpDictionary dictionary;
    QWidget * parent1; // родительский компонент кнопки на главном окне, куда её возвращать
    QPoint pointGlobal;
    //QRect point2;

signals:

public slots:
    void drawHint(QPoint pos);

public:
    void show();
    void hide();

protected:
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE
    {
        drawHint(event->pos());
    }
};

#endif // HELPSURFACE_H
