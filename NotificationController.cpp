#include "NotificationController.h"



QHash<int, QByteArray> NotificationModel::roleNames() const //+
{
	QHash<int, QByteArray> roles;
	roles[InfoRole] = "Info";
	roles[importanceRole] = "importance";

	return roles;
}

QVariantMap NotificationModel::get(int idx) const //+
{
	QVariantMap map;
	foreach(int k, roleNames().keys())
	{
		map[roleNames().value(k)] = data(index(idx, 0), k);
	}
	return map;
}

QVariant NotificationModel::data(const QModelIndex &index, int role) const
{
	if (index.row() < 0 || index.row() > Notifications.count())
		return QVariant();
	const NotificationObject & ct = Notifications[index.row()];
	if (role == InfoRole)
		return ct.getinfo();
	else if (role == importanceRole)
		return ct.getimp();
	return QVariant();
}

void NotificationModel::add(NotificationObject ct)
{
	beginResetModel();
	Notifications << ct;
	endResetModel();
	qSort(Notifications.begin(), Notifications.end(), [](NotificationObject& a, NotificationObject& b) { return a.getimp() < b.getimp(); });
}

void NotificationModel::Delete(int d)
{
	// Notifications.removeAt(0);
	beginResetModel();
	Notifications.removeAt(d);
	endResetModel();

}

int NotificationModel::rowCount(const QModelIndex &parent) const
{
	return Notifications.size();
}


NotificationModel::NotificationModel(QObject *parent)
	: QAbstractListModel(parent)
{
	// qDebug() << Notifications.at(0).getinfo();
}

void NotificationController::addNotification(QString pInfotext, int pimportance) {
	notificationList.add(NotificationObject(pInfotext, pimportance));
}

NotificationController::NotificationController(QObject *parent) : QObject(parent)
{
	addNotification("Уведомление с важностью 2", 2);
	addNotification("Уведомление с важностью 3", 3);
	addNotification("Уведомление с важностью 1", 1);
	addNotification("Уведомление с важностью 5", 5);
	addNotification("Уведомление с важностью 0", 0);
	addNotification("Уведомление с важностью 1", 1);
	addNotification("Уведомление с важностью 2", 2);

}

void NotificationController::notifSlot(const int &msg) {
	notificationList.Delete(msg);
}


