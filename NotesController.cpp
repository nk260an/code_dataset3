#include "NotesController.h"
#include <QDirIterator>
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QtDebug>

NoteObject::NoteObject(const QString &datetime,
           const QString &caption,
           const QString &reference,
                       const QString &path,
           const int &atch)
    : m_datetime(datetime),
      m_caption(caption),
      m_reference(reference),
      m_path(path)
{
    m_text = atch & ATCH_TEXT;
    m_screenshot = atch & ATCH_SCREEN;
    m_audio = atch & ATCH_AUDIO;
    m_photo = atch & ATCH_PHOTO;
    m_video = atch & ATCH_VIDEO;
}

QString NoteObject::dateTime() const
{
    return m_datetime;
}

QString NoteObject::caption() const
{
    return m_caption;
}

QString NoteObject::reference() const
{
    return m_reference;
}

QString NoteObject::path() const
{
    return m_path;
}

bool NoteObject::isText() const
{
    return m_text;
}

bool NoteObject::isScreenshot() const
{
    return m_screenshot;
}

bool NoteObject::isAudio() const
{
    return m_audio;
}

bool NoteObject::isVideo() const
{
    return m_video;
}

bool NoteObject::isPhoto() const
{
    return m_photo;
}

NotesModel::NotesModel(QObject *parent)
    : QAbstractListModel(parent)
{
    // TODO прописать загрузку из папок и XML
}

void NotesModel::addNote(const NoteObject &newNote)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_note_items << newNote;
    endInsertRows();
    // TODO создать папку и XML
}

int NotesModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_note_items.count();
}

QVariant NotesModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= m_note_items.count())
        return QVariant();

    const NoteObject &messageToReturn = m_note_items[index.row()];
    if (role == DatetimeRole)
        return messageToReturn.dateTime();
    else if (role == CaptionRole)
        return messageToReturn.caption();
    else if (role == ReferenceRole)
        return messageToReturn.reference();
    else if (role == PathRole)
        return messageToReturn.path();

    else if (role == TextIncludedRole)
        return messageToReturn.isText();
    else if (role == ScreenshotIncludedRole)
        return messageToReturn.isScreenshot();
    else if (role == AudioIncludedRole)
        return messageToReturn.isAudio();
    else if (role == PhotoIncludedRole)
        return messageToReturn.isPhoto();
    else if (role == VideoIncludedRole)
        return messageToReturn.isVideo();
    return QVariant();
}

QHash<int, QByteArray> NotesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DatetimeRole] = "datetime";
    roles[CaptionRole] = "caption";
    roles[ReferenceRole] = "reference";
    roles[TextIncludedRole] = "istext";
    roles[ScreenshotIncludedRole] = "isscshot";
    roles[AudioIncludedRole] = "isaudio";
    roles[PhotoIncludedRole] = "isphoto";
    roles[VideoIncludedRole] = "isvideo";
    roles[PathRole] = "notepath";
    return roles;
}

QVariantMap NotesModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

void NotesModel::clear() // код очистки ListView для Богомолова
{
    if(rowCount() > 0){
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);
    m_note_items.clear();
    endRemoveRows();
    }
}

NotesListController::NotesListController()
    :QObject()
{
    // TODO загрузка из папок в notesModel
    loadNotes();
}

void NotesListController::loadNotes()
{
    QDirIterator it("Notes");
    //qDebug() << it.next();
    while (it.hasNext()) {
        QString strNext = it.next();
        //qDebug() << strNext;
        if(!strNext.contains("."))
        {
            QString xmlFileName(strNext + "/metadata.xml");
            QFile xmlFile(xmlFileName);
            if(xmlFile.exists())
            {
                QXmlStreamReader xmlReader(&xmlFile);
                //xmlWriter.setCodec(QTextCodec::codecForName("UTF-8"));
                if(xmlFile.open(QFile::ReadOnly | QFile::Text))
                {
                    qDebug() << "Opened" << xmlFile.fileName();
                    QString datetime;
                    QString caption;
                    QString reference;
                    QString path;
                    int atch = 0;

                    while (!xmlReader.atEnd() && !xmlReader.hasError())
                    {
                        QXmlStreamReader::TokenType token = xmlReader.readNext();
                        if (token == QXmlStreamReader::StartDocument)
                            continue;
                        if (token == QXmlStreamReader::StartElement)
                        {
                            qDebug() << xmlReader.name();
                            if (xmlReader.name() == "NoteMetadata")
                            {
                                qDebug() << "NoteMetadata";
                                datetime = xmlReader.attributes().value("datetime").toString();
                                caption = xmlReader.attributes().value("caption").toString();
                                reference = xmlReader.attributes().value("reference").toString();
                                path = xmlReader.attributes().value("path").toString();

                                if (xmlReader.attributes().hasAttribute("isText"))
                                {
                                    atch += ATCH_TEXT;
                                    //qDebug() << "isText";
                                }
                                if (xmlReader.attributes().hasAttribute("isScreenshot"))
                                {
                                    atch += ATCH_SCREEN;
                                    //qDebug() << "isScreenshot";
                                }
                                if (xmlReader.attributes().hasAttribute("isAudio"))
                                    atch += ATCH_AUDIO;
                                if (xmlReader.attributes().hasAttribute("isPhoto"))
                                    atch += ATCH_PHOTO;
                                if (xmlReader.attributes().hasAttribute("isVideo"))
                                    atch += ATCH_VIDEO;
                                //qDebug() << datetime;
                            }

                        }
                    }
                    qDebug() << "datetime " << datetime;
                    qDebug() << "caption" << caption;
                    qDebug() << "reference" << reference;
                    qDebug() << "path" << path;
                    qDebug() << "atch" << atch;
                    NoteObject tmpObj(datetime,
                                      caption,
                                      reference,
                                      path,
                                      atch);
                    // TODO добавить в модель
                    notesModel.addNote(tmpObj);
                    xmlFile.close();
                }
                else
                {
                    qDebug() << "Файл " << strNext << " невозможно открыть";
                }
            }
            else
            {
                qDebug() << "Файл " << xmlFileName << " не существует";
            }
        }
        // /etc/.
        // /etc/..
        // /etc/X11
        // /etc/X11/fs
        // ...
    }

    /*QDir dir;
    QStringList filter;
    QStringList files;
    QString filename;
    QFile file;
    QXmlStreamReader xmlReader(&file);
    QString ElemName;
    filter << "*.xml"; //Расширение файла
    dir.setPath(QDir::currentPath() + "//Notes//"); //путь к файлам
    if(files.isEmpty()){
        filename = "default_settings.xml"; //Загрузка параметров по умоляанию
    }else{
        filename = dir.path() + files.last(); //Последний файл конфигураци
    }
    file.setFileName(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text)){
    }
    while( !xmlReader.atEnd() )
    {
        xmlReader.readNext();
        if( xmlReader.name() == "SettingsMap" )
        {
            xmlReader.readNext();
            continue;
        }
        if( xmlReader.isEndElement() )
        {
            xmlReader.readNext();
            continue;
        }
        if(xmlReader.isStartDocument()){
            continue;
        }
        ElemName = xmlReader.name().toString();
        xmlReader.readNext();
        map[ElemName] = xmlReader.text().toString();
        xmlReader.readNext();
        xmlReader.readNext();
    }
    file.close();*/
}

void NotesListController::saveXML(const NoteObject &newNote)
{
    QDir dir(newNote.path());
    //qDebug() << "Вызван слот Autosave::save";

    QFile file(dir.absolutePath() + "//metadata.xml");
    QXmlStreamWriter xmlWriter(&file);
    //xmlWriter.setCodec(QTextCodec::codecForName("UTF-8"));

    if(!file.open(QFile::WriteOnly | QFile::Text))
    {
        qDebug() << "Не удалось открыть и записать XML";
        return;
    }

    xmlWriter.setAutoFormatting( true );
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement( "NoteMetadata" );

    // TODO писать параметры тут
    xmlWriter.writeAttribute("datetime", newNote.dateTime());
    xmlWriter.writeAttribute("caption", newNote.caption());
    xmlWriter.writeAttribute("reference", newNote.reference());
    xmlWriter.writeAttribute("path", newNote.path());

    if(newNote.isText())
        xmlWriter.writeAttribute("isText", "true");
    qDebug() << "isText = " << newNote.isText();
    if(newNote.isScreenshot())
        xmlWriter.writeAttribute("isScreenshot", "true");
    if(newNote.isAudio())
        xmlWriter.writeAttribute("isAudio", "true");
    if(newNote.isPhoto())
        xmlWriter.writeAttribute("isPhoto", "true");
    if(newNote.isVideo())
        xmlWriter.writeAttribute("isVideo", "true");

    // финализация XML и закрытие файла
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    file.close();

}

void NotesListController::exampleClearList()  // код очистки ListView для Богомолова
{
    notesModel.clear();
}

void NotesListController::addNote(QString datetime,
                                  QString caption,
                                  QString reference,
                                  QString path,
                                  int isText,
                                  int isScreenshot,
                                  int isAudio,
                                  int isPhoto,
                                  int isVideo)
{
    qDebug() << "Слот вызван";
    int attachmentflags = isText * ATCH_TEXT + isScreenshot * ATCH_SCREEN + isAudio * ATCH_AUDIO + isPhoto*ATCH_PHOTO + isVideo * ATCH_VIDEO;
    qDebug() << "attachmentflags" << attachmentflags;
    NoteObject tmpData(QDate::currentDate().toString("dd.MM.yyyy ") +
                                      QTime::currentTime().toString("hh:mm"),
                                      "Заметка " + QString::number(notesModel.rowCount() + 1),
                                      reference,
                                      path,
                                      attachmentflags);
    notesModel.addNote(tmpData);

    // TODO записать в XML
    saveXML(tmpData);
}
